package com.epam.newsmanagement.service;

import com.epam.newsmanagement.dao.INewsDAO;
import com.epam.newsmanagement.entity.News;
import com.epam.newsmanagement.exception.ServiceException;
import com.epam.newsmanagement.util.SearchCriteria;

import java.util.List;

/**
 * <p>Represents service for {@link INewsDAO}</p>
 *
 * @author Dzmitry Anikeichanka
 */
public interface INewsService {

    /**
     * <p>Adds new News into the database and returns the added entity ID</p>
     *
     * @param news news entity that must be added
     * @return {@link Long} added entity ID
     * @throws ServiceException
     */
    Long addNews(News news) throws ServiceException;

    /**
     * <p>Receives news by ID</p>
     *
     * @param newsId news ID which entity must be received by
     * @return {@link News} received entity
     * @throws ServiceException
     */
    News getNewsById(Long newsId) throws ServiceException;

    /**
     * <p>Receives all news</p>
     *
     * @return {@link List} received entities
     * @throws ServiceException
     */
    List<News> getAllNews() throws ServiceException;

    /**
     * <p>Search news by {@link SearchCriteria} ID</p>
     *
     * @param searchCriteria search criteria which news must be searched by
     * @param start first index of the range
     * @param end last index of the range
     * @return {@link List} received entities
     * @throws ServiceException
     */
    List<News> searchNewsBySearchCriteria(SearchCriteria searchCriteria, int start, int end) throws ServiceException;

    /**
     * <p>Search news by author ID</p>
     *
     * @param authorId author ID which news must be searched by
     * @param start first index of the range
     * @param end last index of the range
     * @return {@link List} received entities
     * @throws ServiceException
     */
    List<News> searchNewsByAuthor(Long authorId, int start, int end) throws ServiceException;

    /**
     * <p>Search news by tags IDs</p>
     *
     * @param tagIdList tags IDs which news must be searched by
     * @param start first index of the range
     * @param end last index of the range
     * @return {@link List} received entities
     * @throws ServiceException
     */
    List<News> searchNewsByTags(List<Long> tagIdList, int start, int end) throws ServiceException;

    /**
     * <p>Search news by author ID and tags IDs</p>
     *
     * @param authorId author ID which news must be searched by
     * @param tagIdList tag IDs which news must be searched by
     * @param start first index of the range
     * @param end last index of the range
     * @return {@link List} received entities
     * @throws ServiceException
     */
    List<News> searchNewsByAuthorAndTags(Long authorId, List<Long> tagIdList, int start, int end) throws ServiceException;

    /**
     * <p>Receives news within the range</p>
     *
     * @param start first news index
     * @param end last news index
     * @return {@link News} object with received news
     * @throws ServiceException
     */
    List<News> getNewsWithRange(int start, int end) throws ServiceException;

    /**
     * <p>Updates news entity</p>
     *
     * @param news news entity that must be updated
     * @throws ServiceException
     */
    void updateNews(News news) throws ServiceException;

    /**
     * <p>Removes news by ID</p>
     *
     * @param newsId news ID which entity must be removed by
     * @throws ServiceException
     */
    void deleteNewsById(Long newsId) throws ServiceException;

    /**
     * <p>Gets count of all news</p>
     *
     * @return count of all news
     * @throws ServiceException
     */
    int getAllNewsCount() throws ServiceException;

    /**
     * <p>Gets count of news according to {@link SearchCriteria}</p>
     *
     * @param searchCriteria {@link SearchCriteria} object that determines news
     * @return count of news
     * @throws ServiceException
     */
    int getNewsCountBySearchCriteria(SearchCriteria searchCriteria) throws ServiceException;

    /**
     * <p>Receives next news ID by current news ID</p>
     *
     * @param newsId current news ID
     * @return next news ID or 0 if there is no next news
     * @throws ServiceException
     */
    long getNextNewsId(long newsId) throws ServiceException;

    /**
     * <p>Receives previous news ID by current news ID</p>
     *
     * @param newsId current news ID
     * @return previous news ID or 0 if there is no next news
     * @throws ServiceException
     */
    long getPreviousNewsId(long newsId) throws ServiceException;
}
