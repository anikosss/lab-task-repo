package com.epam.newsmanagement.dao;

import com.epam.newsmanagement.entity.User;
import com.epam.newsmanagement.exception.DAOException;

/**
 * <p>Represents {@link User} DAO operations</p>
 *
 * @author Dzmitry Anikeichanka
 */
public interface IUserDAO extends IBaseDAO<User, Long> {
    /**
     * <p>Receives {@link User} from the database</p>
     *
     * @param login user login that determines user reading
     * @return {@link User} object
     * @throws DAOException
     */
    User readUserByLogin(String login) throws DAOException;

}
