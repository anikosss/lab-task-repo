package com.epam.newsmanagement.command.impl;

import com.epam.newsmanagement.command.IActionCommand;
import com.epam.newsmanagement.command.factory.ActionCommandFactory;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;
import java.util.Map;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath:test-client-context.xml"})
public class NewsCommandTest extends Assert {
    @Autowired
    private ActionCommandFactory factory;
    private IActionCommand command;

    @Before
    public void setUp() throws Exception {
        command = factory.defineCommand("home");
    }

    @After
    public void tearDown() throws Exception {
    }

    @Test
    public void testExecute() throws Exception {
        Map<String, Integer> map = new HashMap<>(10);
        map.put("1", 1);
        HttpServletRequest request = Mockito.mock(HttpServletRequest.class);
        assertEquals("/WEB-INF/jsp/news.jsp", command.execute(request));
    }

}