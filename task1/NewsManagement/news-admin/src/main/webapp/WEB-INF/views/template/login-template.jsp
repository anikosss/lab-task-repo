<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<!doctype html>
<html lang="en">
<head>
    <tiles:insertAttribute name="head" />
    <title>Admin login</title>
</head>
<body>
<tiles:insertAttribute name="header" />
<div class="container">
    <div class="row">
        <div class="col-md-offset-3 col-md-6">
            <tiles:insertAttribute name="main-content" />
        </div>
    </div>
</div>
<tiles:insertAttribute name="footer" />
</body>
</html>
