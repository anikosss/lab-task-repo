package com.epam.newsmanagement.exception;

/**
 * <p>Represents exception for DAO classes</p>
 *
 * @author Dzmitry Anikeichanka
 */
public class DAOException extends Exception {
	private static final long serialVersionUID = 4094940358854583040L;

	public DAOException() {
        super();
    }

    public DAOException(String message) {
        super(message);
    }
}
