package com.epam.newsmanagement.dao;

import com.epam.newsmanagement.entity.Role;

/**
 * <p>Represents {@link Role} DAO operations</p>
 *
 * @author Dzmitry Anikeichanka
 */
public interface IRoleDAO extends IBaseDAO<Role, Long> {

}
