package com.epam.newsmanagement.service.impl;

import com.epam.newsmanagement.dao.IRoleDAO;
import com.epam.newsmanagement.entity.Role;
import com.epam.newsmanagement.service.IRoleService;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.ArrayList;
import java.util.List;

import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath:test-context.xml"})
public class RoleServiceImplTest extends Assert {
    @InjectMocks
    @Autowired
    private IRoleService roleService;
    @Mock
    private IRoleDAO roleDAO;

    @Before
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void testReadRoleById() throws Exception {
        when(roleDAO.read(2L)).thenReturn(buildRole(2L));
        Role role = roleService.getRoleById(2L);
        verify(roleDAO).read(2L);
        assertEquals("name2", role.getName());
    }

    @Test
    public void testReadRoleByWrongId() throws Exception {
        when(roleDAO.read(11L)).thenReturn(null);
        Role role = roleService.getRoleById(11L);
        assertNull(role);
    }

    @Test
    public void testAddNewRole() throws Exception {
        Role role = buildRole(1L);
        when(roleDAO.create(role)).thenReturn(10L);
        when(roleDAO.read(10L)).thenReturn(buildRole(10L));
        Long id = roleService.addRole(role);
        Role result = roleService.getRoleById(id);
        verify(roleDAO).create(role);
        verify(roleDAO).read(10L);
        assertEquals("name10", result.getName());
    }

    @Test
    public void testReadAllRoles() throws Exception {
        List<Role> roles = buildRolesList(4);
        when(roleDAO.readAll()).thenReturn(roles);
        List<Role> result = roleService.getAllRoles();
        verify(roleDAO).readAll();
        assertEquals(4, result.size());
    }

    @Test
    public void testUpdateRole() throws Exception {
        Role role = buildRole(2L);
        when(roleDAO.read(2L)).thenReturn(new Role(2L, "newName", 2L));
        roleService.updateRole(role);
        verify(roleDAO).update(role);
        assertEquals("newName", roleService.getRoleById(2L).getName());
    }

    @Test
    public void testDeleteRoleById() throws Exception {
        when(roleDAO.read(2L)).thenReturn(null);
        roleService.deleteRoleById(2L);
        verify(roleDAO).delete(2L);
        assertNull(roleService.getRoleById(2L));
    }

    private Role buildRole(long id) {
        return new Role(id, "name" + id, id);
    }

    private List<Role> buildRolesList(int size) {
        List<Role> result = new ArrayList<>();
        for (int i = 0; i < size; i++) {
            result.add(buildRole((long) i));
        }
        return result;
    }
    
}