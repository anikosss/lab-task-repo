package com.epam.newsmanagement.entity;

import java.io.Serializable;
import java.sql.Date;

/**
 * <p>Represents Author entity</p>
 *
 * @author Dzmitry Anikeichanka
 */
public class Author implements Serializable {
	private static final long serialVersionUID = -3166218123310574289L;
	private long authorId;
    private String name;
    private Date expired;

    public Author() {
    }

    public Author(long authorId, String name, Date expired) {
        this.authorId = authorId;
        this.name = name;
        this.expired = expired;
    }

    public long getAuthorId() {
        return authorId;
    }

    public void setAuthorId(long authorId) {
        this.authorId = authorId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Date getExpired() {
        return expired;
    }

    public void setExpired(Date expired) {
        this.expired = expired;
    }

    @Override
    public String toString() {
        StringBuilder result = new StringBuilder("Author{");
        result.append("authorId = ").append(authorId);
        result.append(", name = ").append(name);
        result.append(", expired = ").append(expired);
        result.append("}");
        return result.toString();
    }

    @Override
    public int hashCode() {
        int result = (int) (authorId ^ (authorId >>> 32));
        result = 31 * result + name.hashCode();
        result = 31 * result + expired.hashCode();
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (this == obj) {
            return true;
        }
        if (obj.getClass() != getClass()) {
            return false;
        }
        Author author = (Author) obj;
        if (authorId != author.authorId) {
            return false;
        }
        if (!name.equals(author.name)) {
            return false;
        }
        return expired.equals(author.expired);
    }
}
