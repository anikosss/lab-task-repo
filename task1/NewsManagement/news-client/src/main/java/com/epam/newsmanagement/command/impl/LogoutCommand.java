package com.epam.newsmanagement.command.impl;

import com.epam.newsmanagement.command.IActionCommand;
import com.epam.newsmanagement.exception.CommandException;
import com.epam.newsmanagement.message.Actions;
import com.epam.newsmanagement.util.MethodEnum;
import com.epam.newsmanagement.util.ResponseHolder;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

/**
 * <p>Represents implementation for command that provides logout operation</p>
 */
@Component
public class LogoutCommand implements IActionCommand {

    @Override
    public ResponseHolder execute(HttpServletRequest request) throws CommandException {
        HttpSession session = request.getSession(false);
        if (session != null) {
            session.removeAttribute("user");
        }
        return new ResponseHolder(MethodEnum.REDIRECT, Actions.LOGIN_ACTION);
    }
}
