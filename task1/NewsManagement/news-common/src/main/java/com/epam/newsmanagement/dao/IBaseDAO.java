package com.epam.newsmanagement.dao;

import com.epam.newsmanagement.exception.DAOException;

import java.util.List;

/**
 * <p>Represents common dao operations</p>
 *
 * @param <E> entity class
 * @param <ID> type of identifier
 * @author Dzmitry Anikeichanka
 */
public interface IBaseDAO<E, ID> {

    /**
     * <p>Inserts new entity into the database and returns the inserted entity ID</p>
     *
     * @param entity entity which must be inserted
     * @return inserted entity ID
     * @throws DAOException
     */
    ID create(E entity) throws DAOException;

    /**
     * <p>Receives an entity by the ID from database</p>
     *
     * @param id entity ID
     * @return received entity
     * @throws DAOException
     */
    E read(ID id) throws DAOException;

    /**
     * <p>Receives all entities from database</p>
     *
     * @return {@link List} received entities list
     * @throws DAOException
     */
    List<E> readAll() throws DAOException;

    /**
     * <p>Updates an entity in database</p>
     *
     * @param entity entity which must be updated
     * @throws DAOException
     */
    void update(E entity) throws DAOException;

    /**
     * <p>Removes an entity from database</p>
     *
     * @param id entity ID which must be deleted
     * @throws DAOException
     */
    void delete(ID id) throws DAOException;
}
