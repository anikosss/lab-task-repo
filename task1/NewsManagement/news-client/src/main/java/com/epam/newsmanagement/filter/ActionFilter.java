package com.epam.newsmanagement.filter;

import com.epam.newsmanagement.entity.User;
import com.epam.newsmanagement.message.Actions;
import com.epam.newsmanagement.message.Attributes;
import com.epam.newsmanagement.message.ErrorMessages;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

/**
 * <p>Filters request from user</p>
 */
@WebFilter(filterName = "ActionFilter", servletNames = { "Controller" })
public class ActionFilter implements Filter {
    public void destroy() {
    }
    
    public void init(FilterConfig config) throws ServletException {

    }

    public void doFilter(ServletRequest req, ServletResponse resp, FilterChain chain) throws ServletException, IOException {
        processFilter((HttpServletRequest) req, (HttpServletResponse) resp);
        chain.doFilter(req, resp);
    }

    /**
     * <p>Processes request and response</p>
     *
     * @param request {@link HttpServletRequest} that should be filtered
     * @param response {@link HttpServletResponse} that should be filtered
     * @throws ServletException
     * @throws IOException
     */
    private void processFilter(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String action = request.getParameter(Attributes.ACTION);
        if (action == null) {
            response.sendError(403);
            return;
        }
        if (action.equals(Attributes.NEWS) || action.equals(Attributes.VIEW_NEWS)) {
            HttpSession session = request.getSession(false);
            if (session == null) {
                request.setAttribute(Attributes.ERROR, ErrorMessages.PLEASE_LOG_IN);
                request.getRequestDispatcher(Actions.LOGIN_ACTION).forward(request, response);
            }
            User user = (User) session.getAttribute(Attributes.USER);
            if (user == null) {
                request.setAttribute(Attributes.ERROR, ErrorMessages.PLEASE_LOG_IN);
                request.getRequestDispatcher(Actions.LOGIN_ACTION).forward(request, response);
            }
        }
    }

}
