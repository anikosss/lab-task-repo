package com.epam.newsmanagement.exception;

/**
 * <p>Represents custom exception for command tier</p>
 */
public class CommandException extends Exception {

    public CommandException() {
        super();
    }

    public CommandException(String message) {
        super(message);
    }
}
