package com.epam.newsmanagement.controller;

import com.epam.newsmanagement.entity.User;
import com.epam.newsmanagement.message.Pages;
import com.epam.newsmanagement.service.INewsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class AdminMainController {
    @Autowired
    private INewsService newsService;

    @RequestMapping({"/"})
    public String showHomePage(Model model) {
        model.addAttribute("user", new User());
        return Pages.LOGIN;
    }
    
    @RequestMapping("/login")
    public String showLoginPage() {
    	return "login";
    }

    /*@RequestMapping(path = "/admin", method = RequestMethod.POST)
    public String loginUser(BindingResult bindingResult, Model model) {
        if (bindingResult.hasErrors()) {
            model.addAttribute("error", bindingResult.getFieldError());
            return Pages.LOGIN;
        }
        return "/admin";
    }*/

}
