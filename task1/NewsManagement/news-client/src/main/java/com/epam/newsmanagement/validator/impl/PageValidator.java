package com.epam.newsmanagement.validator.impl;

import com.epam.newsmanagement.exception.ValidateException;
import com.epam.newsmanagement.message.Attributes;
import com.epam.newsmanagement.message.RegExpressions;
import com.epam.newsmanagement.validator.IRequestValidator;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletRequest;

/**
 * <p>Represents validator for page</p>
 */
@Component
public class PageValidator implements IRequestValidator {

    private PageValidator() {}

    @Override
    public boolean validateRequest(HttpServletRequest request) throws ValidateException {
        String pageParam = request.getParameter(Attributes.PAGE);
        if (pageParam == null || pageParam.isEmpty()) {
            request.setAttribute(Attributes.PAGE, 1);
            return false;
        }
        if (!pageParam.matches(RegExpressions.NUMBER_REGEX)) {
            request.setAttribute(Attributes.PAGE, 1);
            return false;
        }
        request.setAttribute(Attributes.PAGE, Integer.parseInt(pageParam));
        return true;
    }
}
