package com.epam.newsmanagement.message;

/**
 * <p>Represents table row names as constant fields</p>
 *
 * @author Dzmitry Anikeichanka
 */
public final class TableRowNames {
    public static final String AUTHOR_ID_PK = "author_id_pk";
    public static final String AUTHOR_NAME = "au_name";
    public static final String AUTHOR_EXPIRED = "au_expired";
    public static final String COMMENT_ID_PK = "comment_id_pk";
    public static final String COMMENT_TEXT = "com_text";
    public static final String COMMENT_CREATION_DATE = "com_creation_date";
    public static final String COMMENT_NEWS_ID = "com_news_id";
    public static final String NEWS_ID_PK = "news_id_pk";
    public static final String NEWS_TITLE = "nw_title";
    public static final String NEWS_SHORT_TEXT = "nw_short_text";
    public static final String NEWS_FULL_TEXT = "nw_full_text";
    public static final String NEWS_CREATION_DATE = "nw_creation_date";
    public static final String NEWS_MODIFICATION_DATE = "nw_modification_date";
    public static final String NEWS_NEXT = "nw_next_news";
    public static final String NEWS_PREVIOUS = "nw_prev_news";
    public static final String ROLE_ID_PK = "role_id_pk";
    public static final String ROLE_NAME = "role_name";
    public static final String ROLE_USER_ID = "role_user_id";
    public static final String TAG_ID_PK = "tag_id_pk";
    public static final String TAG_NAME = "tag_name";
    public static final String USER_ID_PK = "user_id_pk";
    public static final String USER_NAME = "user_name";
    public static final String USER_LOGIN = "user_login";
    public static final String USER_PASSWORD = "user_password";
    public static final String NEWS_TAGS_ID_PK = "news_tag_id_pk";
    public static final String NEWS_TAGS_NEWS_ID = "nt_news_id";
    public static final String NEWS_TAGS_TAG_ID = "nt_tag_id";
    public static final String NEWS_AUTHORS_ID_PK = "news_author_id_pk";
    public static final String NEWS_AUTHORS_NEWS_ID = "na_news_id";
    public static final String NEWS_AUTHORS_AUTHOR_ID = "na_author_id";
    public static final String NEWS_COUNT = "count";

    private TableRowNames() {}

}
