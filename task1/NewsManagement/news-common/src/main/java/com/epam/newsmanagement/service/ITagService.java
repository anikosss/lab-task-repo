package com.epam.newsmanagement.service;

import com.epam.newsmanagement.dao.ITagDAO;
import com.epam.newsmanagement.entity.Tag;
import com.epam.newsmanagement.exception.ServiceException;

import java.util.List;

/**
 * <p>Represents service for {@link ITagDAO}</p>
 *
 * @author Dzmitry Anikeichanka
 */
public interface ITagService {

    /**
     * <p>Adds new Tag into the database and returns the added entity ID</p>
     *
     * @param tag tag entity that must be added
     * @return {@link Long} added entity ID
     * @throws ServiceException
     */
    Long addTag(Tag tag) throws ServiceException;

    /**
     * <p>Adds news tag by addition tag ID and news ID into the NEWS_TAGS table</p>
     *
     * @param newsId news ID that must be inserted into the table
     * @param tagId tag ID that must be inserted into the table
     * @return {@link Long} added row ID
     * @throws ServiceException
     */
    Long addNewsTag(Long newsId, Long tagId) throws ServiceException;

    /**
     * <p>Receives tag by ID</p>
     *
     * @param tagId tag ID which entity must be received by
     * @return {@link Tag} received entity
     * @throws ServiceException
     */
    Tag getTagById(Long tagId) throws ServiceException;

    /**
     * <p>Receives all tags</p>
     *
     * @return {@link List} received entities
     * @throws ServiceException
     */
    List<Tag> getAllTags() throws ServiceException;

    /**
     * <p>Receives tags by news ID</p>
     *
     * @param newsId news ID which entity must be received by
     * @return {@link List} of received entities
     * @throws ServiceException
     */
    List<Tag> getTagsByNewsId(Long newsId) throws ServiceException;

    /**
     * <p>Updates tag entity</p>
     *
     * @param tag tag entity that must be updated
     * @throws ServiceException
     */
    void updateTag(Tag tag) throws ServiceException;

    /**
     * <p>Removes tag by ID</p>
     *
     * @param tagId tag ID which entity must be removed by
     * @throws ServiceException
     */
    void deleteTagById(Long tagId) throws ServiceException;

    /**
     * <p>Removes news tag</p>
     *
     * @param newsId news ID which tag must be removed
     * @param tagId tag ID which must be removed from news
     * @throws ServiceException
     */
    void deleteNewsTag(Long newsId, Long tagId) throws ServiceException;

    /**
     * <p>Removes all news tags</p>
     *
     * @param newsId news ID which all tags must be removed
     * @throws ServiceException
     */
    void deleteAllNewsTags(Long newsId) throws ServiceException;
}
