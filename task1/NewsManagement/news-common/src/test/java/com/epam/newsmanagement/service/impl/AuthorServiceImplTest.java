package com.epam.newsmanagement.service.impl;

import com.epam.newsmanagement.dao.IAuthorDAO;
import com.epam.newsmanagement.entity.Author;
import com.epam.newsmanagement.service.IAuthorService;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.sql.Date;
import java.util.ArrayList;
import java.util.List;

import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath:test-context.xml"})
public class AuthorServiceImplTest extends Assert {
    @Autowired
    @InjectMocks
    private IAuthorService authorService;
    @Mock
    private IAuthorDAO authorDAO;

    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void testGetAuthorById() throws Exception {
        when(authorDAO.read(3L)).thenReturn(buildAuthor(3L));
        Author author = authorService.getAuthorById(3L);
        verify(authorDAO).read(3L);
        assertEquals("name3", author.getName());
    }

    @Test
    public void testGetAuthorByWrongId() throws Exception {
        when(authorDAO.read(20L)).thenReturn(null);
        Author author = authorService.getAuthorById(20L);
        verify(authorDAO).read(20L);
        assertNull(author);
    }

    @Test
    public void testAddNewAuthor() throws Exception {
        Author author = buildAuthor(2L);
        when(authorDAO.create(author)).thenReturn(10L);
        when(authorDAO.read(10L)).thenReturn(author);
        Long id = authorService.addAuthor(author);
        Author result = authorService.getAuthorById(id);
        verify(authorDAO).create(author);
        verify(authorDAO).read(id);
        assertEquals("name2", result.getName());
    }

    @Test
    public void testAddNewNewsAuthor() throws Exception {
        when(authorDAO.readByNewsId(2L)).thenReturn(buildAuthor(3L));
        authorService.addNewsAuthor(2L, 3L);
        Author author = authorService.getAuthorByNewsId(2L);
        verify(authorDAO).createByNewsId(2L, 3L);
        verify(authorDAO).readByNewsId(2L);
        assertEquals("name3", author.getName());
    }

    @Test
    public void testGetAllAuthors() throws Exception {
        when(authorDAO.readAll()).thenReturn(buildAuthorsList(4));
        List<Author> authors = authorService.getAllAuthors();
        verify(authorDAO).readAll();
        assertEquals(4, authors.size());
    }

    @Test
    public void testUpdateAuthor() throws Exception {
        Author author = buildAuthor(2L);
        when(authorDAO.read(2L)).thenReturn(new Author(2L, "newName", null));
        authorService.updateAuthor(author);
        Author result = authorService.getAuthorById(2L);
        verify(authorDAO).update(author);
        verify(authorDAO).read(2L);
        assertEquals("newName", result.getName());
    }

    @Test
    public void testUpdateNewsAuthor() throws Exception {
        when(authorDAO.readByNewsId(2L)).thenReturn(buildAuthor(2L));
        authorService.updateNewsAuthor(2L, 2L);
        Author author = authorService.getAuthorByNewsId(2L);
        verify(authorDAO).updateNewsAuthor(2L, 2L);
        verify(authorDAO).readByNewsId(2L);
        assertEquals("name2", author.getName());
    }

    @Test
    public void testDeleteAuthorById() throws Exception {
        when(authorDAO.read(5L)).thenReturn(null);
        authorService.deleteAuthorById(5L);
        Author author = authorService.getAuthorById(5L);
        verify(authorDAO).delete(5L);
        assertNull(author);
    }

    @Test
    public void testDeleteNewsAuthor() throws Exception {
        when(authorDAO.readByNewsId(3L)).thenReturn(null);
        authorService.deleteNewsAuthor(3L);
        Author author = authorService.getAuthorByNewsId(3L);
        verify(authorDAO).deleteByNewsId(3L);
        assertNull(author);
    }

    @Test
    public void testExpired() throws Exception {
        Author author = buildAuthor(2L);
        author.setExpired(new Date(System.currentTimeMillis()));
        when(authorDAO.read(2L)).thenReturn(author);
        authorService.setExpired(2L);
        Author result = authorService.getAuthorById(2L);
        verify(authorDAO).setExpired(2L);
        verify(authorDAO).read(2L);
        assertNotNull(result.getExpired());
    }

    private Author buildAuthor(long id) {
        return new Author(id, "name" + id, null);
    }

    private List<Author> buildAuthorsList(int size) {
        List<Author> result = new ArrayList<>();
        for (int i = 0; i < size; i++) {
            result.add(buildAuthor((long) i));
        }
        return result;
    }
    
}