package com.epam.newsmanagement.command.impl;

import com.epam.newsmanagement.command.IActionCommand;
import com.epam.newsmanagement.dto.NewsDTO;
import com.epam.newsmanagement.entity.Author;
import com.epam.newsmanagement.entity.Tag;
import com.epam.newsmanagement.exception.CommandException;
import com.epam.newsmanagement.exception.ServiceException;
import com.epam.newsmanagement.message.Attributes;
import com.epam.newsmanagement.message.Pages;
import com.epam.newsmanagement.service.IAuthorService;
import com.epam.newsmanagement.service.ICommonService;
import com.epam.newsmanagement.service.INewsService;
import com.epam.newsmanagement.service.ITagService;
import com.epam.newsmanagement.util.ResponseHolder;
import com.epam.newsmanagement.validator.IRequestValidator;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.List;

/**
 * <p>Represents implementation for command that should display news for client</p>
 */
@Component
public class NewsCommand implements IActionCommand {
    private static final int NEWS_PER_PAGE = 6;
    private static final Logger LOGGER = Logger.getLogger(NewsCommand.class);
    @Autowired
    private ICommonService commonService;
    @Autowired
    private INewsService newsService;
    @Autowired
    private IAuthorService authorService;
    @Autowired
    private ITagService tagService;
    @Autowired
    @Qualifier("pageValidator")
    IRequestValidator pageValidator;

    private NewsCommand() {}

    @Override
    public ResponseHolder execute(HttpServletRequest request) throws CommandException {
        ResponseHolder result = new ResponseHolder();
        try {
            pageValidator.validateRequest(request);
            int page = (int) request.getAttribute(Attributes.PAGE);
            List<NewsDTO> news = readNews(page);
            List<Author> authors = authorService.getAllAuthors();
            List<Tag> tags = tagService.getAllTags();
            int newsCount = newsService.getAllNewsCount();
            int pagesCount = (int) Math.ceil((double) newsCount / NEWS_PER_PAGE);
            HttpSession session = request.getSession();
            session.setAttribute(Attributes.AUTHORS, authors);
            session.setAttribute(Attributes.TAGS, tags);
            request.setAttribute(Attributes.NEWS_LIST, news);
            request.setAttribute(Attributes.PAGES_COUNT, pagesCount);
            request.setAttribute(Attributes.ACTION, Attributes.NEWS);
            result.setPage(Pages.NEWS_PAGE);
        } catch (ServiceException e) {
            LOGGER.error(e.getMessage(), e);
        }
        return result;
    }

    /**
     * <p>Reads news according to the particular page</p>
     *
     * @param page page that determines the scope of news
     * @return {@link List} of {@link NewsDTO} objects
     * @throws ServiceException
     */
    private List<NewsDTO> readNews(int page) throws ServiceException {
        int start = (page - 1) * NEWS_PER_PAGE + 1;
        int end = start + NEWS_PER_PAGE - 1;
        return commonService.getNewsWithRange(start, end);
    }

}
