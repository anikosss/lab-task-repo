package com.epam.newsmanagement.logger;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;
import org.apache.log4j.xml.DOMConfigurator;

import java.io.InputStream;

/**
 * <p>Represents logger Service classes</p>
 *
 * @author Dzmitry Anikeichanka
 */
public class ServiceLogger {
    private static final Logger LOGGER = Logger.getLogger(ServiceLogger.class);
    private String pathToConfig;
    private String pathToTestConfig;

    private ServiceLogger() {}

    /**
     * <p>Configures logger for application by the path</p>
     */
    private void initLogger() {
        InputStream inputStream = ServiceLogger.class.getClassLoader().getResourceAsStream(pathToConfig);
        new DOMConfigurator().doConfigure(inputStream, LogManager.getLoggerRepository());
    }

    /**
     * <p>Configures logger for tests by the path</p>
     */
    private void initTestLogger() {
        new PropertyConfigurator().doConfigure(pathToTestConfig, LogManager.getLoggerRepository());
    }


    /**
     * <p>Writes to log-file information about exception</p>
     *
     * @param throwable exception that has been occurred
     */
    public void error(Throwable throwable) {
        LOGGER.error(throwable.getMessage(), throwable);
    }

    public void setPathToConfig(String pathToConfig) {
        this.pathToConfig = pathToConfig;
    }

    public void setPathToTestConfig(String pathToTestConfig) {
        this.pathToTestConfig = pathToTestConfig;
    }
}
