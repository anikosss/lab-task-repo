package com.epam.newsmanagement.service;

import com.epam.newsmanagement.dao.ICommentDAO;
import com.epam.newsmanagement.entity.Comment;
import com.epam.newsmanagement.exception.ServiceException;

import java.util.List;

/**
 * <p>Represents service for {@link ICommentDAO}</p>
 *
 * @author Dzmitry Anikeichanka
 */
public interface ICommentService {

    /**
     * <p>Adds new Comment into the database and returns the added entity ID</p>
     *
     * @param comment comment entity that must be added
     * @return {@link Long} added entity ID
     * @throws ServiceException
     */
    Long addComment(Comment comment) throws ServiceException;

    /**
     * <p>Receives comment by ID</p>
     *
     * @param commentId comment ID which entity must be received by
     * @return {@link Comment} received entity
     * @throws ServiceException
     */
    Comment getCommentById(Long commentId) throws ServiceException;

    /**
     * <p>Receives all comments</p>
     *
     * @return {@link List} received entities
     * @throws ServiceException
     */
    List<Comment> getAllComments() throws ServiceException;

    /**
     * <p>Receives all news comments</p>
     *
     * @param newsId news ID which comments must be received for
     * @return {@link List} received entities
     * @throws ServiceException
     */
    List<Comment> getNewsComments(Long newsId) throws ServiceException;

    /**
     * <p>Updates comment entity</p>
     *
     * @param comment comment entity that must be updated
     * @throws ServiceException
     */
    void updateComment(Comment comment) throws ServiceException;

    /**
     * <p>Removes comment by ID</p>
     *
     * @param commentId comment ID which entity must be removed by
     * @throws ServiceException
     */
    void deleteComment(Long commentId) throws ServiceException;

    /**
     * <p>Removes all news comments</p>
     *
     * @param newsId news ID which comments must be removed
     * @throws ServiceException
     */
    void deleteNewsComments(Long newsId) throws ServiceException;
}
