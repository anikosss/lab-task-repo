package com.epam.newsmanagement.service;

import com.epam.newsmanagement.dao.IRoleDAO;
import com.epam.newsmanagement.entity.Role;
import com.epam.newsmanagement.exception.ServiceException;

import java.util.List;

/**
 * <p>Represents service for {@link IRoleDAO}</p>
 *
 * @author Dzmitry Anikeichanka
 */
public interface IRoleService {

    /**
     * <p>Adds new Role into the database and returns the added entity ID</p>
     *
     * @param role role entity that must be added
     * @return {@link Long} added entity ID
     * @throws ServiceException
     */
    Long addRole(Role role) throws ServiceException;

    /**
     * <p>Receives role by ID</p>
     *
     * @param roleId role ID which entity must be received by
     * @return {@link Role} received entity
     * @throws ServiceException
     */
    Role getRoleById(Long roleId) throws ServiceException;

    /**
     * <p>Receives all roles</p>
     *
     * @return {@link List} received entities
     * @throws ServiceException
     */
    List<Role> getAllRoles() throws ServiceException;

    /**
     * <p>Updates role entity</p>
     *
     * @param role role entity that must be updated
     * @throws ServiceException
     */
    void updateRole(Role role) throws ServiceException;

    /**
     * <p>Removes role by ID</p>
     *
     * @param roleId role ID which entity must be removed by
     * @throws ServiceException
     */
    void deleteRoleById(Long roleId) throws ServiceException;
}
