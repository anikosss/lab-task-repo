package com.epam.newsmanagement.parser.impl;

import com.epam.newsmanagement.entity.Comment;
import com.epam.newsmanagement.exception.ParserException;
import com.epam.newsmanagement.message.Attributes;
import com.epam.newsmanagement.parser.IRequestParser;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletRequest;
import java.sql.Date;

/**
 * <p>Represent parser for parsing request into {@link Comment} object</p>
 */
@Component
public class CommentParser implements IRequestParser<Comment> {

    private CommentParser() {}

    @Override
    public Comment parseRequest(HttpServletRequest request) throws ParserException {
        String commentText = request.getParameter(Attributes.COMMENT);
        Long newsId = Long.parseLong(request.getParameter(Attributes.NEWS));
        return new Comment(0, commentText, new Date(System.currentTimeMillis()), newsId);
    }
}
