package com.epam.newsmanagement.service.impl;

import com.epam.newsmanagement.dto.NewsDTO;
import com.epam.newsmanagement.entity.Author;
import com.epam.newsmanagement.entity.Comment;
import com.epam.newsmanagement.entity.News;
import com.epam.newsmanagement.entity.Tag;
import com.epam.newsmanagement.service.*;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.sql.Date;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.mockito.Matchers.anyLong;
import static org.mockito.Mockito.*;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath:test-context.xml"})
/*@TransactionConfiguration(transactionManager = "transactionManager")
@Transactional*/
public class CommonServiceImplTest extends Assert {
    @Autowired
    @InjectMocks
    private ICommonService commonService;
    @Mock
    private INewsService newsService;
    @Mock
    private IAuthorService authorService;
    @Mock
    private ITagService tagService;
    @Mock
    private ICommentService commentService;

    @Before
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);
    }

    @Ignore
    @Test
    public void testGetAllNews() throws Exception {
        when(newsService.getAllNews()).thenReturn(buildNewsList(3));
        when(authorService.getAuthorByNewsId(anyLong())).thenReturn(buildAuthor(1L));
        when(tagService.getTagsByNewsId(anyLong())).thenReturn(new ArrayList<>(Arrays.asList(
                buildTag(1L),
                buildTag(2L)
        )));
        when(commentService.getNewsComments(anyLong())).thenReturn(new ArrayList<>(Arrays.asList(
                buildComment(1L),
                buildComment(2L)
        )));
        List<NewsDTO> news = commonService.getAllNews();
        verify(newsService).getAllNews();
        verify(authorService, times(3)).getAuthorByNewsId(anyLong());
        verify(tagService, times(3)).getTagsByNewsId(anyLong());
        verify(commentService, times(3)).getNewsComments(anyLong());
        assertEquals(3, news.size());
    }

    @Ignore
    @Test
    public void testGetNewsWithRange() throws Exception {
        when(newsService.getNewsWithRange(anyInt(), anyInt())).thenReturn(buildNewsList(3));
        when(authorService.getAuthorByNewsId(anyLong())).thenReturn(buildAuthor(1L));
        when(tagService.getTagsByNewsId(anyLong())).thenReturn(new ArrayList<>(Arrays.asList(
                buildTag(1L),
                buildTag(2L)
        )));
        when(commentService.getNewsComments(anyLong())).thenReturn(new ArrayList<>(Arrays.asList(
                buildComment(1L),
                buildComment(2L)
        )));
        List<NewsDTO> news = commonService.getNewsWithRange(3, 6);
        verify(newsService).getNewsWithRange(anyInt(), anyInt());
        verify(authorService, times(3)).getAuthorByNewsId(anyLong());
        verify(tagService, times(3)).getTagsByNewsId(anyLong());
        verify(commentService, times(3)).getNewsComments(anyLong());
        assertEquals(3, news.size());
    }

    @Ignore
    @Test
    public void testGetNewsById() throws Exception {
        when(newsService.getNewsById(3L)).thenReturn(buildNews(3L));
        when(authorService.getAuthorByNewsId(3L)).thenReturn(buildAuthor(3L));
        when(tagService.getTagsByNewsId(3L)).thenReturn(new ArrayList<>(Arrays.asList(buildTag(3L))));
        when(commentService.getNewsComments(3L)).thenReturn(new ArrayList<>(Arrays.asList(buildComment(3L))));
        NewsDTO news = commonService.getNewsById(3L);
        verify(newsService).getNewsById(3L);
        verify(authorService).getAuthorByNewsId(3L);
        verify(commentService).getNewsComments(3L);
        verify(tagService).getTagsByNewsId(3L);
        assertEquals("name3", news.getAuthor().getName());
    }

    @Ignore
    @Test
    public void testAddNewNews() throws Exception {
        News news = buildNews(3L);
        when(newsService.addNews(news)).thenReturn(10L);
        when(newsService.getNewsById(10L)).thenReturn(buildNews(3L));
        NewsDTO newsTO = buildNewsDTO(3L);
        newsTO.setNews(news);
        commonService.addNews(newsTO);
        verify(newsService).addNews(news);
        verify(authorService).addNewsAuthor(10L, 3L);
        verify(tagService).addNewsTag(10L, 3L);
    }

    @Ignore
    @Test
    public void testEditNews() throws Exception {
        NewsDTO newsTO = buildNewsDTO(3L);
        News news = buildNews(3L);
        newsTO.setNews(news);
        commonService.editNews(newsTO);
        verify(newsService).updateNews(news);
        verify(authorService).updateNewsAuthor(3L, 3L);
        verify(tagService).deleteAllNewsTags(3L);
        verify(tagService).addNewsTag(3L, 2L);
        verify(tagService).addNewsTag(3L, 3L);
    }

    @Ignore
    @Test
    public void testDeleteNews() throws Exception {
        when(newsService.getNewsById(3L)).thenReturn(null);
        commonService.deleteNews(3L);
        verify(newsService).deleteNewsById(3L);
        verify(tagService).deleteAllNewsTags(3L);
        verify(authorService).deleteNewsAuthor(3L);
        verify(commentService).deleteNewsComments(3L);
        assertNull(newsService.getNewsById(3L));
    }

    private News buildNews(long id) {
        return new News(id, "title" + id, "text" + id, "text" + id, new Date(System.currentTimeMillis()),
                new Date(System.currentTimeMillis()));
    }


    private NewsDTO buildNewsDTO(long id) {
        News news = buildNews(id);
        Author author = buildAuthor(id);
        List<Tag> tags = new ArrayList<>(Arrays.asList(buildTag(id)));
        List<Comment> comments = new ArrayList<>(Arrays.asList(buildComment(id)));
        return new NewsDTO(news, author, tags, comments, 0, 0);
    }

    private Author buildAuthor(long id) {
        return new Author(id, "name" + id, null);
    }

    private Comment buildComment(long id) {
        return new Comment(id, "text" + id, new Date(System.currentTimeMillis()), id);
    }

    private Tag buildTag(long id) {
        return new Tag(id, "name" + id);
    }

    private List<News> buildNewsList(int size) {
        List<News> result = new ArrayList<>();
        for (int i = 0; i < size; i++) {
            result.add(buildNews((long) i));
        }
        return result;
    }

}