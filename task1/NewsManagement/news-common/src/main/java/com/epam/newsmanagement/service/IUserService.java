package com.epam.newsmanagement.service;

import com.epam.newsmanagement.dao.IUserDAO;
import com.epam.newsmanagement.entity.User;
import com.epam.newsmanagement.exception.ServiceException;
import org.springframework.security.core.userdetails.UserDetailsService;

import java.util.List;

/**
 * <p>Represents service for {@link IUserDAO}</p>
 *
 * @author Dzmitry Anikeichanka
 */
public interface IUserService {

    /**
     * <p>Adds new User into the database and returns the added entity ID</p>
     *
     * @param user user entity that must be added
     * @return {@link Long} added entity ID
     * @throws ServiceException
     */
    Long addUser(User user) throws ServiceException;

    /**
     * <p>Receives user by ID</p>
     *
     * @param userId user ID which entity must be received by
     * @return {@link User} received entity
     * @throws ServiceException
     */
    User getUserById(Long userId) throws ServiceException;
    
    /**
     * <p>Receives user by login</p>
     *
     * @param login user login which entity must be received by
     * @return {@link User} received entity
     * @throws ServiceException
     */
    User getUserByLogin(String login) throws ServiceException;

    /**
     * <p>Receives all users</p>
     *
     * @return {@link List} received entities
     * @throws ServiceException
     */
    List<User> getAllUsers() throws ServiceException;

    /**
     * <p>Updates user entity</p>
     *
     * @param user user entity that must be updated
     * @throws ServiceException
     */
    void updateUser(User user) throws ServiceException;

    /**
     * <p>Removes user by ID</p>
     *
     * @param userId user ID which entity must be removed by
     * @throws ServiceException
     */
    void deleteUserById(Long userId) throws ServiceException;
}
