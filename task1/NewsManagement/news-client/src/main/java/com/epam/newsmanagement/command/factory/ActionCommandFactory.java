package com.epam.newsmanagement.command.factory;

import com.epam.newsmanagement.command.IActionCommand;
import com.epam.newsmanagement.exception.CommandException;
import com.epam.newsmanagement.message.ErrorMessages;

import java.util.Map;

/**
 * <p>Represents factory for defining command according to request</p>
 */
public class ActionCommandFactory {
    private Map<String, IActionCommand> commands;

    private ActionCommandFactory() {}

    /**
     * <p>Defines right command implementation</p>
     *
     * @param key key that command defines by
     * @return {@link IActionCommand} object that should be executed
     * @throws CommandException
     */
    public IActionCommand defineCommand(String key) throws CommandException {
        IActionCommand result = commands.get(key);
        if (result == null) {
            throw new CommandException(String.format(ErrorMessages.WRONG_COMMAND, key));
        }
        return result;
    }

    public void setCommands(Map<String, IActionCommand> commands) {
        this.commands = commands;
    }
}
