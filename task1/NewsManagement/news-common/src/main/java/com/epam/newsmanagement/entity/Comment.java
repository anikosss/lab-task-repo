package com.epam.newsmanagement.entity;

import java.io.Serializable;
import java.sql.Date;

/**
 * <p>Represents Comment entity</p>
 *
 * @author Dzmitry Anikeichanka
 */
public class Comment implements Serializable {
	private static final long serialVersionUID = -3362164411346276277L;
	private long commentId;
    private String text;
    private Date creationDate;
    private long newsId;

    public Comment() {
    }

    public Comment(long commentId, String text, Date creationDate, long newsId) {
        this.commentId = commentId;
        this.text = text;
        this.creationDate = creationDate;
        this.newsId = newsId;
    }

    public long getCommentId() {
        return commentId;
    }

    public void setCommentId(long commentId) {
        this.commentId = commentId;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public Date getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(Date creationDate) {
        this.creationDate = creationDate;
    }

    public long getNewsId() {
        return newsId;
    }

    public void setNewsId(long newsId) {
        this.newsId = newsId;
    }

    @Override
    public String toString() {
        StringBuilder result = new StringBuilder("Comment{");
        result.append("commentId = ").append(commentId);
        result.append(", text = ").append(text);
        result.append(", creationDate = ").append(creationDate);
        result.append(", newsId = ").append(newsId);
        result.append("}");
        return result.toString();
    }

    @Override
    public int hashCode() {
        int result = (int) (commentId ^ (commentId >>> 32));
        result = 31 * result + text.hashCode();
        result = 31 * result + creationDate.hashCode();
        result = 31 * result + (int) (newsId ^ (newsId >>> 32));
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (this == obj) {
            return true;
        }
        if (obj.getClass() != getClass()) {
            return false;
        }
        Comment comment = (Comment) obj;
        if (commentId != comment.commentId) {
            return false;
        }
        if (newsId != comment.newsId) {
            return false;
        }
        if (!text.equals(comment.text)) {
            return false;
        }
        return creationDate.equals(comment.creationDate);

    }
}
