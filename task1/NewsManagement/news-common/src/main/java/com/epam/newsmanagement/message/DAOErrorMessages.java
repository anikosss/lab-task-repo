package com.epam.newsmanagement.message;

/**
 * <p>Represents DAO error messages as constant fields</p>
 *
 * @author Dzmitry Anikeichanka
 */
public final class DAOErrorMessages {
    public static final String CREATE_AUTHOR = "Unable to add author into the database: ";
    public static final String CREATE_COMMENT = "Unable to add comment into the database: ";
    public static final String CREATE_NEWS = "Unable to add news into the database: ";
    public static final String CREATE_ROLE = "Unable to add role into the database: ";
    public static final String CREATE_TAG = "Unable to add tag into the database: ";
    public static final String CREATE_USER = "Unable to add user into the database: ";
    public static final String CREATE_NEWS_AUTHOR = "Unable to add new's author into the database, " +
            "new's ID = %d, author's IDs = %d ";
    public static final String CREATE_NEWS_TAG = "Unable to add new's tag into the database, news ID = %d, tag ID = %d ";
    public static final String READ_AUTHOR = "Unable to read author from the database with id: %d";
    public static final String READ_AUTHOR_BY_NEWS_ID = "Unable to read author from the database by news id: %d";
    public static final String READ_COMMENT = "Unable to read comment from the database with id: %d";
    public static final String READ_COMMENT_BY_NEWS_ID = "Unable to read comment from the database by news id: %d";
    public static final String READ_NEWS = "Unable to read news from the database with id: %d";
    public static final String READ_NEWS_SHORT_TEXT = "Unable to read news short text from the database with news id: %d";
    public static final String READ_NEWS_COUNT_BY_SEARCH_CRITERIA = "Unable to calculate news count by criteria: ";
    public static final String READ_NEWS_WITH_RANGE = "Unable to read news with such range: start = %d, end = %d";
    public static final String READ_NEXT_NEWS = "Unable to read next news for news with ID = %d";
    public static final String READ_PREVIOUS_NEWS = "Unable to read previous news for news with ID = %d";
    public static final String READ_ROLE = "Unable to read role from the database with id: %d";
    public static final String READ_TAG = "Unable to read tag from the database with id: %d";
    public static final String READ_TAG_BY_NEWS_ID = "Unable to read tag from the database by news id: %d";
    public static final String READ_USER_BY_ID = "Unable to read user from the database with id: %d";
    public static final String READ_USER_BY_LOGIN = "Unable to read user from the database with login: %s";
    public static final String READ_ALL_AUTHORS = "Unable to read all authors from the database";
    public static final String READ_ALL_COMMENTS = "Unable to read all comments from the database";
    public static final String READ_ALL_NEWS = "Unable to read all news from the database";
    public static final String READ_ALL_ROLES = "Unable to read all roles from the database";
    public static final String READ_ALL_TAGS = "Unable to read all tags from the database";
    public static final String READ_ALL_USERS = "Unable to read all users from the database";
    public static final String UPDATE_AUTHOR = "Unable to update author in the database: ";
    public static final String UPDATE_COMMENT = "Unable to update comment in the database: ";
    public static final String UPDATE_NEWS = "Unable to update news in the database: ";
    public static final String UPDATE_ROLE = "Unable to update role in the database: ";
    public static final String UPDATE_TAG = "Unable to update tag in the database: ";
    public static final String UPDATE_USER = "Unable to update user in the database: ";
    public static final String UPDATE_NEWS_AUTHOR = "Unable to update news author, news ID = %d, author ID = %d";
    public static final String DELETE_AUTHOR = "Unable to delete author from the database author with id: %d";
    public static final String DELETE_COMMENT = "Unable to delete author from the database comment with id: ";
    public static final String DELETE_NEWS = "Unable to delete author from the database news with id: %d";
    public static final String DELETE_ROLE = "Unable to delete author from the database role with id: %d";
    public static final String DELETE_TAG = "Unable to delete author from the database tag with id: %d";
    public static final String DELETE_USER = "Unable to delete author from the database user with id: %d";
    public static final String DELETE_NEWS_TAG = "Unable to delete news tag, news ID = %d, tag ID = %d ";
    public static final String DELETE_NEWS_AUTHOR = "Unable to delete news author, news ID = %d ";
    public static final String DELETE_ALL_NEWS_TAGS = "Unable to delete all news tags, news ID = %d ";
    public static final String DELETE_ALL_NEWS_COMMENTS = "Unable to delete all news comments, news ID = %d ";
    public static final String NEWS_COUNT = "Unable to get news count";
    public static final String SET_AUTHOR_EXPIRED = "Unable to set expired author with id: %d";
    public static final String SEARCH_NEWS_BY_AUTHOR = "Unable to search news by author ID = %d";
    public static final String SEARCH_NEWS_BY_TAGS = "Unable to search news by tags with ID = ";
    public static final String SEARCH_NEWS_BY_AUTHOR_AND_TAGS = "Unable to search news by author ID = %d and tags with ID = ";
    public static final String SEARCH_NEWS_BY_SEARCH_CRITERIA = "Unable to search news by searchCriteria: ";

    private DAOErrorMessages() {}
}
