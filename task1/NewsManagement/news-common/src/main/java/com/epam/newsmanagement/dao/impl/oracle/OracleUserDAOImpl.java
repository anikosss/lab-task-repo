package com.epam.newsmanagement.dao.impl.oracle;

import com.epam.newsmanagement.dao.IUserDAO;
import com.epam.newsmanagement.entity.User;
import com.epam.newsmanagement.exception.DAOException;
import com.epam.newsmanagement.message.DAOErrorMessages;
import com.epam.newsmanagement.message.TableRowNames;
import com.epam.newsmanagement.util.DAOUtil;
import org.springframework.jdbc.datasource.DataSourceUtils;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * <p>Represents realization {@link IUserDAO} for Oracle database</p>
 *
 * @author Dzmitry Anikeichanka
 */
public class OracleUserDAOImpl implements IUserDAO {
    private static final String SQL_CREATE = "INSERT INTO USERS (USERS.user_id_pk, USERS.user_name, USERS.user_login, " +
            "USERS.user_password) VALUES (USERS_SEQ.nextval, ?, ?, ?)";
    private static final String SQL_READ = "SELECT USERS.user_id_pk, USERS.user_name, USERS.user_login," +
            "USERS.user_password FROM USERS WHERE USERS.user_id_pk = ?";
    private static final String SQL_READ_BY_LOGIN = "SELECT USERS.user_id_pk, USERS.user_name, USERS.user_login," +
            "USERS.user_password FROM USERS WHERE USERS.user_login = ?";
    private static final String SQL_READ_ALL = "SELECT USERS.user_id_pk, USERS.user_name, USERS.user_login, " +
            "USERS.user_password FROM USERS";
    private static final String SQL_UPDATE = "UPDATE USERS SET USERS.user_name = ?, USERS.user_login = ?, " +
            "USERS.user_password = ? WHERE USERS.user_id_pk = ?";
    private static final String SQL_DELETE = "DELETE FROM USERS WHERE USERS.user_id_pk = ?";
    private DataSource dataSource;

    private OracleUserDAOImpl() {}

    @Override
    public Long create(User user) throws DAOException {
        Long result = null;
        Connection connection = null;
        PreparedStatement statement = null;
        ResultSet resultSet = null;
        try {
            connection = DataSourceUtils.getConnection(dataSource);
            statement = connection.prepareStatement(SQL_CREATE, new String[]{ TableRowNames.USER_ID_PK });
            statement.setString(1, user.getName());
            statement.setString(2, user.getLogin());
            statement.setString(3, user.getPassword());
            statement.executeUpdate();
            resultSet = statement.getGeneratedKeys();
            if (resultSet.next()) {
                result = resultSet.getLong(1);
            } else {
                throw new DAOException(DAOErrorMessages.CREATE_USER + user);
            }
        } catch (SQLException e) {
            throw new DAOException(e.getMessage() + ". " + DAOErrorMessages.CREATE_USER + user);
        } finally {
            DAOUtil.releaseResources(dataSource, connection, statement, resultSet);
        }
        return result;
    }

    @Override
    public User read(Long userId) throws DAOException {
        User result = null;
        Connection connection = null;
        PreparedStatement statement = null;
        ResultSet resultSet = null;
        try {
            connection = DataSourceUtils.getConnection(dataSource);
            statement = connection.prepareStatement(SQL_READ);
            statement.setLong(1, userId);
            resultSet = statement.executeQuery();
            if (resultSet.next()) {
                result = buildUser(resultSet);
            }
        } catch (SQLException e) {
            throw new DAOException(e.getMessage() + ". " + String.format(DAOErrorMessages.READ_USER_BY_ID, userId));
        } finally {
            DAOUtil.releaseResources(dataSource, connection, statement, resultSet);
        }
        return result;
    }

    @Override
    public List<User> readAll() throws DAOException {
        List<User> result = null;
        Connection connection = null;
        PreparedStatement statement = null;
        ResultSet resultSet = null;
        try {
            connection = DataSourceUtils.getConnection(dataSource);
            statement = connection.prepareStatement(SQL_READ_ALL);
            resultSet = statement.executeQuery();
            result = new ArrayList<>();
            while (resultSet.next()) {
                User user = buildUser(resultSet);
                result.add(user);
            }
        } catch (SQLException e) {
            throw new DAOException(e.getMessage() + ". " + DAOErrorMessages.READ_ALL_USERS);
        } finally {
            DAOUtil.releaseResources(dataSource, connection, statement, resultSet);
        }
        return result;
    }

    @Override
    public void update(User user) throws DAOException {
        Connection connection = null;
        PreparedStatement statement = null;
        try {
            connection = DataSourceUtils.getConnection(dataSource);
            statement = connection.prepareStatement(SQL_UPDATE);
            statement.setString(1, user.getName());
            statement.setString(2, user.getLogin());
            statement.setString(3, user.getPassword());
            statement.setLong(4, user.getUserId());
            statement.executeUpdate();
        } catch (SQLException e) {
            throw new DAOException(e.getMessage() + ". " + DAOErrorMessages.UPDATE_USER + user);
        } finally {
            DAOUtil.releaseResources(dataSource, connection, statement);
        }
    }

    @Override
    public void delete(Long userId) throws DAOException {
        Connection connection = null;
        PreparedStatement statement = null;
        try {
            connection = DataSourceUtils.getConnection(dataSource);
            statement = connection.prepareStatement(SQL_DELETE);
            statement.setLong(1, userId);
            statement.executeUpdate();
        } catch (SQLException e) {
            throw new DAOException(e.getMessage() + ". " + String.format(DAOErrorMessages.DELETE_USER, userId));
        } finally {
            DAOUtil.releaseResources(dataSource, connection, statement);
        }
    }

    @Override
    public User readUserByLogin(String login) throws DAOException {
        User result = null;
        Connection connection = null;
        PreparedStatement statement = null;
        ResultSet resultSet = null;
        try {
            connection = DataSourceUtils.getConnection(dataSource);
            statement = connection.prepareStatement(SQL_READ_BY_LOGIN);
            statement.setString(1, login);
            resultSet = statement.executeQuery();
            if (resultSet.next()) {
                result = buildUser(resultSet);
            }
        } catch (SQLException e) {
            throw new DAOException(e.getMessage() + ". " + String.format(DAOErrorMessages.READ_USER_BY_LOGIN, login));
        } finally {
            DAOUtil.releaseResources(dataSource, connection, statement, resultSet);
        }
        return result;
    }

    /**
     * Creates User object from ResultSet
     *
     * @param resultSet {@link ResultSet} which data must be received from
     * @return {@link User} received entity
     * @throws SQLException
     */
    private User buildUser(ResultSet resultSet) throws SQLException {
        long id = resultSet.getLong(TableRowNames.USER_ID_PK);
        String name = resultSet.getString(TableRowNames.USER_NAME);
        String login = resultSet.getString(TableRowNames.USER_LOGIN);
        String password = resultSet.getString(TableRowNames.USER_PASSWORD);
        return new User(id, name, login, password);
    }

    public void setDataSource(DataSource dataSource) {
        this.dataSource = dataSource;
    }
}
