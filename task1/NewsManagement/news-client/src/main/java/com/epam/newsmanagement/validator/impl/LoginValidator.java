package com.epam.newsmanagement.validator.impl;

import com.epam.newsmanagement.exception.ValidateException;
import com.epam.newsmanagement.message.Attributes;
import com.epam.newsmanagement.message.ErrorMessages;
import com.epam.newsmanagement.validator.IRequestValidator;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletRequest;

/**
 * <p>Represents validator for login operation</p>
 */
@Component
public class LoginValidator implements IRequestValidator {

    private LoginValidator() {}

    @Override
    public boolean validateRequest(HttpServletRequest request) throws ValidateException {
        String login = request.getParameter(Attributes.LOGIN);
        String password = request.getParameter(Attributes.PASSWORD);
        if (login == null || password == null || login.isEmpty() || password.isEmpty()) {
            request.setAttribute(Attributes.ERROR, ErrorMessages.WRONG_LOGIN_OR_PASS);
            throw new ValidateException(ErrorMessages.WRONG_LOGIN_OR_PASS);
        }
        return true;
    }
}
