package com.epam.newsmanagement.service;

import com.epam.newsmanagement.dto.NewsDTO;
import com.epam.newsmanagement.exception.ServiceException;
import com.epam.newsmanagement.util.SearchCriteria;

import java.util.List;

/**
 * <p>Represents common service transactional operations for application</p>
 *
 * @author Dzmitry Anikeichanka
 */
public interface ICommonService {

    /**
     * <p>Adds new news with author and tags in one transaction</p>
     *
     * @param newsDTO {@link NewsDTO} object which data must be added into the database
     * @return {@link Long} added entity ID
     * @throws ServiceException
     */
    Long addNews(NewsDTO newsDTO) throws ServiceException;

    /**
     * <p>Receives all news with author, tags and comments</p>
     *
     * @return {@link NewsDTO} object with received news, author, tags and comments
     * @throws ServiceException
     */
    List<NewsDTO> getAllNews() throws ServiceException;

    /**
     * <p>Searches news by {@link SearchCriteria}</p>
     *
     * @param searchCriteria {@link SearchCriteria} object which data must be searched by
     * @param start first index of the range
     * @param end last index of the range
     * @return {@link List} of {@link NewsDTO} objects
     * @throws ServiceException
     */
    List<NewsDTO> searchNews(SearchCriteria searchCriteria, int start, int end) throws ServiceException;

    /**
     * <p>Receives news with author, tags and comments within the range</p>
     *
     * @param start first news index
     * @param end last news index
     * @return {@link NewsDTO} object with received news, author, tags and comments
     * @throws ServiceException
     */
    List<NewsDTO> getNewsWithRange(int start, int end) throws ServiceException;

    /**
     * <p>Receives one news with author, tags and comments</p>
     *
     * @param newsId news ID which data must be received by
     * @return {@link NewsDTO} object with searched data
     * @throws ServiceException
     */
    NewsDTO getNewsById(Long newsId) throws ServiceException;

    /**
     * <p>Edits news with author, tags and comments in one transaction</p>
     *
     * @param newsDTO {@link NewsDTO} object with data
     * @throws ServiceException
     */
    void editNews(NewsDTO newsDTO) throws ServiceException;

    /**
     * <p>Removes news with author, tags and comments in one transaction</p>
     *
     * @param newsId news ID which data must be removed by
     * @throws ServiceException
     */
    void deleteNews(Long newsId) throws ServiceException;
}
