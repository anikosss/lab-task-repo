package com.epam.newsmanagement.entity;

import java.io.Serializable;

/**
 * <p>Represents Role entity</p>
 *
 * @author Dzmitry Anikeichanka
 */
public class Role implements Serializable {
	private static final long serialVersionUID = 2030314206507226372L;
	private long roleId;
    private String name;
    private long userId;

    public Role() {
    }

    public Role(long roleId, String name, long userId) {
        this.roleId = roleId;
        this.name = name;
        this.userId = userId;
    }

    public long getRoleId() {
        return roleId;
    }

    public void setRoleId(long roleId) {
        this.roleId = roleId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public long getUserId() {
        return userId;
    }

    public void setUserId(long userId) {
        this.userId = userId;
    }

    @Override
    public String toString() {
        StringBuilder result = new StringBuilder("Role{");
        result.append("roleId = ").append(roleId);
        result.append(", name = ").append(name);
        result.append(", userId = ").append(userId);
        result.append('}');
        return result.toString();
    }

    @Override
    public int hashCode() {
        int result = (int) (roleId ^ (roleId >>> 32));
        result = 31 * result + name.hashCode();
        result = 31 * result + (int) (userId ^ (userId >>> 32));
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (this == obj) {
            return true;
        }
        if (obj.getClass() != getClass()) {
            return false;
        }
        Role role = (Role) obj;
        if (roleId != role.roleId) {
            return false;
        }
        if (userId != role.userId) {
            return false;
        }
        return name.equals(role.name);

    }
}
