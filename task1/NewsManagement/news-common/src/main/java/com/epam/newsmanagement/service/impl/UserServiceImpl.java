package com.epam.newsmanagement.service.impl;

import java.util.List;

import com.epam.newsmanagement.dao.IUserDAO;
import com.epam.newsmanagement.entity.User;
import com.epam.newsmanagement.exception.DAOException;
import com.epam.newsmanagement.exception.ServiceException;
import com.epam.newsmanagement.service.IUserService;

/**
 * <p>Represents realization for {@link IUserService}</p>
 *
 * @author Dzmitry Anikeichanka
 */
public class UserServiceImpl implements IUserService {
    private IUserDAO userDAO;

    @Override
    public Long addUser(User user) throws ServiceException {
        Long result = null;
        try {
            result = userDAO.create(user);
        } catch (DAOException e) {
            throw new ServiceException(e.getMessage());
        }
        return result;
    }

    @Override
    public User getUserById(Long userId) throws ServiceException {
        User result = null;
        try {
            result = userDAO.read(userId);
        } catch (DAOException e) {
            throw new ServiceException(e.getMessage());
        }
        return result;
    }

    @Override
    public User getUserByLogin(String login) throws ServiceException {
    	User result = null;
    	try {
    		result = userDAO.readUserByLogin(login);
    	} catch (DAOException e) {
    		throw new ServiceException(e.getMessage());
    	}
    	return result;
    }
    
    @Override
    public List<User> getAllUsers() throws ServiceException {
        List<User> result = null;
        try {
            result = userDAO.readAll();
        } catch (DAOException e) {
            throw new ServiceException(e.getMessage());
        }
        return result;
    }

    @Override
    public void updateUser(User user) throws ServiceException {
        try {
            userDAO.update(user);
        } catch (DAOException e) {
            throw new ServiceException(e.getMessage());
        }
    }

    @Override
    public void deleteUserById(Long userId) throws ServiceException {
        try {
            userDAO.delete(userId);
        } catch (DAOException e) {
            throw new ServiceException(e.getMessage());
        }
    }

    public void setUserDAO(IUserDAO userDAO) {
        this.userDAO = userDAO;
    }

}
