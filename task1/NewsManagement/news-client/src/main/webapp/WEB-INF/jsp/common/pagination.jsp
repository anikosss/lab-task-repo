<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<br>
<br>
<br>
<div class="row">
	<div class="col-md-offset-4 col-md-4 text-center">
		<ul class="pagination pagination-lg">
			<c:forEach begin="1" end="${pagesCount}" var="num">
				<c:url value="/controller" var="pageUrl">
					<c:param name="action" value="${action}" />
					<c:forEach items="${params}" var="entry">
						<c:param name="${entry.key}" value="${entry.value}" />
					</c:forEach>
					<c:param name="page" value="${num}" />
				</c:url>
				<li><a href="${pageUrl}">${num}</a></li>
			</c:forEach>
		</ul>
	</div>
</div>