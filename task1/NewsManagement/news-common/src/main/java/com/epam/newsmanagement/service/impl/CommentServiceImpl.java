package com.epam.newsmanagement.service.impl;

import com.epam.newsmanagement.dao.ICommentDAO;
import com.epam.newsmanagement.entity.Comment;
import com.epam.newsmanagement.exception.DAOException;
import com.epam.newsmanagement.exception.ServiceException;
import com.epam.newsmanagement.service.ICommentService;

import java.util.List;

/**
 * <p>Represents realization for {@link ICommentService}</p>
 *
 * @author Dzmitry Anikeichanka
 */
public class CommentServiceImpl implements ICommentService {
    private ICommentDAO commentDAO;

    @Override
    public Long addComment(Comment comment) throws ServiceException {
        Long result = null;
        try {
            result = commentDAO.create(comment);
        } catch (DAOException e) {
            throw new ServiceException(e.getMessage());
        }
        return result;
    }

    @Override
    public Comment getCommentById(Long commentId) throws ServiceException {
        Comment result = null;
        try {
            result = commentDAO.read(commentId);
        } catch (DAOException e) {
            throw new ServiceException(e.getMessage());
        }
        return result;
    }

    @Override
    public List<Comment> getAllComments() throws ServiceException {
        List<Comment> result = null;
        try {
            result = commentDAO.readAll();
        } catch (DAOException e) {
            throw new ServiceException(e.getMessage());
        }
        return result;
    }

    @Override
    public List<Comment> getNewsComments(Long newsId) throws ServiceException {
        List<Comment> result = null;
        try {
            result = commentDAO.readByNewsId(newsId);
        } catch (DAOException e) {
            throw new ServiceException(e.getMessage());
        }
        return result;
    }

    @Override
    public void updateComment(Comment comment) throws ServiceException {
        try {
            commentDAO.update(comment);
        } catch (DAOException e) {
            throw new ServiceException(e.getMessage());
        }
    }

    @Override
    public void deleteComment(Long commentId) throws ServiceException {
        try {
            commentDAO.delete(commentId);
        } catch (DAOException e) {
            throw new ServiceException(e.getMessage());
        }
    }

    @Override
    public void deleteNewsComments(Long newsId) throws ServiceException {
        try {
            commentDAO.deleteByNewsId(newsId);
        } catch (DAOException e) {
            throw new ServiceException(e.getMessage());
        }
    }

    public void setCommentDAO(ICommentDAO commentDAO) {
        this.commentDAO = commentDAO;
    }
}
