package com.epam.newsmanagement.dao;

import com.epam.newsmanagement.entity.Comment;
import com.epam.newsmanagement.exception.DAOException;

import java.util.List;

/**
 * <p>Represents {@link Comment} DAO operations</p>
 *
 * @author Dzmitry Anikeichanka
 */
public interface ICommentDAO extends IBaseDAO<Comment, Long> {

    /**
     * <p>Receives {@link List} of comments by News ID from the database</p>
     *
     * @param newsId News ID which comments must be received by
     * @return {@link List} of comments
     * @throws DAOException
     */
    List<Comment> readByNewsId(Long newsId) throws DAOException;

    /**
     * <p>Removes {@link Comment} entity from the database by News ID</p>
     *
     * @param newsId News ID which comment must be deleted by
     * @throws DAOException
     */
    void deleteByNewsId(Long newsId) throws DAOException;
}
