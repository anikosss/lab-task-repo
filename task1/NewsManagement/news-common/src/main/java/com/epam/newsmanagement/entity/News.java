package com.epam.newsmanagement.entity;

import java.io.Serializable;
import java.sql.Date;

/**
 * <p>Represents News entity</p>
 *
 * @author Dzmitry Anikeichanka
 */
public class News implements Serializable {
	private static final long serialVersionUID = 3730118153869302872L;
	private long newsId;
    private String title;
    private String shortText;
    private String fullText;
    private Date creationDate;
    private Date modificationDate;

    public News() {
    }

    public News(long newsId, String title, String shortText, String fullText, Date creationDate, Date modificationDate) {
        this.newsId = newsId;
        this.title = title;
        this.shortText = shortText;
        this.fullText = fullText;
        this.creationDate = creationDate;
        this.modificationDate = modificationDate;
    }

    public long getNewsId() {
        return newsId;
    }

    public void setNewsId(long newsId) {
        this.newsId = newsId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getShortText() {
        return shortText;
    }

    public void setShortText(String shortText) {
        this.shortText = shortText;
    }

    public String getFullText() {
        return fullText;
    }

    public void setFullText(String fullText) {
        this.fullText = fullText;
    }

    public Date getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(Date creationDate) {
        this.creationDate = creationDate;
    }

    public Date getModificationDate() {
        return modificationDate;
    }

    public void setModificationDate(Date modificationDate) {
        this.modificationDate = modificationDate;
    }

    @Override
    public String toString() {
        StringBuilder result = new StringBuilder("News{\n");
        result.append("\tnewsId = ").append(newsId);
        result.append(",\n\ttitle = ").append(title);
        result.append(",\n\tshortText = ").append(shortText);
        result.append(",\n\tfullText = ").append(fullText);
        result.append(",\n\tcreationDate = ").append(creationDate);
        result.append(",\n\tmodificationDate = ").append(modificationDate);
        result.append(",");
        return result.toString();
    }

    @Override
    public int hashCode() {
        int result = (int) (newsId ^ (newsId >>> 32));
        result = 31 * result + title.hashCode();
        result = 31 * result + shortText.hashCode();
        result = 31 * result + fullText.hashCode();
        result = 31 * result + creationDate.hashCode();
        result = 31 * result + modificationDate.hashCode();
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (this == obj) {
            return true;
        }
        if (obj.getClass() != getClass()) {
            return false;
        }
        News news = (News) obj;
        if (newsId != news.newsId) {
            return false;
        }
        if (!title.equals(news.title)) {
            return false;
        }
        if (!shortText.equals(news.shortText)) {
            return false;
        }
        if (!fullText.equals(news.fullText)) {
            return false;
        }
        if (!creationDate.equals(news.creationDate)) {
            return false;
        }
        return modificationDate.equals(news.modificationDate);
    }
}
