package com.epam.newsmanagement.service;

import com.epam.newsmanagement.dao.IAuthorDAO;
import com.epam.newsmanagement.entity.Author;
import com.epam.newsmanagement.exception.ServiceException;

import java.util.List;

/**
 * <p>Represents service for {@link IAuthorDAO}</p>
 *
 * @author Dzmitry Anikeichanka
 */
public interface IAuthorService {

    /**
     * <p>Adds new Author into the database and returns the added entity ID</p>
     *
     * @param author author entity that must be added
     * @return {@link Long} added entity ID
     * @throws ServiceException
     */
    Long addAuthor(Author author) throws ServiceException;

    /**
     * <p>Adds news author by addition author ID and news ID into the NEWS_AUTHORS table</p>
     *
     * @param newsId news ID that must be inserted into the table
     * @param authorId author ID that must be inserted into the table
     * @return {@link Long} added row ID
     * @throws ServiceException
     */
    Long addNewsAuthor(Long newsId, Long authorId) throws ServiceException;

    /**
     * <p>Receives author by ID</p>
     *
     * @param authorId author ID which entity must be received by
     * @return {@link Author} received entity
     * @throws ServiceException
     */
    Author getAuthorById(Long authorId) throws ServiceException;

    /**
     * <p>Receives author by news ID</p>
     *
     * @param newsId news ID which entity must be received by
     * @return {@link Author} received entity
     * @throws ServiceException
     */
    Author getAuthorByNewsId(Long newsId) throws ServiceException;

    /**
     * <p>Receives all authors</p>
     *
     * @return {@link List} received entities
     * @throws ServiceException
     */
    List<Author> getAllAuthors() throws ServiceException;

    /**
     * <p>Updates author entity</p>
     *
     * @param author author entity that must be updated
     * @throws ServiceException
     */
    void updateAuthor(Author author) throws ServiceException;

    /**
     * <p>Updates author for news</p>
     *
     * @param newsId news ID which author entity must be updated
     * @param authorId author ID which must be a new author for the news
     * @throws ServiceException
     */
    void updateNewsAuthor(Long newsId, Long authorId) throws ServiceException;

    /**
     * <p>Removes author by ID</p>
     *
     * @param authorId author ID which entity must be removed by
     * @throws ServiceException
     */
    void deleteAuthorById(Long authorId) throws ServiceException;

    /**
     * <p>Removes author for the news by news ID</p>
     *
     * @param newsId news ID which author must be removed
     * @throws ServiceException
     */
    void deleteNewsAuthor(Long newsId) throws ServiceException;

    /**
     * <p>Sets author expired</p>
     *
     * @param authorId author ID which must be set expired
     * @throws ServiceException
     */
    void setExpired(Long authorId) throws ServiceException;
}
