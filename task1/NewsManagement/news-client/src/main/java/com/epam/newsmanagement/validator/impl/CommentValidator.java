package com.epam.newsmanagement.validator.impl;

import com.epam.newsmanagement.exception.ValidateException;
import com.epam.newsmanagement.message.Attributes;
import com.epam.newsmanagement.message.ErrorMessages;
import com.epam.newsmanagement.validator.IRequestValidator;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletRequest;

/**
 * <p>Represents validator for comment</p>
 */
@Component
public class CommentValidator implements IRequestValidator {

    private CommentValidator() {}

    @Override
    public boolean validateRequest(HttpServletRequest request) throws ValidateException {
        String comment = request.getParameter(Attributes.COMMENT);
        if (comment == null || comment.isEmpty()) {
            request.setAttribute(Attributes.ERROR, ErrorMessages.WRONG_COMMENT);
            throw new ValidateException(ErrorMessages.WRONG_COMMENT);
        }
        return true;
    }
}
