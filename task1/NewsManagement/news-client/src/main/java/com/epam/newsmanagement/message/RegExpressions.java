package com.epam.newsmanagement.message;

/**
 * <p>Represent class for constant regular expressions</p>
 */
public final class RegExpressions {
    public static final String NUMBER_REGEX = "(?m)^[1-9]\\d*";
}
