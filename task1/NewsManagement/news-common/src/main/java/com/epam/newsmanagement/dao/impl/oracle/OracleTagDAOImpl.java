package com.epam.newsmanagement.dao.impl.oracle;

import com.epam.newsmanagement.dao.ITagDAO;
import com.epam.newsmanagement.entity.Tag;
import com.epam.newsmanagement.exception.DAOException;
import com.epam.newsmanagement.message.DAOErrorMessages;
import com.epam.newsmanagement.message.TableRowNames;
import com.epam.newsmanagement.util.DAOUtil;
import org.springframework.jdbc.datasource.DataSourceUtils;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * <p>Represents realization {@link ITagDAO} for Oracle database</p>
 *
 * @author Dzmitry Anikeichanka
 */
public class OracleTagDAOImpl implements ITagDAO {
    private static final String SQL_CREATE = "INSERT INTO TAGS (TAGS.tag_id_pk, TAGS.tag_name) VALUES (TAGS_SEQ.nextval, ?)";
    private static final String SQL_CREATE_NEWS_TAG = "INSERT INTO NEWS_TAGS (NEWS_TAGS.news_tag_id_pk, " +
            "NEWS_TAGS.nt_news_id, nt_tag_id) VALUES (NEWS_TAGS_SEQ.nextval, ?, ?)";
    private static final String SQL_READ = "SELECT TAGS.tag_id_pk, TAGS.tag_name FROM TAGS WHERE TAGS.tag_id_pk = ?";
    private static final String SQL_READ_ALL = "SELECT TAGS.tag_id_pk, TAGS.tag_name FROM TAGS";
    private static final String SQL_READ_BY_NEWS_ID = "SELECT TAGS.tag_id_pk, TAGS.tag_name " +
            "FROM NEWS_TAGS INNER JOIN TAGS ON NEWS_TAGS.nt_tag_id = TAGS.tag_id_pk WHERE NEWS_TAGS.nt_news_id = ?";
    private static final String SQL_UPDATE = "UPDATE TAGS SET TAGS.tag_name = ? WHERE TAGS.tag_id_pk = ?";
    private static final String SQL_DELETE = "DELETE FROM TAGS WHERE TAGS.tag_id_pk = ?";
    private static final String SQL_DELETE_NEWS_TAG = "DELETE FROM NEWS_TAGS WHERE (NEWS_TAGS.nt_news_id = ? " +
            "AND NEWS_TAGS.nt_tag_id = ?)";
    private static final String SQL_DELETE_ALL_TAGS_BY_NEWS_ID = "DELETE FROM NEWS_TAGS WHERE NEWS_TAGS.nt_news_id = ?";
    private DataSource dataSource;

    @Override
    public Long create(Tag tag) throws DAOException {
        Long result = null;
        Connection connection = null;
        PreparedStatement statement = null;
        ResultSet resultSet = null;
        try {
            connection = DataSourceUtils.getConnection(dataSource);
            statement = connection.prepareStatement(SQL_CREATE, new String[]{ TableRowNames.TAG_ID_PK });
            statement.setString(1, tag.getName());
            statement.executeUpdate();
            resultSet = statement.getGeneratedKeys();
            if (resultSet.next()) {
                result = resultSet.getLong(1);
            } else {
                throw new DAOException(DAOErrorMessages.CREATE_TAG + tag);
            }
        } catch (SQLException e) {
            throw new DAOException(e.getMessage() + ". " + DAOErrorMessages.CREATE_TAG + tag);
        } finally {
            DAOUtil.releaseResources(dataSource, connection, statement, resultSet);
        }
        return result;
    }

    @Override
    public Tag read(Long tagId) throws DAOException {
        Tag result = null;
        Connection connection = null;
        PreparedStatement statement = null;
        ResultSet resultSet = null;
        try {
            connection = DataSourceUtils.getConnection(dataSource);
            statement = connection.prepareStatement(SQL_READ);
            statement.setLong(1, tagId);
            resultSet = statement.executeQuery();
            if (resultSet.next()) {
                result = buildTag(resultSet);
            }
        } catch (SQLException e) {
            throw new DAOException(e.getMessage() + ". " + String.format(DAOErrorMessages.READ_TAG, tagId));
        } finally {
            DAOUtil.releaseResources(dataSource, connection, statement, resultSet);
        }
        return result;
    }

    @Override
    public List<Tag> readAll() throws DAOException {
        List<Tag> result = null;
        Connection connection = null;
        PreparedStatement statement = null;
        ResultSet resultSet = null;
        try {
            connection = DataSourceUtils.getConnection(dataSource);
            statement = connection.prepareStatement(SQL_READ_ALL);
            resultSet = statement.executeQuery();
            result = new ArrayList<>();
            while (resultSet.next()) {
                Tag tag = buildTag(resultSet);
                result.add(tag);
            }
        } catch (SQLException e) {
            throw new DAOException(e.getMessage() + ". " + DAOErrorMessages.READ_ALL_TAGS);
        } finally {
            DAOUtil.releaseResources(dataSource, connection, statement, resultSet);
        }
        return result;
    }

    @Override
    public void update(Tag tag) throws DAOException {
        Connection connection = null;
        PreparedStatement statement = null;
        try {
            connection = DataSourceUtils.getConnection(dataSource);
            statement = connection.prepareStatement(SQL_UPDATE);
            statement.setString(1, tag.getName());
            statement.setLong(2, tag.getTagId());
            statement.executeUpdate();
        } catch (SQLException e) {
            throw new DAOException(e.getMessage() + ". " + DAOErrorMessages.UPDATE_TAG + tag);
        } finally {
            DAOUtil.releaseResources(dataSource, connection, statement);
        }
    }

    @Override
    public void delete(Long tagId) throws DAOException {
        Connection connection = null;
        PreparedStatement statement = null;
        try {
            connection = DataSourceUtils.getConnection(dataSource);
            statement = connection.prepareStatement(SQL_DELETE);
            statement.setLong(1, tagId);
            statement.executeUpdate();
        } catch (SQLException e) {
            throw new DAOException(e.getMessage() + ". " + String.format(DAOErrorMessages.DELETE_TAG, tagId));
        } finally {
            DAOUtil.releaseResources(dataSource, connection, statement);
        }
    }

    @Override
    public List<Tag> readByNewsId(Long newsId) throws DAOException {
        List<Tag> result = null;
        Connection connection = null;
        PreparedStatement statement = null;
        ResultSet resultSet = null;
        try {
            connection = DataSourceUtils.getConnection(dataSource);
            statement = connection.prepareStatement(SQL_READ_BY_NEWS_ID);
            statement.setLong(1, newsId);
            resultSet = statement.executeQuery();
            result = new ArrayList<>();
            while (resultSet.next()) {
                Tag tag = buildTag(resultSet);
                result.add(tag);
            }
        } catch (SQLException e) {
            throw new DAOException(e.getMessage() + ". " + String.format(DAOErrorMessages.READ_TAG_BY_NEWS_ID, newsId));
        } finally {
            DAOUtil.releaseResources(dataSource, connection, statement, resultSet);
        }
        return result;
    }

    @Override
    public Long createByNewsId(Long newsId, Long tagId) throws DAOException {
        Long result = null;
        Connection connection = null;
        PreparedStatement statement = null;
        ResultSet resultSet = null;
        try {
            connection = DataSourceUtils.getConnection(dataSource);
            statement = connection.prepareStatement(SQL_CREATE_NEWS_TAG, new String[]{ TableRowNames.NEWS_TAGS_ID_PK });
            statement.setLong(1, newsId);
            statement.setLong(2, tagId);
            statement.executeUpdate();
            resultSet = statement.getGeneratedKeys();
            if (resultSet.next()) {
                result = resultSet.getLong(1);
            } else {
                throw new DAOException(String.format(DAOErrorMessages.CREATE_NEWS_TAG, newsId, tagId));
            }
        } catch (SQLException e) {
            throw new DAOException(e.getMessage() + ". " + String.format(DAOErrorMessages.CREATE_NEWS_TAG, newsId, tagId));
        } finally {
            DAOUtil.releaseResources(dataSource, connection, statement, resultSet);
        }
        return result;
    }

    @Override
    public void deleteByNewsId(Long newsId, Long tagId) throws DAOException {
        Connection connection = null;
        PreparedStatement statement = null;
        try {
            connection = DataSourceUtils.getConnection(dataSource);
            statement = connection.prepareStatement(SQL_DELETE_NEWS_TAG);
            statement.setLong(1, newsId);
            statement.setLong(2, tagId);
            statement.executeUpdate();
        } catch (SQLException e) {
            throw new DAOException(e.getMessage() + ". " + String.format(DAOErrorMessages.DELETE_NEWS_TAG, newsId, tagId));
        } finally {
            DAOUtil.releaseResources(dataSource, connection, statement);
        }
    }

    @Override
    public void deleteAllByNewsId(Long newsId) throws DAOException {
        Connection connection = null;
        PreparedStatement statement = null;
        try {
            connection = DataSourceUtils.getConnection(dataSource);
            statement = connection.prepareStatement(SQL_DELETE_ALL_TAGS_BY_NEWS_ID);
            statement.setLong(1, newsId);
            statement.executeUpdate();
        } catch (SQLException e) {
            throw new DAOException(e.getMessage() + ". " + String.format(DAOErrorMessages.DELETE_ALL_NEWS_TAGS, newsId));
        } finally {
            DAOUtil.releaseResources(dataSource, connection, statement);
        }
    }

    /**
     * Creates Tag object from ResultSet
     *
     * @param resultSet {@link ResultSet} which data must be received from
     * @return {@link Tag} received entity
     * @throws SQLException
     */
    private Tag buildTag(ResultSet resultSet) throws SQLException {
        long id = resultSet.getLong(TableRowNames.TAG_ID_PK);
        String name = resultSet.getString(TableRowNames.TAG_NAME);
        return new Tag(id, name);
    }

    public void setDataSource(DataSource dataSource) {
        this.dataSource = dataSource;
    }
}
