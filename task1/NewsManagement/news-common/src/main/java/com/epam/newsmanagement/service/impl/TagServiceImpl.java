package com.epam.newsmanagement.service.impl;

import com.epam.newsmanagement.dao.ITagDAO;
import com.epam.newsmanagement.entity.Tag;
import com.epam.newsmanagement.exception.DAOException;
import com.epam.newsmanagement.exception.ServiceException;
import com.epam.newsmanagement.service.ITagService;

import java.util.List;

/**
 * <p>Represents realization for {@link ITagService}</p>
 *
 * @author Dzmitry Anikeichanka
 */
public class TagServiceImpl implements ITagService {
    private ITagDAO tagDAO;

    @Override
    public Long addTag(Tag tag) throws ServiceException {
        Long result = null;
        try {
            result = tagDAO.create(tag);
        } catch (DAOException e) {
            throw new ServiceException(e.getMessage());
        }
        return result;
    }

    @Override
    public Long addNewsTag(Long newsId, Long tagId) throws ServiceException {
        Long result = null;
        try {
            result = tagDAO.createByNewsId(newsId, tagId);
        } catch (DAOException e) {
            throw new ServiceException(e.getMessage());
        }
        return result;
    }

    @Override
    public Tag getTagById(Long tagId) throws ServiceException {
        Tag result = null;
        try {
            result = tagDAO.read(tagId);
        } catch (DAOException e) {
            throw new ServiceException(e.getMessage());
        }
        return result;
    }

    @Override
    public List<Tag> getAllTags() throws ServiceException {
        List<Tag> result = null;
        try {
            result = tagDAO.readAll();
        } catch (DAOException e) {
            throw new ServiceException(e.getMessage());
        }
        return result;
    }

    @Override
    public List<Tag> getTagsByNewsId(Long newsId) throws ServiceException {
        List<Tag> result = null;
        try {
            result = tagDAO.readByNewsId(newsId);
        } catch (DAOException e) {
            throw new ServiceException(e.getMessage());
        }
        return result;
    }

    @Override
    public void updateTag(Tag tag) throws ServiceException {
        try {
            tagDAO.update(tag);
        } catch (DAOException e) {
            throw new ServiceException(e.getMessage());
        }
    }

    @Override
    public void deleteTagById(Long tagId) throws ServiceException {
        try {
            tagDAO.delete(tagId);
        } catch (DAOException e) {
            throw new ServiceException(e.getMessage());
        }
    }

    @Override
    public void deleteNewsTag(Long newsId, Long tagId) throws ServiceException {
        try {
            tagDAO.deleteByNewsId(newsId, tagId);
        } catch (DAOException e) {
            throw new ServiceException(e.getMessage());
        }
    }

    @Override
    public void deleteAllNewsTags(Long newsId) throws ServiceException {
        try {
            tagDAO.deleteAllByNewsId(newsId);
        } catch (DAOException e) {
            throw new ServiceException(e.getMessage());
        }
    }

    public void setTagDAO(ITagDAO tagDAO) {
        this.tagDAO = tagDAO;
    }
}
