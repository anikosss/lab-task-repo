package com.epam.newsmanagement.message;

/**
 * <p>Represents constant values for all pages</p>
 */
public final class Pages {
    public static final String NEWS_PAGE = "/WEB-INF/jsp/news.jsp";
    public static final String VIEW_NEWS_PAGE = "/WEB-INF/jsp/view-news.jsp";
    public static final String LOGIN_PAGE = "/WEB-INF/jsp/login.jsp";
    public static final String ERROR_404_PAGE = "/WEB-INF/jsp/error404.jsp";
}
