package com.epam.newsmanagement.entity;

import java.io.Serializable;

/**
 * <p>Represents User entity</p>
 *
 * @author Dzmitry Anikeichanka
 */
public class User implements Serializable {
	private static final long serialVersionUID = -1922215964529248642L;
	private long userId;
    private String name;
    private String login;
    private String password;

    public User() {
    }

    public User(long userId, String name, String login, String password) {
        this.userId = userId;
        this.name = name;
        this.login = login;
        this.password = password;
    }

    public long getUserId() {
        return userId;
    }

    public void setUserId(long userId) {
        this.userId = userId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    @Override
    public String toString() {
        StringBuilder result = new StringBuilder("User{");
        result.append("userId = ").append(userId);
        result.append(", name = ").append(name);
        result.append(", login = ").append(login);
        result.append(", password = ").append(password);
        result.append('}');
        return result.toString();
    }

    @Override
    public int hashCode() {
        int result = (int) (userId ^ (userId >>> 32));
        result = 31 * result + name.hashCode();
        result = 31 * result + login.hashCode();
        result = 31 * result + password.hashCode();
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (this == obj) {
            return true;
        }
        if (obj.getClass() != getClass()) {
            return false;
        }
        User user = (User) obj;
        if (userId != user.userId) {
            return false;
        }
        if (!name.equals(user.name)) {
            return false;
        }
        if (!login.equals(user.login)) {
            return false;
        }
        return password.equals(user.password);
    }
}
