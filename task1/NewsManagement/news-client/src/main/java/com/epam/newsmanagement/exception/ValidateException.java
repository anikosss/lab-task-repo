package com.epam.newsmanagement.exception;

/**
 * <p>Represents custom exception for validators</p>
 */
public class ValidateException extends RuntimeException {

    public ValidateException() {
        super();
    }

    public ValidateException(String message) {
        super(message);
    }
}
