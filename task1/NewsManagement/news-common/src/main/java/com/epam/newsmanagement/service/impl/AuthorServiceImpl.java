package com.epam.newsmanagement.service.impl;

import com.epam.newsmanagement.dao.IAuthorDAO;
import com.epam.newsmanagement.entity.Author;
import com.epam.newsmanagement.exception.DAOException;
import com.epam.newsmanagement.exception.ServiceException;
import com.epam.newsmanagement.service.IAuthorService;

import java.util.List;

/**
 * <p>Represents realization for {@link IAuthorService}</p>
 *
 * @author Dzmitry Anikeichanka
 */
public class AuthorServiceImpl implements IAuthorService {
    private IAuthorDAO authorDAO;

    @Override
    public Long addAuthor(Author author) throws ServiceException {
        Long result = null;
        try {
            result = authorDAO.create(author);
        } catch (DAOException e) {
            throw new ServiceException(e.getMessage());
        }
        return result;
    }

    @Override
    public Long addNewsAuthor(Long newsId, Long authorId) throws ServiceException {
        Long result = null;
        try {
            result = authorDAO.createByNewsId(newsId, authorId);
        } catch (DAOException e) {
            throw new ServiceException(e.getMessage());
        }
        return result;
    }

    @Override
    public Author getAuthorById(Long authorId) throws ServiceException{
        Author result = null;
        try {
            result = authorDAO.read(authorId);
        } catch (DAOException e) {
            throw new ServiceException(e.getMessage());
        }
        return result;
    }

    @Override
    public Author getAuthorByNewsId(Long newsId) throws ServiceException {
        Author result = null;
        try {
            result = authorDAO.readByNewsId(newsId);
        } catch (DAOException e) {
            throw new ServiceException(e.getMessage());
        }
        return result;
    }

    @Override
    public List<Author> getAllAuthors() throws ServiceException {
        List<Author> result = null;
        try {
            result = authorDAO.readAll();
        } catch (DAOException e) {
            throw new ServiceException(e.getMessage());
        }
        return result;
    }

    @Override
    public void updateAuthor(Author author) throws ServiceException {
        try {
            authorDAO.update(author);
        } catch (DAOException e) {
            throw new ServiceException(e.getMessage());
        }
    }

    @Override
    public void updateNewsAuthor(Long newsId, Long authorId) throws ServiceException {
        try {
            authorDAO.updateNewsAuthor(newsId, authorId);
        } catch (DAOException e) {
            throw new ServiceException(e.getMessage());
        }
    }

    @Override
    public void deleteAuthorById(Long authorId) throws ServiceException {
        try {
            authorDAO.delete(authorId);
        } catch (DAOException e) {
            throw new ServiceException(e.getMessage());
        }
    }

    @Override
    public void deleteNewsAuthor(Long newsId) throws ServiceException {
        try {
            authorDAO.deleteByNewsId(newsId);
        } catch (DAOException e) {
            throw new ServiceException(e.getMessage());
        }
    }

    @Override
    public void setExpired(Long authorId) throws ServiceException {
        try {
            authorDAO.setExpired(authorId);
        } catch (DAOException e) {
            throw new ServiceException(e.getMessage());
        }
    }

    public void setAuthorDAO(IAuthorDAO authorDAO) {
        this.authorDAO = authorDAO;
    }
}
