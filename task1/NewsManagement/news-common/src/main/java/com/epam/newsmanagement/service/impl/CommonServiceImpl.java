package com.epam.newsmanagement.service.impl;

import com.epam.newsmanagement.dto.NewsDTO;
import com.epam.newsmanagement.entity.News;
import com.epam.newsmanagement.entity.Tag;
import com.epam.newsmanagement.exception.ServiceException;
import com.epam.newsmanagement.service.*;
import com.epam.newsmanagement.util.SearchCriteria;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

/**
 * <p>Represents realization for {@link ICommonService}</p>
 *
 * @author Dzmitry Anikeichanka
 */
@Transactional(propagation = Propagation.SUPPORTS, readOnly = true, rollbackFor = ServiceException.class)
public class CommonServiceImpl implements ICommonService {
    private INewsService newsService;
    private ITagService tagService;
    private IAuthorService authorService;
    private ICommentService commentService;

    @Override
    public List<NewsDTO> getAllNews() throws ServiceException {
        List<NewsDTO> result = new ArrayList<>();
        List<News> newsList = newsService.getAllNews();
        for (News news : newsList) {
            NewsDTO newsDTO = buildNewsDTO(news);
            result.add(newsDTO);
        }
        return result;
    }

    @Override
    public List<NewsDTO> searchNews(SearchCriteria searchCriteria, int start, int end) throws ServiceException {
        /*if (searchCriteria.getAuthorId() == null) {
            if (searchCriteria.getTagIdList() == null) {
                return getAllNews();
            }
            List<News> newsList = newsService.searchNewsByTags(searchCriteria.getTagIdList(), start, end);
            return buildNewsDTOList(newsList);
        }
        if (searchCriteria.getTagIdList() == null) {
            List<News> newsList = newsService.searchNewsByAuthor(searchCriteria.getAuthorId(), start, end);
            return buildNewsDTOList(newsList);
        }
        List<News> newsList = newsService.searchNewsByAuthorAndTags(searchCriteria.getAuthorId(),
                searchCriteria.getTagIdList(), start, end);*/
        List<News> newsList = newsService.searchNewsBySearchCriteria(searchCriteria, start, end);
        return buildNewsDTOList(newsList);
    }

    @Override
    public List<NewsDTO> getNewsWithRange(int start, int end) throws ServiceException {
        List<NewsDTO> result = new ArrayList<>();
        List<News> newsList = newsService.getNewsWithRange(start, end);
        for (News news : newsList) {
            NewsDTO newsDTO = buildNewsDTO(news);
            result.add(newsDTO);
        }
        return result;
    }

    @Override
    public NewsDTO getNewsById(Long newsId) throws ServiceException {
        News news = newsService.getNewsById(newsId);
        return buildNewsDTO(news);
    }

    @Override
    @Transactional(propagation = Propagation.REQUIRED, rollbackFor = ServiceException.class)
    public Long addNews(NewsDTO newsDTO) throws ServiceException {
        Long newsId = newsService.addNews(newsDTO.getNews());
        authorService.addNewsAuthor(newsId, newsDTO.getAuthor().getAuthorId());
        for (Tag tag : newsDTO.getTags()) {
            tagService.addNewsTag(newsId, tag.getTagId());
        }
        return newsId;
    }

    @Override
    @Transactional(propagation = Propagation.REQUIRED, rollbackFor = ServiceException.class)
    public void editNews(NewsDTO newsDTO) throws ServiceException {
        newsService.updateNews(newsDTO.getNews());
        long newsId = newsDTO.getNews().getNewsId();
        authorService.updateNewsAuthor(newsId, newsDTO.getAuthor().getAuthorId());
        tagService.deleteAllNewsTags(newsId);
        for (Tag tag : newsDTO.getTags()) {
            tagService.addNewsTag(newsId, tag.getTagId());
        }
    }

    @Override
    @Transactional(propagation = Propagation.REQUIRED, rollbackFor = ServiceException.class)
    public void deleteNews(Long newsId) throws ServiceException {
        commentService.deleteNewsComments(newsId);
        authorService.deleteNewsAuthor(newsId);
        tagService.deleteAllNewsTags(newsId);
        newsService.deleteNewsById(newsId);
    }

    /**
     * Creates {@link NewsDTO} object with data for news
     *
     * @param news news object which data must be received for
     * @return {@link NewsDTO} object with received data
     * @throws ServiceException
     */
    private NewsDTO buildNewsDTO(News news) throws ServiceException {
        if (news == null) {
            return null;
        }
        long newsId = news.getNewsId();
        long nextNewsId = newsService.getNextNewsId(newsId);
        long prevNewsId = newsService.getPreviousNewsId(newsId);
        NewsDTO result = new NewsDTO();
        result.setNews(news);
        result.setAuthor(authorService.getAuthorByNewsId(newsId));
        result.setTags(tagService.getTagsByNewsId(newsId));
        result.setComments(commentService.getNewsComments(newsId));
        result.setNextNewsId(nextNewsId);
        result.setPrevNewsId(prevNewsId);
        return result;
    }

    /**
     * Creates list of {@link NewsDTO} objects for news
     *
     * @param newsList list of news objects which data must be received for
     * @return {@link List} of {@link NewsDTO} objects with received data
     * @throws ServiceException
     */
    private List<NewsDTO> buildNewsDTOList(List<News> newsList) {
        if (newsList == null) {
            return null;
        }
        List<NewsDTO> result = new ArrayList<>();
        for (News news : newsList) {
            NewsDTO newsDTO = buildNewsDTO(news);
            result.add(newsDTO);
        }
        return result;
    }

    public void setNewsService(INewsService newsService) {
        this.newsService = newsService;
    }

    public void setTagService(ITagService tagService) {
        this.tagService = tagService;
    }

    public void setAuthorService(IAuthorService authorService) {
        this.authorService = authorService;
    }

    public void setCommentService(ICommentService commentService) {
        this.commentService = commentService;
    }
}
