package com.epam.newsmanagement.parser.impl;

import com.epam.newsmanagement.exception.ParserException;
import com.epam.newsmanagement.message.Attributes;
import com.epam.newsmanagement.parser.IRequestParser;
import com.epam.newsmanagement.util.SearchCriteria;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.List;

/**
 * <p>Represent parser for parsing request into {@link SearchCriteria} object</p>
 */
@Component("searchCriteriaParser")
public class SearchCriteriaParser implements IRequestParser<SearchCriteria> {

    private SearchCriteriaParser() {}

    @Override
    public SearchCriteria parseRequest(HttpServletRequest request) throws ParserException {
        SearchCriteria result;
        String authorParam = request.getParameter(Attributes.AUTHOR);
        Long authorId = 0L;
        if (authorParam != null && !authorParam.isEmpty()) {
            authorId = Long.parseLong(authorParam);
        }
        String[] tagsParams = request.getParameterValues(Attributes.TAGS);
        List<Long> tagIdList = new ArrayList<>();
        if (tagsParams != null) {
            for (String tagParam : tagsParams) {
                Long tagId = Long.parseLong(tagParam);
                tagIdList.add(tagId);
            }
        }
        result = new SearchCriteria();
        if (authorId != 0) {
            result.setAuthorId(authorId);
        }
        if (tagIdList.size() != 0) {
            result.setTagIdList(tagIdList);
        }
        return result;
    }
}
