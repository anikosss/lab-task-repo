package com.epam.newsmanagement.dto;

import com.epam.newsmanagement.entity.Author;
import com.epam.newsmanagement.entity.Comment;
import com.epam.newsmanagement.entity.News;
import com.epam.newsmanagement.entity.Tag;

import java.io.Serializable;
import java.util.List;

/**
 * <p>Represents Transfer Object for {@link News} entity</p>
 *
 * @author Dzmitry Anikeichanka
 */
public class NewsDTO implements Serializable {
	private static final long serialVersionUID = -3553918779112348765L;
	private News news;
    private Author author;
    private List<Tag> tags;
    private List<Comment> comments;
    private long nextNewsId;
    private long prevNewsId;

    public NewsDTO() {}

    public NewsDTO(News news, Author author, List<Tag> tags) {
        this.news = news;
        this.author = author;
        this.tags = tags;
    }

    public NewsDTO(News news, Author author, List<Tag> tags, List<Comment> comments, long nextNewsId, long prevNewsId) {
        this.news = news;
        this.author = author;
        this.tags = tags;
        this.comments = comments;
        this.nextNewsId = nextNewsId;
        this.prevNewsId = prevNewsId;
    }

    public News getNews() {
        return news;
    }

    public Author getAuthor() {
        return author;
    }

    public List<Tag> getTags() {
        return tags;
    }

    public List<Comment> getComments() {
        return comments;
    }

    public void setNews(News news) {
        this.news = news;
    }

    public void setAuthor(Author author) {
        this.author = author;
    }

    public void setTags(List<Tag> tags) {
        this.tags = tags;
    }

    public void setComments(List<Comment> comments) {
        this.comments = comments;
    }

    @Override
    public int hashCode() {
        int result = news.hashCode();
        result = 31 * result + author.hashCode();
        result = 31 * result + tags.hashCode();
        result = 31 * result + comments.hashCode();
        result = 31 * result + (int) (nextNewsId ^ (nextNewsId >>> 32));
        result = 31 * result + (int) (prevNewsId ^ (prevNewsId >>> 32));
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (this == obj) {
            return true;
        }
        if (obj.getClass() != getClass()) {
            return false;
        }
        NewsDTO newsDTO = (NewsDTO) obj;
        if (!news.equals(newsDTO.news)) {
            return false;
        }
        if (!author.equals(newsDTO.author)) {
            return false;
        }
        if (!tags.equals(newsDTO.tags)) {
            return false;
        }
        if (nextNewsId != newsDTO.nextNewsId) {
            return false;
        }
        if (prevNewsId != newsDTO.prevNewsId) {
            return false;
        }
        return comments.equals(newsDTO.comments);

    }

    @Override
    public String toString() {
        final StringBuilder result = new StringBuilder("NewsDTO{\n");
        result.append("\tnews = ").append(news);
        result.append(",\n\tauthor = ").append(author);
        result.append(",\n\ttags = ").append(tags);
        result.append(",\n\tcomments = ").append(comments);
        result.append(",\n\tnextNewsId = ").append(nextNewsId);
        result.append(",\n\tprevNewsId = ").append(prevNewsId);
        result.append("}\n");
        return result.toString();
    }

    public long getNextNewsId() {
        return nextNewsId;
    }

    public void setNextNewsId(long nextNewsId) {
        this.nextNewsId = nextNewsId;
    }

    public long getPrevNewsId() {
        return prevNewsId;
    }

    public void setPrevNewsId(long prevNewsId) {
        this.prevNewsId = prevNewsId;
    }
}
