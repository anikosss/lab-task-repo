package com.epam.newsmanagement.command.impl;

import com.epam.newsmanagement.command.IActionCommand;
import com.epam.newsmanagement.dto.NewsDTO;
import com.epam.newsmanagement.exception.CommandException;
import com.epam.newsmanagement.message.Attributes;
import com.epam.newsmanagement.message.Pages;
import com.epam.newsmanagement.parser.IRequestParser;
import com.epam.newsmanagement.service.ICommonService;
import com.epam.newsmanagement.service.INewsService;
import com.epam.newsmanagement.util.MethodEnum;
import com.epam.newsmanagement.util.ResponseHolder;
import com.epam.newsmanagement.util.SearchCriteria;
import com.epam.newsmanagement.validator.IRequestValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * <p>Represents implementation for command that provide filtering news</p>
 */
@Component
public class SearchCommand implements IActionCommand {
    private static final int NEWS_PER_PAGE = 6;
    @Autowired
    private ICommonService commonService;
    @Autowired
    private INewsService newsService;
    @Autowired
    @Qualifier("searchCriteriaParser")
    private IRequestParser<SearchCriteria> searchCriteriaParser;
    @Autowired
    @Qualifier("pageValidator")
    private IRequestValidator pageValidator;

    @Override
    public ResponseHolder execute(HttpServletRequest request) throws CommandException {
        ResponseHolder result = new ResponseHolder();
        SearchCriteria searchCriteria = searchCriteriaParser.parseRequest(request);
        pageValidator.validateRequest(request);
        int page = (int) request.getAttribute(Attributes.PAGE);
        int newsCount = newsService.getNewsCountBySearchCriteria(searchCriteria);
        int pagesCount = (int) Math.ceil((double) newsCount / NEWS_PER_PAGE);
        int range[] = defineRange(page);
        List<NewsDTO> newsList = commonService.searchNews(searchCriteria, range[0], range[1]);
        Map<String, String> params = processParams(searchCriteria);
        request.setAttribute(Attributes.NEWS_LIST, newsList);
        request.setAttribute(Attributes.ACTION, Attributes.SEARCH);
        request.setAttribute(Attributes.PARAMS, params);
        request.setAttribute(Attributes.PAGES_COUNT, pagesCount);
        request.setAttribute(Attributes.PAGE, page);
        result.setPage(Pages.NEWS_PAGE);
        result.setMethod(MethodEnum.FORWARD);
        return result;
    }

    /**
     * <p>Defines the range of news according to page</p>
     *
     * @param page page that determines the range
     * @return array of 2 ints - (1) for first index of range and (2) for last index
     */
    private int[] defineRange(int page) {
        int[] result = new int[2];
        result[0] = (page - 1) * NEWS_PER_PAGE + 1;
        result[1] = result [0] + NEWS_PER_PAGE - 1;
        return result;
    }

    /**
     * <p>Processes the request by putting them into the map</p>
     *
     * @param searchCriteria {@link SearchCriteria} object that determines parameters
     * @return {@link Map} with parameters
     */
    private Map<String, String> processParams(SearchCriteria searchCriteria) {
        Map<String, String> result = new HashMap<>();
        if (searchCriteria.getAuthorId() != null) {
            result.put(Attributes.AUTHOR, searchCriteria.getAuthorId().toString());
        }
        if (searchCriteria.getTagIdList() != null) {
            for (Long tagId : searchCriteria.getTagIdList()) {
                result.put(Attributes.TAGS, tagId.toString());
            }
        }
        return result;
    }
}
