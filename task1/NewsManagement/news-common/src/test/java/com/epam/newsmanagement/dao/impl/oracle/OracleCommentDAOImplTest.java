package com.epam.newsmanagement.dao.impl.oracle;

import com.epam.newsmanagement.dao.ICommentDAO;
import com.epam.newsmanagement.entity.Comment;
import com.github.springtestdbunit.DbUnitTestExecutionListener;
import com.github.springtestdbunit.annotation.DatabaseOperation;
import com.github.springtestdbunit.annotation.DatabaseSetup;
import com.github.springtestdbunit.annotation.DatabaseTearDown;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.springframework.test.context.support.DirtiesContextTestExecutionListener;
import org.springframework.test.context.transaction.TransactionalTestExecutionListener;

import java.sql.Date;
import java.util.List;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath:test-context.xml"})
@DatabaseSetup(value = "classpath:test-dataset.xml")
@DatabaseTearDown(value = "classpath:test-dataset.xml", type = DatabaseOperation.DELETE_ALL)
@TestExecutionListeners({ DependencyInjectionTestExecutionListener.class,
        DirtiesContextTestExecutionListener.class,
        TransactionalTestExecutionListener.class,
        DbUnitTestExecutionListener.class })
public class OracleCommentDAOImplTest extends Assert {
    @Autowired
    private ICommentDAO commentDAO;

    @Test
    public void testRead() throws Exception {
        Comment comment = commentDAO.read(2L);
        assertEquals("text2", comment.getText());
    }

    @Test
    public void testReadByNewsId() throws Exception {
        List<Comment> comments = commentDAO.readByNewsId(2L);
        assertEquals(1, comments.size());
        assertEquals("text2", comments.get(0).getText());
    }

    @Test
    public void testCreate() throws Exception {
        Comment comment = new Comment(1L, "text", new Date(System.currentTimeMillis()), 1L);
        Long id = commentDAO.create(comment);
        Comment dbComment = commentDAO.read(id);
        assertNotNull(id);
        assertEquals(comment.getText(), dbComment.getText());
    }

    @Test
    public void testReadAll() throws Exception {
        List<Comment> comments = commentDAO.readAll();
        assertEquals(4, comments.size());
    }

    @Test
    public void testUpdate() throws Exception {
        Comment comment = commentDAO.read(2L);
        comment.setText("newText");
        commentDAO.update(comment);
        comment = commentDAO.read(2L);
        assertEquals("newText", comment.getText());
    }

    @Test
    public void testDelete() throws Exception {
        commentDAO.delete(3L);
        Comment comment = commentDAO.read(3L);
        assertNull(comment);
    }

    @Test
    public void testDeleteByNewsId() throws Exception {
        commentDAO.deleteByNewsId(3L);
        List<Comment> comments = commentDAO.readByNewsId(3L);
        assertEquals(0, comments.size());
    }
    
}