package com.epam.newsmanagement.service.impl;

import com.epam.newsmanagement.dao.ITagDAO;
import com.epam.newsmanagement.entity.Tag;
import com.epam.newsmanagement.service.ITagService;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath:test-context.xml"})
public class TagServiceImplTest extends Assert {
    @Autowired
    @InjectMocks
    private ITagService tagService;
    @Mock
    private ITagDAO tagDAO;

    @Before
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void testReadTagById() throws Exception {
        when(tagDAO.read(3L)).thenReturn(buildTag(3L));
        Tag tag = tagService.getTagById(3L);
        verify(tagDAO).read(3L);
        assertEquals("name3", tag.getName());
    }

    @Test
    public void testReadTagByWrongId() throws Exception {
        when(tagDAO.read(11L)).thenReturn(null);
        Tag tag = tagService.getTagById(11L);
        verify(tagDAO).read(11L);
        assertNull(tag);
    }

    @Test
    public void testAddNewTag() throws Exception {
        Tag tag = buildTag(3L);
        when(tagDAO.create(tag)).thenReturn(10L);
        when(tagDAO.read(10L)).thenReturn(tag);
        Long id = tagService.addTag(tag);
        Tag result = tagService.getTagById(id);
        verify(tagDAO).create(tag);
        verify(tagDAO).read(id);
        assertEquals("name3", result.getName());
    }

    @Test
    public void testAddNewsTag() throws Exception {
        when(tagDAO.readByNewsId(3L)).thenReturn(new ArrayList<Tag>(Arrays.asList(buildTag(2L), buildTag(3L))));
        tagService.addNewsTag(3L, 2L);
        tagService.addNewsTag(3L, 3L);
        List<Tag> tags = tagService.getTagsByNewsId(3L);
        verify(tagDAO).createByNewsId(3L, 2L);
        verify(tagDAO).createByNewsId(3L, 3L);
        verify(tagDAO).readByNewsId(3L);
        assertEquals(2, tags.size());
    }

    @Test
    public void testReadAllTags() throws Exception {
        when(tagDAO.readAll()).thenReturn(buildTagsList(6));
        List<Tag> tags = tagService.getAllTags();
        verify(tagDAO).readAll();
        assertEquals(6, tags.size());
    }

    @Test
    public void testUpdateTag() throws Exception {
        Tag tag = buildTag(2L);
        when(tagDAO.read(2L)).thenReturn(new Tag(2L, "newName"));
        tagService.updateTag(tag);
        Tag result = tagService.getTagById(2L);
        verify(tagDAO).update(tag);
        verify(tagDAO).read(2L);
        assertEquals("newName", result.getName());
    }

    @Test
    public void testDeleteNewsTag() throws Exception {
        when(tagDAO.readByNewsId(2L)).thenReturn(new ArrayList<>(Arrays.asList(buildTag(1L), buildTag(2L))));
        tagService.deleteNewsTag(2L, 3L);
        List<Tag> tags = tagService.getTagsByNewsId(2L);
        verify(tagDAO).deleteByNewsId(2L, 3L);
        assertEquals(2, tags.size());
    }

    @Test
    public void testDeleteTagById() throws Exception {
        when(tagDAO.read(3L)).thenReturn(null);
        tagService.deleteTagById(3L);
        Tag tag = tagService.getTagById(3L);
        verify(tagDAO).delete(3L);
        assertNull(tag);
    }

    @Test
    public void testDeleteAllNewsTags() throws Exception {
        when(tagDAO.readByNewsId(2L)).thenReturn(new ArrayList<>());
        tagService.deleteAllNewsTags(2L);
        List<Tag> tags = tagService.getTagsByNewsId(2L);
        verify(tagDAO).deleteAllByNewsId(2L);
        assertEquals(0, tags.size());
    }

    private Tag buildTag(long id) {
        return new Tag(id, "name" + id);
    }

    private List<Tag> buildTagsList(int size) {
        List<Tag> result = new ArrayList<>();
        for (int i = 0; i < size; i++) {
            result.add(buildTag((long) size));
        }
        return result;
    }
}