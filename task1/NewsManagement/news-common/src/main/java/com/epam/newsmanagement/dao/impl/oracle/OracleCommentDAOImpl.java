package com.epam.newsmanagement.dao.impl.oracle;

import com.epam.newsmanagement.dao.ICommentDAO;
import com.epam.newsmanagement.entity.Comment;
import com.epam.newsmanagement.exception.DAOException;
import com.epam.newsmanagement.message.DAOErrorMessages;
import com.epam.newsmanagement.message.TableRowNames;
import com.epam.newsmanagement.util.DAOUtil;
import org.springframework.jdbc.datasource.DataSourceUtils;

import javax.sql.DataSource;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;

/**
 * <p>Represents realization {@link ICommentDAO} for Oracle database</p>
 *
 * @author Dzmitry Anikeichanka
 */
public class OracleCommentDAOImpl implements ICommentDAO {
    private static final String SQL_CREATE = "INSERT INTO COMMENTS (comment_id_pk, com_text, com_creation_date, com_news_id) " +
            "VALUES (COMMENTS_SEQ.nextval, ?, ?, ?)";
    private static final String SQL_READ = "SELECT * FROM COMMENTS WHERE comment_id_pk = ?";
    private static final String SQL_READ_BY_NEWS_ID = "SELECT COMMENTS.comment_id_pk, COMMENTS.com_text, " +
            "COMMENTS.com_creation_date, COMMENTS.com_news_id FROM COMMENTS INNER JOIN NEWS ON " +
            "COMMENTS.com_news_id = NEWS.news_id_pk WHERE COMMENTS.com_news_id = ?";
    private static final String SQL_READ_ALL = "SELECT * FROM COMMENTS";
    private static final String SQL_UPDATE = "UPDATE COMMENTS SET com_text = ?, com_creation_date = ?, " +
            "com_news_id = ? WHERE comment_id_pk = ?";
    private static final String SQL_DELETE = "DELETE FROM COMMENTS WHERE comment_id_pk = ?";
    private static final String SQL_DELETE_ALL_COMMENTS_BY_NEWS_ID = "DELETE FROM COMMENTS WHERE COMMENTS.com_news_id = ?";
    private DataSource dataSource;

    @Override
    public Long create(Comment comment) throws DAOException {
        Long result = null;
        Connection connection = null;
        PreparedStatement statement = null;
        ResultSet resultSet = null;
        try {
            connection = DataSourceUtils.getConnection(dataSource);
            statement = connection.prepareStatement(SQL_CREATE, new String[]{ TableRowNames.COMMENT_ID_PK });
            statement.setString(1, comment.getText());
            statement.setDate(2, comment.getCreationDate());
            statement.setLong(3, comment.getNewsId());
            statement.executeUpdate();
            resultSet = statement.getGeneratedKeys();
            if (resultSet.next()) {
                result = resultSet.getLong(1);
            } else {
                throw new DAOException(DAOErrorMessages.CREATE_COMMENT + comment);
            }
        } catch (SQLException e) {
            throw new DAOException(e.getMessage() + ". " + DAOErrorMessages.CREATE_COMMENT + comment);
        } finally {
            DAOUtil.releaseResources(dataSource, connection, statement, resultSet);
        }
        return result;
    }

    @Override
    public Comment read(Long commentId) throws DAOException {
        Comment result = null;
        Connection connection = null;
        PreparedStatement statement = null;
        ResultSet resultSet = null;
        try {
            connection = DataSourceUtils.getConnection(dataSource);
            statement = connection.prepareStatement(SQL_READ);
            statement.setLong(1, commentId);
            resultSet = statement.executeQuery();
            if (resultSet.next()) {
                result = buildComment(resultSet);
            }
        } catch (SQLException e) {
            throw new DAOException(e.getMessage() + ". " + String.format(DAOErrorMessages.READ_COMMENT, commentId));
        } finally {
            DAOUtil.releaseResources(dataSource, connection, statement, resultSet);
        }
        return result;
    }

    @Override
    public List<Comment> readAll() throws DAOException {
        List<Comment> result = null;
        Connection connection = null;
        PreparedStatement statement = null;
        ResultSet resultSet = null;
        try {
            connection = DataSourceUtils.getConnection(dataSource);
            statement = connection.prepareStatement(SQL_READ_ALL);
            resultSet = statement.executeQuery();
            result = new ArrayList<>();
            while (resultSet.next()) {
                Comment comment = buildComment(resultSet);
                result.add(comment);
            }
        } catch (SQLException e) {
            throw new DAOException(e.getMessage() + ". " + DAOErrorMessages.READ_ALL_COMMENTS);
        } finally {
            DAOUtil.releaseResources(dataSource, connection, statement, resultSet);
        }
        return result;
    }

    @Override
    public void update(Comment comment) throws DAOException {
        Connection connection = null;
        PreparedStatement statement = null;
        try {
            connection = DataSourceUtils.getConnection(dataSource);
            statement = connection.prepareStatement(SQL_UPDATE);
            statement.setString(1, comment.getText());
            statement.setDate(2, comment.getCreationDate());
            statement.setLong(3, comment.getNewsId());
            statement.setLong(4, comment.getCommentId());
            statement.executeUpdate();
        } catch (SQLException e) {
            throw new DAOException(e.getMessage() + ". " + DAOErrorMessages.UPDATE_COMMENT + comment);
        } finally {
            DAOUtil.releaseResources(dataSource, connection, statement);
        }
    }

    @Override
    public void delete(Long commentId) throws DAOException {
        Connection connection = null;
        PreparedStatement statement = null;
        try {
            connection = DataSourceUtils.getConnection(dataSource);
            statement = connection.prepareStatement(SQL_DELETE);
            statement.setLong(1, commentId);
            statement.executeUpdate();
        } catch (SQLException e) {
            throw new DAOException(e.getMessage() + ". " + DAOErrorMessages.DELETE_COMMENT + commentId);
        } finally {
            DAOUtil.releaseResources(dataSource, connection, statement);
        }
    }

    @Override
    public List<Comment> readByNewsId(Long newsId) throws DAOException {
        List<Comment> result = null;
        Connection connection = null;
        PreparedStatement statement = null;
        ResultSet resultSet = null;
        try {
            connection = DataSourceUtils.getConnection(dataSource);
            statement = connection.prepareStatement(SQL_READ_BY_NEWS_ID);
            statement.setLong(1, newsId);
            resultSet = statement.executeQuery();
            result = new ArrayList<>();
            while (resultSet.next()) {
                Comment comment = buildComment(resultSet);
                result.add(comment);
            }
        } catch (SQLException e) {
            throw new DAOException(e.getMessage() + ". " + String.format(DAOErrorMessages.READ_COMMENT_BY_NEWS_ID, newsId));
        } finally {
            DAOUtil.releaseResources(dataSource, connection, statement, resultSet);
        }
        return result;
    }

    @Override
    public void deleteByNewsId(Long newsId) throws DAOException {
        Connection connection = null;
        PreparedStatement statement = null;
        try {
            connection = DataSourceUtils.getConnection(dataSource);
            statement = connection.prepareStatement(SQL_DELETE_ALL_COMMENTS_BY_NEWS_ID);
            statement.setLong(1, newsId);
            statement.executeUpdate();
        } catch (SQLException e) {
            throw new DAOException(e.getMessage() + ". " + String.format(DAOErrorMessages.DELETE_ALL_NEWS_COMMENTS, newsId));
        } finally {
            DAOUtil.releaseResources(dataSource, connection, statement);
        }
    }
    /**
     * Creates Comment object from ResultSet
     *
     * @param resultSet {@link ResultSet} which data must be received from
     * @return {@link Comment} received entity
     * @throws SQLException
     */
    private Comment buildComment(ResultSet resultSet) throws SQLException {
        long id = resultSet.getLong(TableRowNames.COMMENT_ID_PK);
        String text = resultSet.getString(TableRowNames.COMMENT_TEXT);
        Date creationDate = resultSet.getDate(TableRowNames.COMMENT_CREATION_DATE);
        long newsId = resultSet.getLong(TableRowNames.COMMENT_NEWS_ID);
        return new Comment(id, text, creationDate, newsId);
    }

    public void setDataSource(DataSource dataSource) {
        this.dataSource = dataSource;
    }
}
