package com.epam.newsmanagement.validator;

import com.epam.newsmanagement.exception.ValidateException;

import javax.servlet.http.HttpServletRequest;

/**
 * <p>Represents interface for validators</p>
 */
public interface IRequestValidator {

    /**
     * <p>Validates the request</p>
     *
     * @param request {@link HttpServletRequest} that should be validated
     * @return true if request is valid, false - otherwise
     * @throws ValidateException
     */
    boolean validateRequest(HttpServletRequest request) throws ValidateException;
}
