package com.epam.newsmanagement.dao;

import com.epam.newsmanagement.entity.Tag;
import com.epam.newsmanagement.exception.DAOException;

import java.util.List;

/**
 * <p>Represents {@link Tag} DAO operations</p>
 *
 * @author Dzmitry Anikeichanka
 */
public interface ITagDAO extends IBaseDAO<Tag, Long> {

    /**
     * <p>Inserts News ID and Tag ID into the NEWS_TAGS table in the database</p>
     *
     * @param newsId News ID which must be inserted
     * @param tagId Tag ID which must be inserted
     * @return inserted row ID
     * @throws DAOException
     */
    Long createByNewsId(Long newsId, Long tagId) throws DAOException;

    /**
     * <p>Receives {@link List} of tags from the database by News ID</p>
     *
     * @param newsId News ID which entity must be received by
     * @return {@link List} of tags
     * @throws DAOException
     */
    List<Tag> readByNewsId(Long newsId) throws DAOException;

    /**
     * <p>Removes tag by Tag ID and News ID from the NEWS_TAGS table in the database</p>
     *
     * @param newsId News ID which must be removed from the table
     * @param tagId tag ID which must be removed from the table
     * @throws DAOException
     */
    void deleteByNewsId(Long newsId, Long tagId) throws DAOException;

    /**
     * <p>Removes all Tag IDs by News ID from the NEWS_TAGS table in the database</p>
     *
     * @param newsId News ID which all Tag IDs must be removed from the table
     * @throws DAOException
     */
    void deleteAllByNewsId(Long newsId) throws DAOException;
}
