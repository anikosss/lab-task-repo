package com.epam.newsmanagement.security;

import com.epam.newsmanagement.entity.User;
import com.epam.newsmanagement.service.IUserService;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.*;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath:test-context.xml"})
public class UserDetailsServiceImplTest extends Assert {
    @InjectMocks
	@Autowired
	private UserDetailsService userDetailsService;
    @Mock
    private IUserService userService;

    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
    }

	@Test
	public void testLoadUserByUsername() {
		when(userService.getUserByLogin(anyString())).thenReturn(new User(0L, "name", "login", "password"));
		UserDetails userDetails = userDetailsService.loadUserByUsername("name");
		verify(userService).getUserByLogin(anyString());
		assertEquals("login", userDetails.getUsername());
	}

}
