package com.epam.newsmanagement.util;

import com.epam.newsmanagement.message.Pages;

/**
 * <p>Represents class for holding the request from command tier to servlet that defines page and method</p>
 */
public class ResponseHolder {
    private MethodEnum method = MethodEnum.FORWARD;
    private String page = Pages.ERROR_404_PAGE;

    public ResponseHolder() {}

    public ResponseHolder(MethodEnum method) {
        this.method = method;
    }

    public ResponseHolder(String page) {
        this.page = page;
    }

    public ResponseHolder(MethodEnum method, String page) {
        this.method = method;
        this.page = page;
    }

    public MethodEnum getMethod() {
        return method;
    }

    public void setMethod(MethodEnum method) {
        this.method = method;
    }

    public String getPage() {
        return page;
    }

    public void setPage(String page) {
        this.page = page;
    }

}
