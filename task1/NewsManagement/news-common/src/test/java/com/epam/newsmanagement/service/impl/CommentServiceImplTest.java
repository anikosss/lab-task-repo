package com.epam.newsmanagement.service.impl;

import com.epam.newsmanagement.dao.ICommentDAO;
import com.epam.newsmanagement.entity.Comment;
import com.epam.newsmanagement.service.ICommentService;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.sql.Date;
import java.util.ArrayList;
import java.util.List;

import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath:test-context.xml"})
public class CommentServiceImplTest extends Assert {
    @Autowired
    @InjectMocks
    private ICommentService commentService;
    @Mock
    private ICommentDAO commentDAO;

    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void testReadCommentById() throws Exception {
        when(commentDAO.read(2L)).thenReturn(buildComment(2L));
        Comment comment = commentService.getCommentById(2L);
        verify(commentDAO).read(2L);
        assertEquals("text2", comment.getText());
    }

    @Test
    public void testReadCommentByWrongId() throws Exception {
        when(commentDAO.read(15L)).thenReturn(null);
        Comment comment = commentService.getCommentById(15L);
        verify(commentDAO).read(15L);
        assertNull(comment);
    }

    @Test
    public void testAddNewComment() throws Exception {
        Comment comment = buildComment(3L);
        when(commentDAO.create(comment)).thenReturn(10L);
        when(commentDAO.read(10L)).thenReturn(comment);
        Long id = commentService.addComment(comment);
        Comment result = commentService.getCommentById(id);
        verify(commentDAO).create(comment);
        verify(commentDAO).read(id);
        assertEquals("text3", result.getText());
    }

    @Test
    public void testReadAllComments() throws Exception {
        List<Comment> comments = buildCommentsList(4);
        when(commentDAO.readAll()).thenReturn(comments);
        List<Comment> result = commentService.getAllComments();
        verify(commentDAO).readAll();
        assertEquals(4, result.size());
    }

    @Test
    public void testUpdateComment() throws Exception {
        Comment comment = buildComment(2L);
        when(commentDAO.read(2L)).thenReturn(new Comment(2L, "newText", new Date(System.currentTimeMillis()), 2L));
        commentService.updateComment(comment);
        Comment result = commentService.getCommentById(2L);
        verify(commentDAO).update(comment);
        verify(commentDAO).read(2L);
        assertEquals("newText", result.getText());
    }

    @Test
    public void testDeleteCommitById() throws Exception {
        when(commentDAO.read(5L)).thenReturn(null);
        commentService.deleteComment(5L);
        Comment comment = commentService.getCommentById(5L);
        verify(commentDAO).delete(5L);
        verify(commentDAO).read(5L);
        assertNull(comment);
    }

    @Test
    public void testDeleteByNewsId() throws Exception {
        when(commentDAO.readByNewsId(3L)).thenReturn(null);
        commentService.deleteNewsComments(3L);
        List<Comment> comments = commentService.getNewsComments(3L);
        verify(commentDAO).deleteByNewsId(3L);
        verify(commentDAO).readByNewsId(3L);
        assertNull(comments);
    }

    private Comment buildComment(long id) {
        return new Comment(id, "text" + id, new Date(System.currentTimeMillis()), id);
    }

    private List<Comment> buildCommentsList(int size) {
        List<Comment> result = new ArrayList<>();
        for (int i = 0; i < size; i++) {
            result.add(buildComment((long) i));
        }
        return result;
    }
    
}