<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<header>
    <!-- Fixed header navbar -->
    <div class="navbar navbar-default navbar-fixed-top" role="navigation">
        <div class="container">
            <div class="navbar-header">
                <a class="navbar-brand" href="news-client/controller?action=news&page=1">
                    <span class="nav-header">NEWS PORTAL</span>
                </a>
            </div>
            <ul class="nav navbar-nav navbar-right">
                <li><a href="#" class="nav-lang">RU</a></li>
                <li><a href="#" class="nav-lang">EN</a></li>
                <li><a href="admin/logout" class="nav-logout">Logout</a></li>
            </ul>
        </div>
    </div><!-- Fixed header navbar -->
</header>
<br><br><br><br>