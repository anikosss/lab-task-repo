<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<div class="row">
    <form action="controller" method="GET">
        <input type="hidden" name="action" value="search">
        <div class="col-md-offset-2 col-md-3">
            <div class="form-group">
                <label for="author-select">Select author:</label>
                <select name="author" class="form-control" id="author-select">
                    <option value="0"></option>
                    <c:forEach items="${authors}" var="author">
                        <option value="${author.authorId}">${author.name}</option>
                    </c:forEach>
                </select>
            </div>
        </div>
        <div class="col-md-3">
            <div class="form-group">
                <label for="tags-select">Select tags:</label>
                <select name="tags" class="form-control" id="tags-select" multiple="multiple">
                    <option value="0"></option>
                    <c:forEach items="${tags}" var="tag">
                        <option value="${tag.tagId}">
                                ${tag.name}
                        </option>
                    </c:forEach>
                </select>
            </div>
        </div>
        <div class="col-md-2">
            <button type="submit" class="btn btn-default search" id="filter-btn">Filter</button>
            <a href="controller?action=news&page=1">Reset</a>
        </div>
        <input type="hidden" name="page" value="1" />
    </form>
    <br><br><br><br><br><br>
</div>
