package com.epam.newsmanagement.dao.impl.oracle;

import com.epam.newsmanagement.dao.IAuthorDAO;
import com.epam.newsmanagement.entity.Author;
import com.epam.newsmanagement.exception.DAOException;
import com.epam.newsmanagement.message.DAOErrorMessages;
import com.epam.newsmanagement.message.TableRowNames;
import com.epam.newsmanagement.util.DAOUtil;
import org.springframework.jdbc.datasource.DataSourceUtils;

import javax.sql.DataSource;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;

/**
 * <p>Represents realization {@link IAuthorDAO} for Oracle database</p>
 *
 * @author Dzmitry Anikeichanka
 */
public class OracleAuthorDAOImpl implements IAuthorDAO {
    private static final String SQL_CREATE = "INSERT INTO AUTHORS (AUTHORS.author_id_pk, AUTHORS.au_name, " +
            "AUTHORS.au_expired) VALUES (AUTHORS_SEQ.nextval, ?, ?)";
    private static final String SQL_CREATE_NEWS_AUTHOR = "INSERT INTO NEWS_AUTHORS (NEWS_AUTHORS.news_author_id_pk, " +
            "NEWS_AUTHORS.na_news_id, NEWS_AUTHORS.na_author_id) VALUES(NEWS_AUTHORS_SEQ.nextval, ?, ?)";
    private static final String SQL_READ = "SELECT AUTHORS.author_id_pk, AUTHORS.au_name, AUTHORS.au_expired " +
            "FROM AUTHORS WHERE AUTHORS.author_id_pk = ?";
    private static final String SQL_READ_BY_NEWS_ID = "SELECT AUTHORS.author_id_pk, AUTHORS.au_name, AUTHORS.au_expired " +
            "FROM NEWS_AUTHORS INNER JOIN AUTHORS ON NEWS_AUTHORS.na_author_id = AUTHORS.author_id_pk " +
            "WHERE NEWS_AUTHORS.na_news_id = ?";
    private static final String SQL_READ_ALL = "SELECT AUTHORS.author_id_pk, AUTHORS.au_name, AUTHORS.au_expired " +
            "FROM AUTHORS";
    private static final String SQL_UPDATE = "UPDATE AUTHORS SET AUTHORS.au_name = ?, AUTHORS.au_expired = ? " +
            "WHERE AUTHORS.author_id_pk = ?";
    private static final String SQL_UPDATE_NEWS_AUTHOR = "UPDATE NEWS_AUTHORS SET NEWS_AUTHORS.na_author_id = ? " +
            "WHERE NEWS_AUTHORS.na_news_id = ?";
    private static final String SQL_DELETE = "DELETE FROM AUTHORS WHERE AUTHORS.author_id_pk = ?";
    private static final String SQL_DELETE_BY_NEWS_ID = "DELETE FROM NEWS_AUTHORS WHERE NEWS_AUTHORS.na_news_id = ?";
    private static final String SQL_SET_EXPIRED = "UPDATE AUTHORS SET AUTHORS.au_expired = CURRENT_TIMESTAMP " +
            "WHERE AUTHORS.author_id_pk = ?";
    private DataSource dataSource;

    @Override
    public Long create(Author author) throws DAOException {
        Long result = null;
        Connection connection = null;
        PreparedStatement statement = null;
        ResultSet resultSet = null;
        try {
            connection = DataSourceUtils.getConnection(dataSource);
            statement = connection.prepareStatement(SQL_CREATE, new String[]{ TableRowNames.AUTHOR_ID_PK });
            statement.setString(1, author.getName());
            if (author.getExpired() != null) {
                statement.setDate(2, author.getExpired());
            } else {
                statement.setNull(2, Types.TIMESTAMP);
            }
            statement.executeUpdate();
            resultSet = statement.getGeneratedKeys();
            if (resultSet.next()) {
                result = resultSet.getLong(1);
            } else {
                throw new DAOException(DAOErrorMessages.CREATE_AUTHOR + author);
            }
        } catch (SQLException e) {
            throw new DAOException(e.getMessage() + ". " + DAOErrorMessages.CREATE_AUTHOR + author);
        } finally {
            DAOUtil.releaseResources(dataSource, connection, statement, resultSet);
        }
        return result;
    }

    @Override
    public Author read(Long authorId) throws DAOException {
        Author result = null;
        Connection connection = null;
        PreparedStatement statement = null;
        ResultSet resultSet = null;
        try {
            connection = DataSourceUtils.getConnection(dataSource);
            statement = connection.prepareStatement(SQL_READ);
            statement.setLong(1, authorId);
            resultSet = statement.executeQuery();
            if (resultSet.next()) {
                result = buildAuthor(resultSet);
            }
        } catch (SQLException e) {
            throw new DAOException(e.getMessage() + ". " + String.format(DAOErrorMessages.READ_AUTHOR, authorId));
        } finally {
            DAOUtil.releaseResources(dataSource, connection, statement, resultSet);
        }
        return result;
    }

    @Override
    public List<Author> readAll() throws DAOException {
        List<Author> result = null;
        Connection connection = null;
        PreparedStatement statement = null;
        ResultSet resultSet = null;
        try {
            connection = DataSourceUtils.getConnection(dataSource);
            statement = connection.prepareStatement(SQL_READ_ALL);
            resultSet = statement.executeQuery();
            result = new ArrayList<>();
            while (resultSet.next()) {
                Author author = buildAuthor(resultSet);
                result.add(author);
            }
        } catch (SQLException e) {
            throw new DAOException(e.getMessage() + ". " + DAOErrorMessages.READ_ALL_AUTHORS);
        } finally {
            DAOUtil.releaseResources(dataSource, connection, statement, resultSet);
        }
        return result;
    }

    @Override
    public void update(Author author) throws DAOException {
        Connection connection = null;
        PreparedStatement statement = null;
        try {
            connection = DataSourceUtils.getConnection(dataSource);
            statement = connection.prepareStatement(SQL_UPDATE);
            statement.setString(1, author.getName());
            if (author.getExpired() != null) {
                statement.setDate(2, author.getExpired());
            } else {
                statement.setNull(2, Types.TIMESTAMP);
            }
            statement.setLong(3, author.getAuthorId());
            statement.executeUpdate();
        } catch (SQLException e) {
            throw new DAOException(e.getMessage() + ". " + DAOErrorMessages.UPDATE_AUTHOR + author);
        } finally {
            DAOUtil.releaseResources(dataSource, connection, statement);
        }
    }

    @Override
    public void delete(Long authorId) throws DAOException {
        Connection connection = null;
        PreparedStatement statement = null;
        try {
            connection = DataSourceUtils.getConnection(dataSource);
            statement = connection.prepareStatement(SQL_DELETE);
            statement.setLong(1, authorId);
            statement.executeUpdate();
        } catch (SQLException e) {
            throw new DAOException(e.getMessage() + ". " + String.format(DAOErrorMessages.DELETE_AUTHOR, authorId));
        } finally {
            DAOUtil.releaseResources(dataSource, connection, statement);
        }
    }

    @Override
    public Author readByNewsId(Long newsId) throws DAOException {
        Author result = null;
        Connection connection = null;
        PreparedStatement statement = null;
        ResultSet resultSet = null;
        try {
            connection = DataSourceUtils.getConnection(dataSource);
            statement = connection.prepareStatement(SQL_READ_BY_NEWS_ID);
            statement.setLong(1, newsId);
            resultSet = statement.executeQuery();
            if (resultSet.next()) {
                result = buildAuthor(resultSet);
            }
        } catch (SQLException e) {
            throw new DAOException(e.getMessage() + ". " + String.format(DAOErrorMessages.READ_AUTHOR_BY_NEWS_ID, newsId));
        } finally {
            DAOUtil.releaseResources(dataSource, connection, statement, resultSet);
        }
        return result;
    }

    @Override
    public void updateNewsAuthor(Long newsId, Long authorId) throws DAOException {
        Connection connection = null;
        PreparedStatement statement = null;
        try {
            connection = DataSourceUtils.getConnection(dataSource);
            statement = connection.prepareStatement(SQL_UPDATE_NEWS_AUTHOR);
            statement.setLong(1, authorId);
            statement.setLong(2, newsId);
            statement.executeUpdate();
        } catch (SQLException e) {
            throw new DAOException(e.getMessage() + ". " +
                    String.format(DAOErrorMessages.UPDATE_NEWS_AUTHOR, newsId, authorId));
        } finally {
            DAOUtil.releaseResources(dataSource, connection, statement);
        }
    }

    @Override
    public Long createByNewsId(Long newsId, Long authorId) throws DAOException {
        Long result = null;
        Connection connection = null;
        PreparedStatement statement = null;
        ResultSet resultSet = null;
        try {
            connection = DataSourceUtils.getConnection(dataSource);
            statement = connection.prepareStatement(SQL_CREATE_NEWS_AUTHOR, new String[]{ TableRowNames.NEWS_AUTHORS_ID_PK });
            statement.setLong(1, newsId);
            statement.setLong(2, authorId);
            statement.executeUpdate();
            resultSet = statement.getGeneratedKeys();
            if (resultSet.next()) {
                result = resultSet.getLong(1);
            } else {
                throw new DAOException(DAOErrorMessages.CREATE_NEWS_AUTHOR + newsId + ", " + authorId);
            }
        } catch (SQLException e) {
            throw new DAOException(e.getMessage() + ". " +
                    String.format(DAOErrorMessages.CREATE_NEWS_AUTHOR, newsId, authorId));
        } finally {
            DAOUtil.releaseResources(dataSource, connection, statement, resultSet);
        }
        return result;
    }

    @Override
    public void deleteByNewsId(Long newsId) throws DAOException {
        Connection connection = null;
        PreparedStatement statement = null;
        try {
            connection = DataSourceUtils.getConnection(dataSource);
            statement = connection.prepareStatement(SQL_DELETE_BY_NEWS_ID);
            statement.setLong(1, newsId);
            statement.executeUpdate();
        } catch (SQLException e) {
            throw new DAOException(e.getMessage() + ". " + String.format(DAOErrorMessages.DELETE_NEWS_AUTHOR, newsId));
        } finally {
            DAOUtil.releaseResources(dataSource, connection, statement);
        }
    }

    @Override
    public void setExpired(Long authorId) throws DAOException {
        Connection connection = null;
        PreparedStatement statement = null;
        try {
            connection = DataSourceUtils.getConnection(dataSource);
            statement = connection.prepareStatement(SQL_SET_EXPIRED);
            statement.setLong(1, authorId);
            statement.executeUpdate();
        } catch (SQLException e) {
            throw new DAOException(e.getMessage() + ". " + String.format(DAOErrorMessages.SET_AUTHOR_EXPIRED, authorId));
        } finally {
            DAOUtil.releaseResources(dataSource, connection, statement);
        }
    }

    /**
     * Creates Author object from ResultSet
     *
     * @param resultSet {@link ResultSet} which data must be received from
     * @return {@link Author} received entity
     * @throws SQLException
     */
    private Author buildAuthor(ResultSet resultSet) throws SQLException {
        long id = resultSet.getLong(TableRowNames.AUTHOR_ID_PK);
        String name = resultSet.getString(TableRowNames.AUTHOR_NAME);
        Date expired = resultSet.getDate(TableRowNames.AUTHOR_EXPIRED);
        return new Author(id, name, expired);
    }

    public void setDataSource(DataSource dataSource) {
        this.dataSource = dataSource;
    }

}
