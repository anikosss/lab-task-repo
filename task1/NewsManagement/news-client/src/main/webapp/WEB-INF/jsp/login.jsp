<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!doctype html>
<html lang="en">
<jsp:include page="common/head.jsp" />
<body>
<jsp:include page="common/header.jsp" />
<div class="container">
    <c:if test="${not empty error}">
        <div class="col-md-offset-4 col-md-4">
            <div class="alert alert-danger alert-header fade in">
                <a href="#" class="close" data-dismiss="alert" area-label="close">&times;</a>
                ${error}.
            </div>
        </div>
    </c:if>
    <br><br><br><br><br>
    <div class="row">
        <div class="col-md-offset-3 col-md-6">
            <form class="login-form" action="controller" method="POST">
                <div class="form-group">
                    <label for="login">Login</label>
                    <input type="text" name="login" class="form-control" id="login" placeholder="Login">
                </div>
                <div class="form-group">
                    <label for="password">Password</label>
                    <input type="password" name="password" class="form-control" id="password" placeholder="Password">
                </div>
                <input type="hidden" name="action" value="login">
                <button type="submit" class="btn btn-default">Submit</button>
            </form>
        </div>
    </div>
</div>
<jsp:include page="common/footer.jsp" />
</body>
</html>
