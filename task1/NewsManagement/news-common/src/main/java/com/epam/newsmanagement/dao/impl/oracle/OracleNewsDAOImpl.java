package com.epam.newsmanagement.dao.impl.oracle;

import com.epam.newsmanagement.dao.INewsDAO;
import com.epam.newsmanagement.entity.News;
import com.epam.newsmanagement.exception.DAOException;
import com.epam.newsmanagement.message.DAOErrorMessages;
import com.epam.newsmanagement.message.TableRowNames;
import com.epam.newsmanagement.util.DAOUtil;
import com.epam.newsmanagement.util.SearchCriteria;
import org.springframework.jdbc.datasource.DataSourceUtils;

import javax.sql.DataSource;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;

/**
 * <p>Represents realization {@link INewsDAO} for Oracle database</p>
 *
 * @author Dzmitry Anikeichanka
 */
public class OracleNewsDAOImpl implements INewsDAO {
    private static final String SQL_CREATE = "INSERT INTO NEWS (news_id_pk, nw_title, nw_short_text, nw_full_text, " +
            "nw_creation_date, nw_modification_date) VALUES (NEWS_SEQ.nextval, ?, ?, ?, ?, ?)";
    private static final String SQL_READ = "SELECT NEWS.news_id_pk, NEWS.nw_title, NEWS.nw_short_text, NEWS.nw_full_text, " +
            "NEWS.nw_creation_date, NEWS.nw_modification_date FROM NEWS WHERE NEWS.news_id_pk = ?";
    private static final String SQL_READ_NEXT = "SELECT nw_next_news from (SELECT news_id_pk, LEAD(news_id_pk, 1, 0) " +
            "OVER(ORDER BY comments_count DESC, nw_modification_date DESC) AS nw_next_news FROM (SELECT NEWS.news_id_pk, " +
            "NEWS.nw_title, NEWS.nw_short_text, NEWS.nw_full_text, NEWS.nw_creation_date, NEWS.nw_modification_date, " +
            "COUNT(COMMENTS.comment_id_pk) AS comments_count FROM NEWS LEFT JOIN COMMENTS ON NEWS.news_id_pk = " +
            "COMMENTS.com_news_id GROUP BY NEWS.news_id_pk, NEWS.nw_title, NEWS.nw_short_text, NEWS.nw_full_text, " +
            "NEWS.nw_creation_date, NEWS.nw_modification_date ORDER BY comments_count DESC, NEWS.nw_modification_date)) " +
            "WHERE news_id_pk = ?";
    private static final String SQL_READ_PREV = "SELECT nw_prev_news from (SELECT news_id_pk, LAG(news_id_pk, 1, 0) " +
            "OVER(ORDER BY comments_count DESC, nw_modification_date DESC) AS nw_prev_news FROM (SELECT NEWS.news_id_pk, " +
            "NEWS.nw_title, NEWS.nw_short_text, NEWS.nw_full_text, NEWS.nw_creation_date, NEWS.nw_modification_date, " +
            "COUNT(COMMENTS.comment_id_pk) AS comments_count FROM NEWS LEFT JOIN COMMENTS ON NEWS.news_id_pk = " +
            "COMMENTS.com_news_id GROUP BY NEWS.news_id_pk, NEWS.nw_title, NEWS.nw_short_text, NEWS.nw_full_text, " +
            "NEWS.nw_creation_date, NEWS.nw_modification_date ORDER BY comments_count DESC, NEWS.nw_modification_date)) " +
            "WHERE news_id_pk = ?";
    private static final String SQL_READ_ALL = "SELECT NEWS.news_id_pk, NEWS.nw_title, NEWS.nw_short_text, NEWS.nw_full_text, " +
            "NEWS.nw_creation_date, NEWS.nw_modification_date, COUNT(COMMENTS.comment_id_pk) AS comments_count " +
            "FROM NEWS LEFT JOIN COMMENTS ON NEWS.news_id_pk = COMMENTS.com_news_id " +
            "GROUP BY NEWS.news_id_pk, NEWS.nw_title, NEWS.nw_short_text, NEWS.nw_full_text, " +
            "NEWS.nw_creation_date, NEWS.nw_modification_date ORDER BY comments_count DESC, NEWS.nw_modification_date DESC";
    private static final String SQL_UPDATE = "UPDATE NEWS SET NEWS.nw_title = ?, NEWS.nw_short_text = ?, " +
            "NEWS.nw_full_text = ?, NEWS.nw_creation_date = ?, NEWS.nw_modification_date = ? WHERE NEWS.news_id_pk = ?";
    private static final String SQL_DELETE = "DELETE FROM NEWS WHERE NEWS.news_id_pk = ?";
    private static final String SQL_COUNT = "SELECT COUNT(*) AS count FROM NEWS";
    private static final String SQL_SEARCH_BY_AUTHOR = "SELECT NEWS.news_id_pk, NEWS.nw_title, NEWS.nw_short_text, " +
            "NEWS.nw_full_text, NEWS.nw_creation_date, NEWS.nw_modification_date, COUNT(NEWS.news_id_pk) as nw_count " +
            "FROM COMMENTS INNER JOIN NEWS ON COMMENTS.com_news_id = NEWS.news_id_pk " +
            "WHERE NEWS.news_id_pk IN (SELECT NEWS_AUTHORS.na_news_id FROM NEWS_AUTHORS " +
            "WHERE NEWS_AUTHORS.na_author_id = ?) ";
    private static final String SEARCH_COMMON_PART = " GROUP BY NEWS.news_id_pk, NEWS.nw_title, " +
            "NEWS.nw_short_text, NEWS.nw_full_text, NEWS.nw_creation_date, NEWS.nw_modification_date " +
            "ORDER BY nw_count DESC, NEWS.nw_modification_date DESC";
    private DataSource dataSource;

    @Override
    public Long create(News news) throws DAOException {
        Long result = null;
        Connection connection = null;
        PreparedStatement statement = null;
        ResultSet resultSet = null;
        try {
            connection = DataSourceUtils.getConnection(dataSource);
            statement = connection.prepareStatement(SQL_CREATE, new String[]{ TableRowNames.NEWS_ID_PK });
            statement.setString(1, news.getTitle());
            statement.setString(2, news.getShortText());
            statement.setString(3, news.getFullText());
            statement.setDate(4, news.getCreationDate());
            statement.setDate(5, news.getModificationDate());
            statement.executeUpdate();
            resultSet = statement.getGeneratedKeys();
            if (resultSet.next()) {
                result = resultSet.getLong(1);
            } else {
                throw new DAOException(DAOErrorMessages.CREATE_NEWS + news);
            }
        } catch (SQLException e) {
            throw new DAOException(e.getMessage() + ". " + DAOErrorMessages.CREATE_NEWS + news);
        } finally {
            DAOUtil.releaseResources(dataSource, connection, statement, resultSet);
        }
        return result;
    }

    @Override
    public News read(Long newsId) throws DAOException {
        News result = null;
        Connection connection = null;
        PreparedStatement statement = null;
        ResultSet resultSet = null;
        try {
            connection = DataSourceUtils.getConnection(dataSource);
            statement = connection.prepareStatement(SQL_READ);
            statement.setLong(1, newsId);
            resultSet = statement.executeQuery();
            if (resultSet.next()) {
                result = buildNews(resultSet);
            }
        } catch (SQLException e) {
            throw new DAOException(e.getMessage() + ". " + String.format(DAOErrorMessages.READ_NEWS, newsId));
        } finally {
            DAOUtil.releaseResources(dataSource, connection, statement, resultSet);
        }
        return result;
    }

    @Override
    public List<News> readAll() throws DAOException {
        List<News> result = null;
        Connection connection = null;
        PreparedStatement statement = null;
        ResultSet resultSet = null;
        try {
            connection = DataSourceUtils.getConnection(dataSource);
            statement = connection.prepareStatement(SQL_READ_ALL);
            resultSet = statement.executeQuery();
            result = new ArrayList<>();
            while (resultSet.next()) {
                News news = buildNews(resultSet);
                result.add(news);
            }
        } catch (SQLException e) {
            throw new DAOException(e.getMessage() + ". " + DAOErrorMessages.READ_ALL_NEWS);
        } finally {
            DAOUtil.releaseResources(dataSource, connection, statement, resultSet);
        }
        return result;
    }

    @Override
    public void update(News news) throws DAOException {
        Connection connection = null;
        PreparedStatement statement = null;
        try {
            connection = DataSourceUtils.getConnection(dataSource);
            statement = connection.prepareStatement(SQL_UPDATE);
            statement.setString(1, news.getTitle());
            statement.setString(2, news.getShortText());
            statement.setString(3, news.getFullText());
            statement.setDate(4, news.getCreationDate());
            statement.setDate(5, news.getModificationDate());
            statement.setLong(6, news.getNewsId());
            statement.executeUpdate();
        } catch (SQLException e) {
            throw new DAOException(e.getMessage() + ". " + DAOErrorMessages.UPDATE_NEWS + news);
        } finally {
            DAOUtil.releaseResources(dataSource, connection, statement);
        }
    }

    @Override
    public void delete(Long newsId) throws DAOException {
        Connection connection = null;
        PreparedStatement statement = null;
        try {
            connection = DataSourceUtils.getConnection(dataSource);
            statement = connection.prepareStatement(SQL_DELETE);
            statement.setLong(1, newsId);
            statement.executeUpdate();
        } catch (SQLException e) {
            throw new DAOException(e.getMessage() + ". " + String.format(DAOErrorMessages.DELETE_NEWS, newsId));
        } finally {
            DAOUtil.releaseResources(dataSource, connection, statement);
        }
    }

    @Override
    public int getAllNewsCount() throws DAOException {
        int result = 0;
        Connection connection = null;
        PreparedStatement statement = null;
        ResultSet resultSet = null;
        try {
            connection = DataSourceUtils.getConnection(dataSource);
            statement = connection.prepareStatement(SQL_COUNT);
            resultSet = statement.executeQuery();
            if (resultSet.next()) {
                result = resultSet.getInt(TableRowNames.NEWS_COUNT);
            }
        } catch (SQLException e) {
            throw new DAOException(e.getMessage() + ". " + DAOErrorMessages.NEWS_COUNT);
        } finally {
            DAOUtil.releaseResources(dataSource, connection, statement, resultSet);
        }
        return result;
    }

    @Override
    public int getNewsCountBySearchCriteria(SearchCriteria searchCriteria) throws DAOException {
        int result = 0;
        Connection connection = null;
        PreparedStatement statement = null;
        ResultSet resultSet = null;
        try {
            if (searchCriteria.getAuthorId() == null && searchCriteria.getTagIdList() == null) {
                return getAllNewsCount();
            }
            connection = DataSourceUtils.getConnection(dataSource);
            String query = defineQuery(searchCriteria);
            statement = connection.prepareStatement(wrapWithCount(query));
            processParams(searchCriteria, statement);
            resultSet = statement.executeQuery();
            if (resultSet.next()) {
                result = resultSet.getInt("news_count");
            }
        } catch (SQLException e) {
            throw new DAOException(e.getMessage() + ". " + DAOErrorMessages.READ_NEWS_COUNT_BY_SEARCH_CRITERIA + searchCriteria);
        } finally {
            DAOUtil.releaseResources(dataSource, connection, statement, resultSet);
        }
        return result;
    }

    @Override
    public List<News> searchByAuthor(Long authorId, int start, int end) throws DAOException {
        List<News> result = null;
        Connection connection = null;
        PreparedStatement statement = null;
        ResultSet resultSet = null;
        try {
            connection = DataSourceUtils.getConnection(dataSource);
            statement = connection.prepareStatement(wrapWithRange(SQL_SEARCH_BY_AUTHOR + SEARCH_COMMON_PART));
            statement.setLong(1, authorId);
            statement.setInt(2, start);
            statement.setInt(3, end);
            resultSet = statement.executeQuery();
            result = new ArrayList<>();
            while (resultSet.next()) {
                News news = buildNews(resultSet);
                result.add(news);
            }
        } catch (SQLException e) {
            throw new DAOException(e.getMessage() + ". " +
                    String.format(DAOErrorMessages.SEARCH_NEWS_BY_AUTHOR, authorId));
        } finally {
            DAOUtil.releaseResources(dataSource, connection, statement, resultSet);
        }
        return result;
    }

    @Override
    public long readNextNewsId(long newsId) throws DAOException {
        long result = 0L;
        Connection connection = null;
        PreparedStatement statement = null;
        ResultSet resultSet = null;
        try {
            connection = DataSourceUtils.getConnection(dataSource);
            statement = connection.prepareStatement(SQL_READ_NEXT);
            statement.setLong(1, newsId);
            resultSet = statement.executeQuery();
            if (resultSet.next()) {
                result = resultSet.getLong(TableRowNames.NEWS_NEXT);
            }
        } catch (SQLException e) {
            throw new DAOException(e.getMessage() + ". " + String.format(DAOErrorMessages.READ_NEXT_NEWS, newsId));
        } finally {
            DAOUtil.releaseResources(dataSource, connection, statement, resultSet);
        }
        return result;
    }

    @Override
    public long readPreviousNewsId(long newsId) throws DAOException {
        long result = 0L;
        Connection connection = null;
        PreparedStatement statement = null;
        ResultSet resultSet = null;
        try {
            connection = DataSourceUtils.getConnection(dataSource);
            statement = connection.prepareStatement(SQL_READ_PREV);
            statement.setLong(1, newsId);
            resultSet = statement.executeQuery();
            if (resultSet.next()) {
                result = resultSet.getLong(TableRowNames.NEWS_PREVIOUS);
            }
        } catch (SQLException e) {
            throw new DAOException(e.getMessage() + ". " + String.format(DAOErrorMessages.READ_PREVIOUS_NEWS, newsId));
        } finally {
            DAOUtil.releaseResources(dataSource, connection, statement, resultSet);
        }
        return result;
    }

    @Override
    public List<News> searchBySearchCriteria(SearchCriteria searchCriteria, int start, int end) throws DAOException {
        List<News> result = null;
        Connection connection = null;
        PreparedStatement statement = null;
        ResultSet resultSet = null;
        try {
            if (searchCriteria.getAuthorId() == null && searchCriteria.getTagIdList() == null) {
                return readWithRange(start, end);
            }
            connection = DataSourceUtils.getConnection(dataSource);
            String query = defineQuery(searchCriteria);
            statement = connection.prepareStatement(wrapWithRange(query));
            int lastIndex = processParams(searchCriteria, statement);
            statement.setInt(lastIndex + 1, start);
            statement.setInt(lastIndex + 2, end);
            resultSet = statement.executeQuery();
            result = new ArrayList<>();
            while (resultSet.next()) {
                News news = buildNews(resultSet);
                result.add(news);
            }
        } catch (SQLException e) {
            throw new DAOException(e.getMessage() + ". " + DAOErrorMessages.SEARCH_NEWS_BY_SEARCH_CRITERIA + searchCriteria);
        } finally {
            DAOUtil.releaseResources(dataSource, connection, statement, resultSet);
        }
        return result;
    }

    @Override
    public List<News> searchByTagIdList(List<Long> tagIdList, int start, int end) throws DAOException {
        List<News> result = null;
        Connection connection = null;
        PreparedStatement statement = null;
        ResultSet resultSet = null;
        try {
            connection = DataSourceUtils.getConnection(dataSource);
            statement = connection.prepareStatement(wrapWithRange(buildSearchByTagsQuery(tagIdList.size())));
            int lastIndex = 0;
            for (int i = 0; i < tagIdList.size(); i++) {
                statement.setLong(i + 1, tagIdList.get(i));
                lastIndex++;
            }
            statement.setInt(lastIndex + 1, start);
            statement.setInt(lastIndex + 2, end);
            resultSet = statement.executeQuery();
            result = new ArrayList<>();
            while (resultSet.next()) {
                News news = buildNews(resultSet);
                result.add(news);
            }
        } catch (SQLException e) {
            throw new DAOException(e.getMessage() + ". " + DAOErrorMessages.SEARCH_NEWS_BY_TAGS + tagIdList);
        } finally {
            DAOUtil.releaseResources(dataSource, connection, statement, resultSet);
        }
        return result;
    }

    @Override
    public List<News> searchByAuthorAndTags(Long authorId, List<Long> tagIdList, int start, int end) throws DAOException {
        List<News> result = null;
        Connection connection = null;
        PreparedStatement statement = null;
        ResultSet resultSet = null;
        try {
            connection = DataSourceUtils.getConnection(dataSource);
            statement = connection.prepareStatement(wrapWithRange(buildSearchByAuthorAndTagsQuery(tagIdList.size())));
            statement.setLong(1, authorId);
            int lastIndex = 1;
            for (int i = 0; i < tagIdList.size(); i++) {
                statement.setLong(i + 2, tagIdList.get(i));
                lastIndex++;
            }
            statement.setInt(lastIndex + 1, start);
            statement.setInt(lastIndex + 2, end);
            resultSet = statement.executeQuery();
            result = new ArrayList<>();
            while (resultSet.next()) {
                News news = buildNews(resultSet);
                result.add(news);
            }
        } catch (SQLException e) {
            throw new DAOException(e.getMessage() + ". " +
                    String.format(DAOErrorMessages.SEARCH_NEWS_BY_AUTHOR_AND_TAGS, authorId) + tagIdList);
        } finally {
            DAOUtil.releaseResources(dataSource, connection, statement, resultSet);
        }
        return result;
    }

    @Override
    public List<News> readWithRange(int start, int end) throws DAOException {
        List<News> result = null;
        Connection connection = null;
        PreparedStatement statement = null;
        ResultSet resultSet = null;
        try {
            connection = DataSourceUtils.getConnection(dataSource);
            statement = connection.prepareStatement(wrapWithRange(SQL_READ_ALL));
            statement.setInt(1, start);
            statement.setInt(2, end);
            resultSet = statement.executeQuery();
            result = new ArrayList<>();
            while (resultSet.next()) {
                News news = buildNews(resultSet);
                result.add(news);
            }
        } catch (SQLException e) {
            throw new DAOException(e.getMessage() + ". " + String.format(DAOErrorMessages.READ_NEWS_WITH_RANGE, start, end));
        } finally {
            DAOUtil.releaseResources(dataSource, connection, statement, resultSet);
        }
        return result;
    }

    /**
     * <p>Creates News object from ResultSet</p>
     *
     * @param resultSet {@link ResultSet} which data must be received from
     * @return {@link News} received entity
     * @throws SQLException
     */
    private News buildNews(ResultSet resultSet) throws SQLException {
        long id = resultSet.getLong(TableRowNames.NEWS_ID_PK);
        String title = resultSet.getString(TableRowNames.NEWS_TITLE);
        String shortText = resultSet.getString(TableRowNames.NEWS_SHORT_TEXT);
        String fullText = resultSet.getString(TableRowNames.NEWS_FULL_TEXT);
        Date creationDate = resultSet.getDate(TableRowNames.NEWS_CREATION_DATE);
        Date modificationDate = resultSet.getDate(TableRowNames.NEWS_MODIFICATION_DATE);
        return new News(id, title, shortText, fullText, creationDate, modificationDate);
    }

    /**
     * <p>Builds query for searching by tags</p>
     *
     * @param tagsCount count of tags
     * @return {@link String} created query
     */
    private String buildSearchByTagsQuery(int tagsCount) {
        final String startPart = "SELECT NEWS.news_id_pk, NEWS.nw_title, NEWS.nw_short_text, " +
                "NEWS.nw_full_text, NEWS.nw_creation_date, NEWS.nw_modification_date, COUNT(NEWS.news_id_pk) as nw_count " +
                "FROM COMMENTS INNER JOIN NEWS ON COMMENTS.com_news_id = NEWS.news_id_pk";
        final String wherePart = " WHERE (";
        final String andPart = " AND ";
        final String searchPart = "(NEWS.news_id_pk IN (SELECT NEWS_TAGS.nt_news_id " +
                "FROM NEWS_TAGS WHERE NEWS_TAGS.nt_tag_id = ?))";
        StringBuilder result = new StringBuilder(startPart);
        if (tagsCount == 0) {
            return result.append(SEARCH_COMMON_PART).toString();
        }
        for (int i = 0; i < tagsCount; i++) {
            if (i == 0) {
                result.append(wherePart);
            }
            if (i > 0 && i < tagsCount) {
                result.append(andPart);
            }
            result.append(searchPart);
        }
        result.append(")");
        result.append(SEARCH_COMMON_PART);
        return result.toString();
    }

    /**
     * <p>Builds query for searching by author and tags</p>
     *
     * @param tagsCount count of tags
     * @return {@link String} created query
     */
    private String buildSearchByAuthorAndTagsQuery(int tagsCount) {
        final String startPart = "SELECT NEWS.news_id_pk, NEWS.nw_title, NEWS.nw_short_text, NEWS.nw_full_text, " +
                "NEWS.nw_creation_date, NEWS.nw_modification_date, COUNT(NEWS.news_id_pk) AS nw_count FROM COMMENTS " +
                "INNER JOIN NEWS ON COMMENTS.com_news_id = NEWS.news_id_pk WHERE (NEWS.news_id_pk IN (SELECT " +
                "NEWS_AUTHORS.na_news_id FROM NEWS_AUTHORS WHERE NEWS_AUTHORS.na_author_id = ?) AND NEWS.news_id_pk IN " +
                "(SELECT NEWS_TAGS.nt_news_id FROM NEWS_TAGS WHERE NEWS_TAGS.nt_tag_id = ?)";
        final String searchPart = " AND NEWS.news_id_pk IN (SELECT NEWS_TAGS.nt_news_id FROM NEWS_TAGS " +
                "WHERE NEWS_TAGS.nt_tag_id = ?)";
        StringBuilder result = new StringBuilder(startPart);
        if (tagsCount <= 1) {
            return result.append(")").append(SEARCH_COMMON_PART).toString();
        }
        for (int i = 1; i < tagsCount; i++) {
            result.append(searchPart);
        }
        result.append(")");
        result.append(SEARCH_COMMON_PART);
        return result.toString();
    }

    /**
     * <p>Defines query according to SearchCriteria</p>
     *
     * @param searchCriteria criteria that determines the result
     * @return {@link String} result query
     */
    private String defineQuery(SearchCriteria searchCriteria) {
        String result;
        if (searchCriteria.getAuthorId() == null) {
            result = buildSearchByTagsQuery(searchCriteria.getTagIdList().size());
        } else {
            if (searchCriteria.getTagIdList() == null) {
                result = SQL_SEARCH_BY_AUTHOR + SEARCH_COMMON_PART;
            } else {
                result = buildSearchByAuthorAndTagsQuery(searchCriteria.getTagIdList().size());
            }
        }
        return result;
    }

    /**
     * <p>Processes the statement by parameters from SearchCriteria</p>
     *
     * @param searchCriteria {@link SearchCriteria} that stores all parameters
     * @param statement {@link PreparedStatement} statement which would be processed
     * @return index of the last inserted parameter
     * @throws SQLException
     */
    private int processParams(SearchCriteria searchCriteria, PreparedStatement statement) throws SQLException {
        int lastParam = 0;
        if (searchCriteria.getAuthorId() == null) {
            for (int i = 0; i < searchCriteria.getTagIdList().size(); i++) {
                statement.setLong(i + 1, searchCriteria.getTagIdList().get(i));
                lastParam++;
            }
        } else {
            if (searchCriteria.getTagIdList() == null) {
                statement.setLong(1, searchCriteria.getAuthorId());
                lastParam++;
            } else {
                statement.setLong(1, searchCriteria.getAuthorId());
                lastParam++;
                for (int i = 0; i < searchCriteria.getTagIdList().size(); i++) {
                    statement.setLong(i + 2, searchCriteria.getTagIdList().get(i));
                    lastParam++;
                }
            }
        }
        return lastParam;
    }

    /**
     * <p>Wraps query into another query for receiving range of data</p>
     * @param query query that should be wrapper
     * @return {@link String} wrapped query
     */
    private String wrapWithRange(String query) {
        final String firstPart = "SELECT * FROM (SELECT ROWNUM rnum, news_rows.* FROM (";
        final String lastPart = ") news_rows) WHERE rnum BETWEEN ? AND ?";
        return firstPart + query + lastPart;
    }

    /**
     * <p>Wraps query into another query for receiving count of rows</p>
     * @param query query that should be wrapper
     * @return {@link String} wrapped query
     */
    private String wrapWithCount(String query) {
        final String firstPart = "SELECT COUNT(news_id_pk) AS news_count FROM(";
        final String lastPart = ") GROUP BY news_id_pk";
        return firstPart + query + lastPart;
    }

    public void setDataSource(DataSource dataSource) {
        this.dataSource = dataSource;
    }
}
