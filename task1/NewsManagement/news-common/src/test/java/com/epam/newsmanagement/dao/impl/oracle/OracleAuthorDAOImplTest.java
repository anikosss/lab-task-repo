package com.epam.newsmanagement.dao.impl.oracle;

import com.epam.newsmanagement.dao.IAuthorDAO;
import com.epam.newsmanagement.dto.NewsDTO;
import com.epam.newsmanagement.entity.Author;
import com.epam.newsmanagement.exception.DAOException;
import com.epam.newsmanagement.service.ICommonService;
import com.github.springtestdbunit.DbUnitTestExecutionListener;
import com.github.springtestdbunit.annotation.DatabaseOperation;
import com.github.springtestdbunit.annotation.DatabaseSetup;
import com.github.springtestdbunit.annotation.DatabaseTearDown;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.springframework.test.context.support.DirtiesContextTestExecutionListener;
import org.springframework.test.context.transaction.TransactionalTestExecutionListener;

import java.util.List;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath:test-context.xml"})
@DatabaseSetup(value = "classpath:test-dataset.xml")
@DatabaseTearDown(value = "classpath:test-dataset.xml", type = DatabaseOperation.DELETE_ALL)
@TestExecutionListeners({ DependencyInjectionTestExecutionListener.class,
        DirtiesContextTestExecutionListener.class,
        TransactionalTestExecutionListener.class,
        DbUnitTestExecutionListener.class })
public class OracleAuthorDAOImplTest extends Assert {
    @Autowired
    private IAuthorDAO authorDAO;
    @Autowired
    private ICommonService commonService;

    @Test
    public void testRead() throws Exception {
        Author author = authorDAO.read(4L);
        assertEquals("name4", author.getName());
    }

    @Test
    public void testReadByNewsId() throws Exception {
        Author author = authorDAO.readByNewsId(3L);
        assertEquals("name3", author.getName());
    }

    @Test
    public void testCreate() throws Exception {
        Author author = new Author(1, "name", null);
        Long id = authorDAO.create(author);
        Author tempAuthor = authorDAO.read(id);
        assertEquals("name", tempAuthor.getName());
    }

    @Test
    public void testAddNewsAuthor() throws Exception {
        Long id = authorDAO.createByNewsId(3L, 1L);
        assertNotNull(id);
    }

    @Test
    public void testReadAll() throws Exception {
        List<Author> authors = authorDAO.readAll();
        assertEquals(4, authors.size());
    }

    @Test
    public void testUpdate() throws Exception {
        Author author = authorDAO.read(2L);
        author.setName("newName");
        authorDAO.update(author);
        author = authorDAO.read(2L);
        assertEquals("newName", author.getName());
    }

    @Test
    public void testUpdateNewsAuthor() throws Exception {
        authorDAO.updateNewsAuthor(2L, 3L);
        NewsDTO newsDTO = commonService.getNewsById(2L);
        assertEquals(3L, newsDTO.getAuthor().getAuthorId());
    }

    @Test(expected = DAOException.class)
    public void testDelete() throws Exception {
        authorDAO.delete(3L);
        Author author = authorDAO.read(3L);
        assertNull(author);
    }

    @Test
    public void testDeleteNewsAuthor() throws Exception {
        authorDAO.deleteByNewsId(2L);
        Author author = authorDAO.readByNewsId(2L);
        assertNull(author);
    }

    @Test
    public void testSetExpired() throws Exception {
        Author author = authorDAO.read(2L);
        assertNull(author.getExpired());
        authorDAO.setExpired(2L);
        author = authorDAO.read(2L);
        assertNotNull(author.getExpired());
    }
    
}