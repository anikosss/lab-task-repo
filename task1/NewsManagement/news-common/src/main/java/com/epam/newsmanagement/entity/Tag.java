package com.epam.newsmanagement.entity;

import java.io.Serializable;

/**
 * <p>Represents Tag entity</p>
 *
 * @author Dzmitry Anikeichanka
 */
public class Tag implements Serializable {
	private static final long serialVersionUID = -6676005924795656855L;
	private long tagId;
    private String name;

    public Tag() {
    }

    public Tag(long tagId, String name) {
        this.tagId = tagId;
        this.name = name;
    }

    public long getTagId() {
        return tagId;
    }

    public void setTagId(long tagId) {
        this.tagId = tagId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        StringBuilder result = new StringBuilder("Tag{");
        result.append("tagId = ").append(tagId);
        result.append(", name = ").append(name);
        result.append('}');
        return result.toString();
    }

    @Override
    public int hashCode() {
        int result = (int) (tagId ^ (tagId >>> 32));
        result = 31 * result + name.hashCode();
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (this == obj) {
            return true;
        }
        if (obj.getClass() != getClass()) {
            return false;
        }
        Tag tag = (Tag) obj;
        if (tagId != tag.tagId) {
            return false;
        }
        return name.equals(tag.name);

    }
}
