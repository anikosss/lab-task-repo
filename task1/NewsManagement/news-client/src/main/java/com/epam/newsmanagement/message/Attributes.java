package com.epam.newsmanagement.message;

/**
 * <p>Represents constant values for all attributes</p>
 */
public final class Attributes {
    public static final String LOGIN = "login";
    public static final String PASSWORD = "password";
    public static final String ERROR = "error";
    public static final String USER = "user";
    public static final String PAGE = "page";
    public static final String AUTHORS = "authors";
    public static final String AUTHOR = "author";
    public static final String TAGS = "tags";
    public static final String NEWS_LIST = "newsList";
    public static final String PAGES_COUNT = "pagesCount";
    public static final String ACTION = "action";
    public static final String NEWS = "news";
    public static final String SEARCH = "search";
    public static final String PARAMS = "params";
    public static final String NEWS_DTO = "newsDTO";
    public static final String VIEW_NEWS = "view-news";
    public static final String COMMENT = "comment";
}
