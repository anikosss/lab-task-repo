package com.epam.newsmanagement.dao.impl.oracle;

import com.epam.newsmanagement.dao.INewsDAO;
import com.epam.newsmanagement.entity.News;
import com.epam.newsmanagement.exception.DAOException;
import com.epam.newsmanagement.util.SearchCriteria;
import com.github.springtestdbunit.DbUnitTestExecutionListener;
import com.github.springtestdbunit.annotation.DatabaseOperation;
import com.github.springtestdbunit.annotation.DatabaseSetup;
import com.github.springtestdbunit.annotation.DatabaseTearDown;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.springframework.test.context.support.DirtiesContextTestExecutionListener;
import org.springframework.test.context.transaction.TransactionalTestExecutionListener;

import java.sql.Date;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath:test-context.xml"})
@DatabaseSetup(value = "classpath:test-dataset.xml")
@DatabaseTearDown(value = "classpath:test-dataset.xml", type = DatabaseOperation.DELETE_ALL)
@TestExecutionListeners({ DependencyInjectionTestExecutionListener.class,
        DirtiesContextTestExecutionListener.class,
        TransactionalTestExecutionListener.class,
        DbUnitTestExecutionListener.class })
public class OracleNewsDAOImplTest extends Assert {
    @Autowired
    private INewsDAO newsDAO;

    @Test
    public void testRead() throws Exception {
        News news = newsDAO.read(2L);
        assertEquals("title2", news.getTitle());
    }

    @Test
    public void testCreate() throws Exception  {
        News news = new News(1, "title", "short-text", "full-text", new Date(System.currentTimeMillis()),
                new Date(System.currentTimeMillis()));
        Long id = newsDAO.create(news);
        News dbNews = newsDAO.read(id);
        assertNotNull(id);
        assertEquals(news.getTitle(), dbNews.getTitle());
    }

    @Test
    public void testReadAll() throws Exception  {
        List<News> news = newsDAO.readAll();
        assertEquals(3, news.size());
        assertEquals(3L, news.get(0).getNewsId());
    }

    @Test
    public void testReadWithRange() throws Exception  {
        List<News> news1 = newsDAO.readWithRange(1,3);
        List<News> news2 = newsDAO.readWithRange(1,2);
        List<News> news3 = newsDAO.readWithRange(2,3);
        List<News> news4 = newsDAO.readWithRange(2,8);
        assertEquals(3, news1.size());
        assertEquals(2, news2.size());
        assertEquals(2, news3.size());
        assertEquals(2, news4.size());
    }

    @Test
    public void testReadNext() throws Exception  {
        List<News> news = newsDAO.readAll();
        System.out.println(news);
        long nextNewsId1 = newsDAO.readNextNewsId(2);
        long nextNewsId2 = newsDAO.readNextNewsId(3);
        assertEquals(0L, nextNewsId1);
        assertEquals(1L, nextNewsId2);
    }

    @Test
    public void testReadPrev() throws Exception  {
        List<News> news = newsDAO.readAll();
        System.out.println(news);
        long nextNewsId1 = newsDAO.readPreviousNewsId(1);
        long nextNewsId2 = newsDAO.readPreviousNewsId(3);
        assertEquals(3L, nextNewsId1);
        assertEquals(0L, nextNewsId2);
    }

    @Test
    public void testUpdate() throws Exception  {
        News news = newsDAO.read(2L);
        news.setTitle("newTitle");
        newsDAO.update(news);
        News dbNews = newsDAO.read(2L);
        assertEquals(news.getTitle(), dbNews.getTitle());
    }

    @Test(expected = DAOException.class)
    public void testDelete() throws Exception  {
        newsDAO.delete(3L);
        News news = newsDAO.read(3L);
        assertNull(news);
    }

    @Test
    public void testGetCount() throws Exception {
        int count = newsDAO.getAllNewsCount();
        assertEquals(3, count);
    }

    @Test
    public void testGetCountBySearchCriteria() throws Exception {
        int count = newsDAO.getNewsCountBySearchCriteria(new SearchCriteria(1L, Arrays.asList(1L, 2L)));
        assertEquals(1, count);
    }

    @Test
    public void testSearchByAuthor() throws Exception {
        List<News> list1 = newsDAO.searchByAuthor(3L, 1, 6);
        List<News> list2 = newsDAO.searchBySearchCriteria(new SearchCriteria(3L), 1, 6);
        assertEquals(1, list1.size());
        assertEquals(1, list2.size());
        assertEquals("title3", list1.get(0).getTitle());
        assertEquals("title3", list2.get(0).getTitle());
    }

    @Test
    public void testSearchByZeroTags() throws Exception {
        List<Long> tagIdList = new ArrayList<>();
        List<News> news1 = newsDAO.searchByTagIdList(tagIdList, 1, 6);
        List<News> news2 = newsDAO.searchBySearchCriteria(new SearchCriteria(tagIdList), 1, 6);
        assertEquals(3, news1.size());
        assertEquals(3, news2.size());
        assertEquals(3L, news1.get(0).getNewsId());
        assertEquals(3L, news2.get(0).getNewsId());
    }

    @Test
    public void testSearchBySingleTag() throws Exception {
        List<Long> tagIdList = new ArrayList<>();
        tagIdList.add(1L);
        List<News> news1 = newsDAO.searchByTagIdList(tagIdList, 1, 6);
        List<News> news2 = newsDAO.searchBySearchCriteria(new SearchCriteria(tagIdList), 1, 6);
        assertEquals(2, news1.size());
        assertEquals(2, news2.size());
    }

    @Test
    public void testSearchByMultiplyTags() throws Exception {
        List<Long> tagIdList = new ArrayList<>(Arrays.asList(1L, 2L));
        List<News> news1 = newsDAO.searchByTagIdList(tagIdList, 1, 6);
        List<News> news2 = newsDAO.searchBySearchCriteria(new SearchCriteria(tagIdList), 1, 6);
        assertEquals(2, news1.size());
        assertEquals(2, news2.size());
    }

    @Test
    public void testSearchByAuthorAndSingleTag() throws Exception {
        List<Long> tags = new ArrayList<>(Arrays.asList(1L));
        List<News> news = newsDAO.searchByAuthorAndTags(1L, tags, 1, 6);
        assertEquals(1, news.size());
        assertEquals("title1", news.get(0).getTitle());
    }

    @Test
    public void testSearchByAuthorAndMultiplyTags() throws Exception {
        List<Long> tags = new ArrayList<>(Arrays.asList(1L, 2L));
        List<News> news1 = newsDAO.searchByAuthorAndTags(1L, tags, 1, 6);
        List<News> news2 = newsDAO.searchBySearchCriteria(new SearchCriteria(1L, tags), 1, 6);
        assertEquals(1, news1.size());
        assertEquals(1, news2.size());
        assertEquals("title1", news1.get(0).getTitle());
        assertEquals("title1", news2.get(0).getTitle());
    }

}