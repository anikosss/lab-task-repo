package com.epam.newsmanagement.service.impl;

import com.epam.newsmanagement.dao.IRoleDAO;
import com.epam.newsmanagement.entity.Role;
import com.epam.newsmanagement.exception.DAOException;
import com.epam.newsmanagement.exception.ServiceException;
import com.epam.newsmanagement.service.IRoleService;

import java.util.List;

/**
 * <p>Represents realization for {@link IRoleService}</p>
 *
 * @author Dzmitry Anikeichanka
 */
public class RoleServiceImpl implements IRoleService {
    private IRoleDAO roleDAO;

    @Override
    public Long addRole(Role role) throws ServiceException {
        Long result = null;
        try {
            result = roleDAO.create(role);
        } catch (DAOException e) {
            throw new ServiceException(e.getMessage());
        }
        return result;
    }

    @Override
    public Role getRoleById(Long roleId) throws ServiceException {
        Role result = null;
        try {
            result = roleDAO.read(roleId);
        } catch (DAOException e) {
            throw new ServiceException(e.getMessage());
        }
        return result;
    }

    @Override
    public List<Role> getAllRoles() throws ServiceException {
        List<Role> result = null;
        try {
            result = roleDAO.readAll();
        } catch (DAOException e) {
            throw new ServiceException(e.getMessage());
        }
        return result;
    }

    @Override
    public void updateRole(Role role) throws ServiceException {
        try {
            roleDAO.update(role);
        } catch (DAOException e) {
            throw new ServiceException(e.getMessage());
        }
    }

    @Override
    public void deleteRoleById(Long roleId) throws ServiceException {
        try {
            roleDAO.delete(roleId);
        } catch (DAOException e) {
            e.printStackTrace();
        }
    }

    public void setRoleDAO(IRoleDAO roleDAO) {
        this.roleDAO = roleDAO;
    }
}
