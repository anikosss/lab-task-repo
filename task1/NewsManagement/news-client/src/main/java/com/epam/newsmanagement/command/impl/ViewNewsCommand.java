package com.epam.newsmanagement.command.impl;

import com.epam.newsmanagement.command.IActionCommand;
import com.epam.newsmanagement.dto.NewsDTO;
import com.epam.newsmanagement.exception.CommandException;
import com.epam.newsmanagement.exception.ServiceException;
import com.epam.newsmanagement.message.Attributes;
import com.epam.newsmanagement.message.Pages;
import com.epam.newsmanagement.service.ICommonService;
import com.epam.newsmanagement.util.ResponseHolder;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletRequest;

/**
 * <p>Represents implementation for command that should view one particular news</p>
 */
@Component
public class ViewNewsCommand implements IActionCommand {
    private static final Logger LOGGER = Logger.getLogger(ViewNewsCommand.class);
    @Autowired
    private ICommonService commonService;

    private ViewNewsCommand() {}

    @Override
    public ResponseHolder execute(HttpServletRequest request) throws CommandException {
        ResponseHolder result = new ResponseHolder();
        long id = Long.parseLong(request.getParameter(Attributes.NEWS));
        try {
            NewsDTO newsDTO = commonService.getNewsById(id);
            if (newsDTO == null) {
                return result;
            }
            request.setAttribute(Attributes.NEWS_DTO, newsDTO);
            result.setPage(Pages.VIEW_NEWS_PAGE);
        } catch (ServiceException e) {
            LOGGER.error(e.getMessage());
        }
        return result;
    }
}
