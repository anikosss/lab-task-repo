package com.epam.newsmanagement.exception;

/**
 * <p>Represents exception for Service classes</p>
 *
 * @author Dzmitry Anikeichanka
 */
public class ServiceException extends RuntimeException{
	private static final long serialVersionUID = -1698741085083999845L;

	public ServiceException() {
        super();
    }

    public ServiceException(String message) {
        super(message);
    }
}
