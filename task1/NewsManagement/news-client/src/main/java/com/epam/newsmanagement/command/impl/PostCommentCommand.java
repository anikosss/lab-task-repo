package com.epam.newsmanagement.command.impl;

import com.epam.newsmanagement.command.IActionCommand;
import com.epam.newsmanagement.entity.Comment;
import com.epam.newsmanagement.exception.CommandException;
import com.epam.newsmanagement.exception.ServiceException;
import com.epam.newsmanagement.exception.ValidateException;
import com.epam.newsmanagement.message.Actions;
import com.epam.newsmanagement.message.Attributes;
import com.epam.newsmanagement.parser.IRequestParser;
import com.epam.newsmanagement.service.ICommentService;
import com.epam.newsmanagement.util.MethodEnum;
import com.epam.newsmanagement.util.ResponseHolder;
import com.epam.newsmanagement.validator.IRequestValidator;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletRequest;

/**
 * <p>Represents implementation for command that should post the comment</p>
 */
@Component
public class PostCommentCommand implements IActionCommand {
    private static final Logger LOGGER = Logger.getLogger(PostCommentCommand.class);
    @Autowired
    private ICommentService commentService;
    @Autowired
    private IRequestParser<Comment> commentParser;
    @Autowired
    @Qualifier("commentValidator")
    private IRequestValidator commentValidator;
    @Autowired
    @Qualifier("newsValidator")
    private IRequestValidator newsValidator;

    private PostCommentCommand() {}

    @Override
    public ResponseHolder execute(HttpServletRequest request) throws CommandException {
        ResponseHolder result = new ResponseHolder(MethodEnum.FORWARD);
        try {
            newsValidator.validateRequest(request);
            Long newsId = Long.parseLong(request.getParameter(Attributes.NEWS));
            result.setPage(String.format(Actions.VIEW_NEWS_ACTION, newsId));
            commentValidator.validateRequest(request);
            Comment comment = commentParser.parseRequest(request);
            commentService.addComment(comment);
            result.setMethod(MethodEnum.REDIRECT);
            result.setPage(String.format(Actions.VIEW_NEWS_ACTION, comment.getNewsId()));
        } catch (ValidateException | ServiceException e) {
            LOGGER.error(e.getMessage(), e);
        }
        return result;
    }
}
