package com.epam.newsmanagement.command;

import com.epam.newsmanagement.exception.CommandException;
import com.epam.newsmanagement.util.ResponseHolder;

import javax.servlet.http.HttpServletRequest;

/**
 * <p>Represents interface for command implementations</p>
 */
public interface IActionCommand {

    /**
     * <p>Executes some logic with request and returns the response</p>
     *
     * @param request {@link HttpServletRequest} that should be processed
     * @return {@link ResponseHolder} object that holds page to forward and method for forward
     * @throws CommandException
     */
    ResponseHolder execute(HttpServletRequest request) throws CommandException;
}
