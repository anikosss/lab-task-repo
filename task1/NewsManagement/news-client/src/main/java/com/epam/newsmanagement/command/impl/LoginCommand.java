package com.epam.newsmanagement.command.impl;

import com.epam.newsmanagement.command.IActionCommand;
import com.epam.newsmanagement.entity.User;
import com.epam.newsmanagement.exception.CommandException;
import com.epam.newsmanagement.message.Actions;
import com.epam.newsmanagement.message.Attributes;
import com.epam.newsmanagement.message.ErrorMessages;
import com.epam.newsmanagement.util.MethodEnum;
import com.epam.newsmanagement.util.ResponseHolder;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletRequest;

/**
 * <p>Represents implementation for command that provides log in operations</p>
 */
@Component
public class LoginCommand implements IActionCommand {

    @Override
    public ResponseHolder execute(HttpServletRequest request) throws CommandException {
        String login = request.getParameter(Attributes.LOGIN);
        String password = request.getParameter(Attributes.PASSWORD);
        if (login == null || password == null || login.isEmpty() || password.isEmpty()) {
            request.setAttribute(Attributes.ERROR, ErrorMessages.WRONG_LOGIN_OR_PASS);
            return new ResponseHolder(MethodEnum.FORWARD, Actions.LOGIN_ACTION);
        }
        User user = new User(1, login, login, password);
        request.getSession().setAttribute(Attributes.USER, user);
        return new ResponseHolder(MethodEnum.REDIRECT, Actions.NEWS_ACTION);
    }
}
