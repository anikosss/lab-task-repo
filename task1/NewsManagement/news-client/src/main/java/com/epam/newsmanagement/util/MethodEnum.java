package com.epam.newsmanagement.util;

/**
 * <p>Represents enum of methods for defining how to move to the resource from servlet</p>
 */
public enum MethodEnum {
    FORWARD,
    REDIRECT
}
