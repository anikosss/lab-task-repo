<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<jsp:useBean id="newsDTO" class="com.epam.newsmanagement.dto.NewsDTO" scope="request" />
<!doctype html>
<html>
<jsp:include page="common/head.jsp" />
<body>
<jsp:include page="common/header.jsp" />
<div class="container">
    <c:if test="${not empty error}">
        <div class="col-md-offset-4 col-md-4">
            <div class="alert alert-danger alert-header fade in">
                <a href="#" class="close" data-dismiss="alert" area-label="close">&times;</a>
                    ${error}.
            </div>
        </div>
    </c:if>
    <div class="row">
        <div class="col-md-12">
            <h4 class="pull-left"><a href="controller?action=news&page=1"><button class="btn btn-default">All news</button></a></h4>
            <h4 class="pull-right">${newsDTO.news.modificationDate}</h4>
            <br>
            <div class="page-header">
                <h1 class="news-header">${newsDTO.news.title}<br><small>by ${newsDTO.author.name}</small></h1>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <p class="news-small-text">
                ${newsDTO.news.shortText}
            </p>
            <p>
                ${newsDTO.news.fullText}
            </p>
        </div>
    </div>
    <br>
    <c:if test="${newsDTO.prevNewsId != 0}">
        <div class="pull-left">
            <a href="controller?action=view-news&news=${newsDTO.prevNewsId}">
                <button class="btn btn-default">
                    <span class="glyphicon glyphicon-chevron-left"></span> Previous
                </button>
            </a>
        </div>
    </c:if>
    <c:if test="${newsDTO.nextNewsId != 0}">
        <div class="pull-right">
            <a href="controller?action=view-news&news=${newsDTO.nextNewsId}">
                <button class="btn btn-default">
                    Next <span class="glyphicon glyphicon-chevron-right"></span>
                </button>
            </a>
        </div>
    </c:if>
    <br><hr><br>
    <h2>Comments:</h2>
    <c:forEach items="${newsDTO.comments}" var="comment">
        <div class="row">
            <div class="col-md-9">
                <div class="page-header">
                    <h4>${comment.creationDate}<br></h4>
                    <div>
                            ${comment.text}
                    </div>
                </div>
            </div>
        </div>
    </c:forEach>
    <br><br>
    <div class="row">
        <div class="col-md-9">
            <form action="controller" method="POST">
                <input type="hidden" name="action" value="post-comment">
                <input type="hidden" name="news" value="${newsDTO.news.newsId}" />
                <div class="form-group">
                    <label for="comment-area">Comment:</label>
                    <textarea name="comment" class="form-control" rows="5" id="comment-area"></textarea>
                </div>
                <div class="pull-right"><button type="submit" class="btn btn-default">Submit</button></div>
            </form>
        </div>
    </div>
</div>
<jsp:include page="common/footer.jsp" />
</body>
</html>
