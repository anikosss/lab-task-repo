package com.epam.newsmanagement.service.impl;

import com.epam.newsmanagement.dao.INewsDAO;
import com.epam.newsmanagement.entity.News;
import com.epam.newsmanagement.exception.DAOException;
import com.epam.newsmanagement.exception.ServiceException;
import com.epam.newsmanagement.service.INewsService;
import com.epam.newsmanagement.util.SearchCriteria;

import java.util.List;

/**
 * <p>Represents realization for {@link INewsService}</p>
 *
 * @author Dzmitry Anikeichanka
 */
public class NewsServiceImpl implements INewsService {
    private INewsDAO newsDAO;

    @Override
    public Long addNews(News news) throws ServiceException {
        Long result = null;
        try {
            result = newsDAO.create(news);
        } catch (DAOException e) {
            throw new ServiceException(e.getMessage());
        }
        return result;
    }

    @Override
    public News getNewsById(Long newsId) throws ServiceException {
        News result = null;
        try {
            result = newsDAO.read(newsId);
        } catch (DAOException e) {
            throw new ServiceException(e.getMessage());
        }
        return result;
    }

    @Override
    public List<News> getAllNews() throws ServiceException {
        List<News> result = null;
        try {
            result = newsDAO.readAll();
        } catch (DAOException e) {
            throw new ServiceException(e.getMessage());
        }
        return result;
    }

    @Override
    public List<News> searchNewsBySearchCriteria(SearchCriteria searchCriteria, int start, int end) throws ServiceException {
        List<News> result = null;
        try {
            result = newsDAO.searchBySearchCriteria(searchCriteria, start, end);
        } catch (DAOException e) {
            throw new ServiceException(e.getMessage());
        }
        return result;
    }

    @Override
    public List<News> searchNewsByAuthor(Long authorId, int start, int end) throws ServiceException {
        List<News> result = null;
        try {
            result = newsDAO.searchByAuthor(authorId, start, end);
        } catch (DAOException e) {
            throw new ServiceException(e.getMessage());
        }
        return result;
    }

    @Override
    public List<News> searchNewsByTags(List<Long> tagIdList, int start, int end) throws ServiceException {
        List<News> result = null;
        try {
            result = newsDAO.searchByTagIdList(tagIdList, start, end);
        } catch (DAOException e) {
            throw new ServiceException(e.getMessage());
        }
        return result;
    }

    @Override
    public List<News> searchNewsByAuthorAndTags(Long authorId, List<Long> tagIdList, int start, int end) throws ServiceException {
        List<News> result = null;
        try {
            result = newsDAO.searchByAuthorAndTags(authorId, tagIdList, start, end);
        } catch (DAOException e) {
            throw new ServiceException(e.getMessage());
        }
        return result;
    }

    @Override
    public List<News> getNewsWithRange(int start, int end) throws ServiceException {
        List<News> result = null;
        try {
            result = newsDAO.readWithRange(start, end);
        } catch (DAOException e) {
            throw new ServiceException(e.getMessage());
        }
        return result;
    }

    @Override
    public void updateNews(News news) throws ServiceException {
        try {
            newsDAO.update(news);
        } catch (DAOException e) {
            throw new ServiceException(e.getMessage());
        }
    }

    @Override
    public void deleteNewsById(Long newsId) throws ServiceException {
        try {
            newsDAO.delete(newsId);
        } catch (DAOException e) {
            throw new ServiceException(e.getMessage());
        }
    }

    @Override
    public int getAllNewsCount() throws ServiceException {
        int result = 0;
        try {
            result = newsDAO.getAllNewsCount();
        } catch (DAOException e) {
            throw new ServiceException(e.getMessage());
        }
        return result;
    }
    
    @Override
    public int getNewsCountBySearchCriteria(SearchCriteria searchCriteria) throws ServiceException {
    	int result = 0;
    	try {
    		result = newsDAO.getNewsCountBySearchCriteria(searchCriteria);
    	} catch (DAOException e) {
    		throw new ServiceException(e.getMessage());
    	}
    	return result;
    }

    @Override
    public long getNextNewsId(long newsId) throws ServiceException {
        long result = 0L;
        try {
            result = newsDAO.readNextNewsId(newsId);
        } catch (DAOException e) {
            throw new ServiceException(e.getMessage());
        }
        return result;
    }

    @Override
    public long getPreviousNewsId(long newsId) throws ServiceException {
        long result = 0L;
        try {
            result = newsDAO.readPreviousNewsId(newsId);
        } catch (DAOException e) {
            throw new ServiceException(e.getMessage());
        }
        return result;
    }

    public void setNewsDAO(INewsDAO newsDAO) {
        this.newsDAO = newsDAO;
    }
}
