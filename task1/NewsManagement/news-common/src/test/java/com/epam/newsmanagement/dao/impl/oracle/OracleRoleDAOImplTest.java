package com.epam.newsmanagement.dao.impl.oracle;

import com.epam.newsmanagement.dao.IRoleDAO;
import com.epam.newsmanagement.entity.Role;
import com.github.springtestdbunit.DbUnitTestExecutionListener;
import com.github.springtestdbunit.annotation.DatabaseOperation;
import com.github.springtestdbunit.annotation.DatabaseSetup;
import com.github.springtestdbunit.annotation.DatabaseTearDown;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.springframework.test.context.support.DirtiesContextTestExecutionListener;
import org.springframework.test.context.transaction.TransactionalTestExecutionListener;

import java.util.List;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath:test-context.xml"})
@DatabaseSetup(value = "classpath:test-dataset.xml")
@DatabaseTearDown(value = "classpath:test-dataset.xml", type = DatabaseOperation.DELETE_ALL)
@TestExecutionListeners({ DependencyInjectionTestExecutionListener.class,
        DirtiesContextTestExecutionListener.class,
        TransactionalTestExecutionListener.class,
        DbUnitTestExecutionListener.class })
public class OracleRoleDAOImplTest extends Assert {
    @Autowired
    private IRoleDAO roleDAO;

    @Test
    public void testRead() throws Exception {
        Role role = roleDAO.read(1L);
        assertEquals("name1", role.getName());
    }

    @Test
    public void testCreate() throws Exception {
        Role role = new Role(1L, "name", 1L);
        Long id = roleDAO.create(role);
        Role dbRole = roleDAO.read(id);
        assertNotNull(id);
        assertEquals(role.getName(), dbRole.getName());
    }

    @Test
    public void testReadAll() throws Exception {
        List<Role> roles = roleDAO.readAll();
        assertEquals(2, roles.size());
    }

    @Test
    public void testUpdate() throws Exception {
        Role role = roleDAO.read(2L);
        role.setName("newName");
        roleDAO.update(role);
        role = roleDAO.read(2L);
        assertEquals("newName", role.getName());
    }

    @Test
    public void testDelete() throws Exception {
        roleDAO.delete(2L);
        Role role = roleDAO.read(2L);
        assertNull(role);
    }
    
}