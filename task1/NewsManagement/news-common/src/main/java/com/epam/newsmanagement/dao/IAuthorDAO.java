package com.epam.newsmanagement.dao;

import com.epam.newsmanagement.entity.Author;
import com.epam.newsmanagement.exception.DAOException;

/**
 * <p>Represents {@link Author} DAO operations</p>
 *
 * @author Dzmitry Anikeichanka
 */
public interface IAuthorDAO extends IBaseDAO<Author, Long> {

    /**
     * <p>Inserts new Author entity into the database by News ID
     * and returns the inserted entity ID</p>
     *
     * @param newsId News ID which new entity must be inserted by
     * @return inserted entity ID
     * @throws DAOException
     */
    Long createByNewsId(Long newsId, Long authorId) throws DAOException;

    /**
     * <p>Receives {@link Author} entity by News ID</p>
     *
     * @param newsId News ID which new entity must be received by
     * @return received entity
     * @throws DAOException
     */
    Author readByNewsId(Long newsId) throws DAOException;

    /**
     * <p>Updates {@link Author} entity in the database by News ID</p>
     *
     * @param newsId News ID which new entity must be updated by
     * @param authorId Author ID on which row must be updated
     * @throws DAOException
     */
    void updateNewsAuthor(Long newsId, Long authorId) throws DAOException;

    /**
     * <p>Removes {@link Author} entity from the database by News ID</p>
     *
     * @param newsId News ID which new entity must be deleted by
     * @throws DAOException
     */
    void deleteByNewsId(Long newsId) throws DAOException;

    /**
     * <p>Sets {@link Author} entity expired by ID</p>
     *
     * @param authorId Author ID which entity must be set by
     * @throws DAOException
     */
    void setExpired(Long authorId) throws DAOException;
}
