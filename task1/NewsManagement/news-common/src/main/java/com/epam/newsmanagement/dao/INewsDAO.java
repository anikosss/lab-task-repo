package com.epam.newsmanagement.dao;

import com.epam.newsmanagement.entity.News;
import com.epam.newsmanagement.exception.DAOException;
import com.epam.newsmanagement.util.SearchCriteria;

import java.util.List;

/**
 * <p>Represents {@link News} DAO operations</p>
 *
 * @author Dzmitry Anikeichanka
 */
public interface INewsDAO extends IBaseDAO<News, Long> {

    /**
     * <p>Receives count of news in the database</p>
     *
     * @return count of news
     * @throws DAOException
     */
    int getAllNewsCount() throws DAOException;

    /**
     * <p>Receives count of news in the database according to the SearchCriteria</p>
     *
     * @param searchCriteria {@link SearchCriteria} object which counting occured by
     * @return count of news
     * @throws DAOException
     */
    int getNewsCountBySearchCriteria(SearchCriteria searchCriteria) throws DAOException;

    /**
     * <p>Receives next news ID for current news ID</p>
     *
     * @param newsId current news ID
     * @return next news ID or 0 if there is no next news
     * @throws DAOException
     */
    long readNextNewsId(long newsId) throws DAOException;

    /**
     * <p>Receives previous news ID for current news ID</p>
     *
     * @param newsId current news ID
     * @return news ID or 0 if there is no previous news
     * @throws DAOException
     */
    long readPreviousNewsId(long newsId) throws DAOException;

    List<News> searchBySearchCriteria(SearchCriteria searchCriteria, int start, int end) throws DAOException;

    /**
     * <p>Searches news by Author ID in the database</p>
     *
     * @param authorId Author ID which news must be searched by
     * @param start first index of the rage
     * @param end last index of the range
     * @return {@link List} of news
     * @throws DAOException
     */
    List<News> searchByAuthor(Long authorId, int start, int end) throws DAOException;

    /**
     * <p>Searches news by {@link List} of Tag IDs in the database</p>
     *
     * @param tagIdList {@link List} of tags which news must be searched by
     * @param start first index of the range
     * @param end last index of the range
     * @return {@link List} of news
     * @throws DAOException
     */
    List<News> searchByTagIdList(List<Long> tagIdList, int start, int end) throws DAOException;

    /**
     * <p>Searches news by Author ID and {@link List} of Tag IDs in the database</p>
     *
     * @param authorId Author ID which news must be searched by
     * @param tagIdList {@link List} of tags which news must be searched by
     * @param start first index of the range
     * @param end last index of the range
     * @return {@link List} of news
     * @throws DAOException
     */
    List<News> searchByAuthorAndTags(Long authorId, List<Long> tagIdList, int start, int end) throws DAOException;

    /**
     * <p>Receives news from database with range</p>
     *
     * @param start first element index
     * @param end last element index
     * @return {@link List} received entities list
     * @throws DAOException
     */
    List<News> readWithRange(int start, int end) throws DAOException;

}
