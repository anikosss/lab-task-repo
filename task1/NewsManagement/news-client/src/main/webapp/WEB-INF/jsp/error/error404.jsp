<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!doctype html>
<html lang="en">
<jsp:include page="../common/head.jsp" />
<body>
<jsp:include page="../common/header.jsp" />
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="page-header">
                    <h1 class="news-header">Page not found<br><small>Error 404: page not found</small></h1>
                    <br>
                    <a href="controller?action=news&page=1">Go to main page</a>
                </div>
            </div>
        </div>
    </div>
</body>
</html>
