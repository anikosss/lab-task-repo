package com.epam.newsmanagement.parser;

import com.epam.newsmanagement.exception.ParserException;

import javax.servlet.http.HttpServletRequest;

/**
 * <p>Represents interface for parsers</p>
 *
 * @param <T> type of returning object
 */
public interface IRequestParser<T> {

    /**
     * <p>Parses the request</p>
     *
     * @param request {@link HttpServletRequest} that should be parsed
     * @return T object that was parsed from request
     * @throws ParserException
     */
    T parseRequest(HttpServletRequest request) throws ParserException;
}
