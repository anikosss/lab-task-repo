package com.epam.newsmanagement.dao.impl.oracle;

import com.epam.newsmanagement.dao.ITagDAO;
import com.epam.newsmanagement.entity.Tag;
import com.epam.newsmanagement.exception.DAOException;
import com.github.springtestdbunit.DbUnitTestExecutionListener;
import com.github.springtestdbunit.annotation.DatabaseOperation;
import com.github.springtestdbunit.annotation.DatabaseSetup;
import com.github.springtestdbunit.annotation.DatabaseTearDown;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.springframework.test.context.support.DirtiesContextTestExecutionListener;
import org.springframework.test.context.transaction.TransactionalTestExecutionListener;

import java.util.List;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath:test-context.xml"})
@DatabaseSetup(value = "classpath:test-dataset.xml")
@DatabaseTearDown(value = "classpath:test-dataset.xml", type = DatabaseOperation.DELETE_ALL)
@TestExecutionListeners({ DependencyInjectionTestExecutionListener.class,
        DirtiesContextTestExecutionListener.class,
        TransactionalTestExecutionListener.class,
        DbUnitTestExecutionListener.class })
public class OracleTagDAOImplTest extends Assert {
    @Autowired
    private ITagDAO tagDAO;

    @Test
    public void testRead() throws Exception {
        Tag tag = tagDAO.read(2L);
        assertEquals("name2", tag.getName());
    }

    @Test
    public void testReadByNewsId() throws Exception {
        List<Tag> tags = tagDAO.readByNewsId(1L);
        assertEquals(2, tags.size());
    }

    @Test
    public void testCreate() throws Exception {
        Tag tag = new Tag(1, "name");
        Long id = tagDAO.create(tag);
        Tag dbTag = tagDAO.read(id);
        assertNotNull(id);
        assertEquals(tag.getName(), dbTag.getName());
    }

    @Test
    public void testAddNewsTag() throws Exception {
        Long id = tagDAO.createByNewsId(2L, 2L);
        assertNotNull(id);
    }

    @Test
    public void testReadAll() throws Exception {
        List<Tag> tags = tagDAO.readAll();
        assertEquals(3, tags.size());
    }

    @Test
    public void testUpdate() throws Exception {
        Tag tag = tagDAO.read(2L);
        tag.setName("newName");
        tagDAO.update(tag);
        tag = tagDAO.read(2L);
        assertEquals("newName", tag.getName());
    }

    @Test(expected = DAOException.class)
    public void testDelete() throws Exception {
        tagDAO.delete(1L);
        Tag tag = tagDAO.read(1L);
    }

    @Test
    public void testDeleteNewsTag() throws Exception {
        tagDAO.deleteByNewsId(3L, 3L);
        List<Tag> tags = tagDAO.readByNewsId(3L);
        assertEquals(0, tags.size());
    }

    @Test
    public void testDeleteAllNewsTags() throws Exception {
        tagDAO.deleteAllByNewsId(1L);
        List<Tag> tags = tagDAO.readByNewsId(1L);
        assertEquals(0, tags.size());
    }
}