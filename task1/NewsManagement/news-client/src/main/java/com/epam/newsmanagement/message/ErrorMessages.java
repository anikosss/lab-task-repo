package com.epam.newsmanagement.message;

/**
 * <p>Represents class for all error messages</p>
 */
public final class ErrorMessages {
    public static final String WRONG_COMMAND = "Wrong command with key: %s";
    public static final String WRONG_LOGIN_OR_PASS = "Wrong login or password";
    public static final String WRONG_COMMENT = "Wrong comment";
    public static final String WRONG_NEWS = "Wrong news ID";
    public static final String PLEASE_LOG_IN = "Please, log in";
}
