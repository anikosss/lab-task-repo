package com.epam.newsmanagement.exception;

/**
 * <p>Represents custom exception for parsers</p>
 */
public class ParserException extends RuntimeException {

    public ParserException() {
        super();
    }

    public ParserException(String message) {
        super(message);
    }
}
