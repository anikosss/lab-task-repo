<%@ taglib prefix="sf" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<br>
<br>
<br>
<br>
<br>
<%--<form class="login-form" action="admin/login" method="POST">
    <div class="form-group">
        <label for="login">Login</label>
        <input type="text" name="login" class="form-control" id="login" placeholder="Login">
    </div>
    <div class="form-group">
        <label for="password">Password</label>
        <input type="password" name="password" class="form-control" id="password" placeholder="Password">
    </div>
    <input type="hidden" name="action" value="login">
    <button type="submit" class="btn btn-default">Submit</button>
</form>--%>
<c:if test="${not empty error}">
	<div class="col-md-offset-4 col-md-4">
		<div class="alert alert-danger alert-header fade in">
			<a href="#" class="close" data-dismiss="alert" area-label="close">&times;</a>
			${error}.
		</div>
	</div>
</c:if>
<form method="POST" action="/static/j_spring_security_check">
	<div class="form-group">
		<label for="login">Login</label> <input type="text" name="j_username"
			class="form-control" id="login" placeholder="Login">
	</div>
	<div class="form-group">
		<label for="password">Password</label> <input type="password"
			name="j_password" class="form-control" id="password"
			placeholder="Password">
	</div>
	<input type="hidden" name="action" value="login">
	<button type="submit" name="commit" class="btn btn-default">Submit</button>
</form>
<%-- <sf:form method="POST" modelAttribute="user"> --%>
<!--     <div class="form-group"> -->
<!--         <label for="user_login">Login</label> -->
<%--         <sf:input path="login" size="20" class="form-control" --%>
<%--                   id="user_login" placeholder="Login" /> --%>
<!--     </div> -->
<!--     <div class="form-group"> -->
<!--         <label for="password">Password</label> -->
<%--         <sf:password path="password" size="30" --%>
<%--                      class="form-control" id="user_password" placeholder="Password" /> --%>
<!--     </div> -->
<!--     <button type="submit" class="btn btn-default">Submit</button> -->
<%-- </sf:form> --%>

