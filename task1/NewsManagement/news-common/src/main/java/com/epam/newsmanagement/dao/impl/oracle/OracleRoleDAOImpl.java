package com.epam.newsmanagement.dao.impl.oracle;

import com.epam.newsmanagement.dao.IRoleDAO;
import com.epam.newsmanagement.entity.Role;
import com.epam.newsmanagement.exception.DAOException;
import com.epam.newsmanagement.message.DAOErrorMessages;
import com.epam.newsmanagement.message.TableRowNames;
import com.epam.newsmanagement.util.DAOUtil;
import org.springframework.jdbc.datasource.DataSourceUtils;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * <p>Represents realization {@link IRoleDAO} for Oracle database</p>
 *
 * @author Dzmitry Anikeichanka
 */
public class OracleRoleDAOImpl implements IRoleDAO {
    private static final String SQL_CREATE = "INSERT INTO ROLES (ROLES.role_id_pk, ROLES.role_name, ROLES.role_user_id) " +
            "VALUES (ROLES_SEQ.nextval, ?, ?)";
    private static final String SQL_READ = "SELECT ROLES.role_id_pk, ROLES.role_name, ROLES.role_user_id FROM ROLES " +
            "WHERE ROLES.role_id_pk = ?";
    private static final String SQL_READ_ALL = "SELECT ROLES.role_id_pk, ROLES.role_name, ROLES.role_user_id FROM ROLES";
    private static final String SQL_UPDATE = "UPDATE ROLES SET ROLES.role_name = ?, ROLES.role_user_id = ? " +
            "WHERE ROLES.role_id_pk = ?";
    private static final String SQL_DELETE = "DELETE FROM ROLES WHERE ROLES.role_id_pk = ?";
    private DataSource dataSource;

    @Override
    public Long create(Role role) throws DAOException {
        Long result = null;
        Connection connection = null;
        PreparedStatement statement = null;
        ResultSet resultSet = null;
        try {
            connection = DataSourceUtils.getConnection(dataSource);
            statement = connection.prepareStatement(SQL_CREATE, new String[]{ TableRowNames.ROLE_ID_PK });
            statement.setString(1, role.getName());
            statement.setLong(2, role.getUserId());
            statement.executeUpdate();
            resultSet = statement.getGeneratedKeys();
            if (resultSet.next()) {
                result = resultSet.getLong(1);
            } else {
                throw new DAOException(DAOErrorMessages.CREATE_ROLE + role);
            }
        } catch (SQLException e) {
            throw new DAOException(e.getMessage() + ". " + DAOErrorMessages.CREATE_ROLE + role);
        } finally {
            DAOUtil.releaseResources(dataSource, connection, statement, resultSet);
        }
        return result;
    }

    @Override
    public Role read(Long roleId) throws DAOException {
        Role result = null;
        Connection connection = null;
        PreparedStatement statement = null;
        ResultSet resultSet = null;
        try {
            connection = DataSourceUtils.getConnection(dataSource);
            statement = connection.prepareStatement(SQL_READ);
            statement.setLong(1, roleId);
            resultSet = statement.executeQuery();
            if (resultSet.next()) {
                result = buildRole(resultSet);
            }
        } catch (SQLException e) {
            throw new DAOException(e.getMessage() + ". " + String.format(DAOErrorMessages.READ_ROLE, roleId));
        } finally {
            DAOUtil.releaseResources(dataSource, connection, statement, resultSet);
        }
        return result;
    }

    @Override
    public List<Role> readAll() throws DAOException {
        List<Role> result = null;
        Connection connection = null;
        PreparedStatement statement = null;
        ResultSet resultSet = null;
        try {
            connection = DataSourceUtils.getConnection(dataSource);
            statement = connection.prepareStatement(SQL_READ_ALL);
            resultSet = statement.executeQuery();
            result = new ArrayList<>();
            while (resultSet.next()) {
                Role role = buildRole(resultSet);
                result.add(role);
            }
        } catch (SQLException e) {
            throw new DAOException(e.getMessage() + ". " + DAOErrorMessages.READ_ALL_ROLES);
        } finally {
            DAOUtil.releaseResources(dataSource, connection, statement, resultSet);
        }
        return result;
    }

    @Override
    public void update(Role role) throws DAOException {
        Connection connection = null;
        PreparedStatement statement = null;
        try {
            connection = DataSourceUtils.getConnection(dataSource);
            statement = connection.prepareStatement(SQL_UPDATE);
            statement.setString(1, role.getName());
            statement.setLong(2, role.getUserId());
            statement.setLong(3, role.getRoleId());
            statement.executeUpdate();
        } catch (SQLException e) {
            throw new DAOException(e.getMessage() + ". " + DAOErrorMessages.UPDATE_ROLE + role);
        } finally {
            DAOUtil.releaseResources(dataSource, connection, statement);
        }
    }

    @Override
    public void delete(Long roleId) throws DAOException {
        Connection connection = null;
        PreparedStatement statement = null;
        try {
            connection = DataSourceUtils.getConnection(dataSource);
            statement = connection.prepareStatement(SQL_DELETE);
            statement.setLong(1, roleId);
            statement.executeUpdate();
        } catch (SQLException e) {
            throw new DAOException(e.getMessage() + ". " + String.format(DAOErrorMessages.DELETE_ROLE, roleId));
        } finally {
            DAOUtil.releaseResources(dataSource, connection, statement);
        }
    }

    /**
     * Creates Role object from ResultSet
     *
     * @param resultSet {@link ResultSet} which data must be received from
     * @return {@link Role} received entity
     * @throws SQLException
     */
    private Role buildRole(ResultSet resultSet) throws SQLException {
        long id = resultSet.getLong(TableRowNames.ROLE_ID_PK);
        String name = resultSet.getString(TableRowNames.ROLE_NAME);
        long userId = resultSet.getLong(TableRowNames.ROLE_USER_ID);
        return new Role(id, name, userId);
    }

    public void setDataSource(DataSource dataSource) {
        this.dataSource = dataSource;
    }
}
