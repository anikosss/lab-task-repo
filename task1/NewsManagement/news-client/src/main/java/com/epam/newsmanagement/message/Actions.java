package com.epam.newsmanagement.message;

/**
 * <p>Represents constant values for all actions</p>
 */
public final class Actions {
    public static final String NEWS_ACTION = "controller?action=news";
    public static final String LOGIN_ACTION = "controller?action=go-to-login";
    public static final String VIEW_NEWS_ACTION = "controller?action=view-news&news=%d";
}
