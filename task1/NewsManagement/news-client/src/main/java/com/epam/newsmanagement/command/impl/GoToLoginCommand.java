package com.epam.newsmanagement.command.impl;

import com.epam.newsmanagement.command.IActionCommand;
import com.epam.newsmanagement.exception.CommandException;
import com.epam.newsmanagement.message.Pages;
import com.epam.newsmanagement.util.MethodEnum;
import com.epam.newsmanagement.util.ResponseHolder;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletRequest;

/**
 * <p>Represents implementation for command that should forward to login page</p>
 */
@Component
public class GoToLoginCommand implements IActionCommand {

    @Override
    public ResponseHolder execute(HttpServletRequest request) throws CommandException {
        return new ResponseHolder(MethodEnum.FORWARD, Pages.LOGIN_PAGE);
    }
}
