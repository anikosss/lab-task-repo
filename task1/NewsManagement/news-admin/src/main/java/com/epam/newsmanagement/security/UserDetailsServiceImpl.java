package com.epam.newsmanagement.security;

import java.util.ArrayList;
import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Component;

import com.epam.newsmanagement.entity.User;
import com.epam.newsmanagement.exception.ServiceException;
import com.epam.newsmanagement.service.IUserService;

@Component("userDetailsService")
public class UserDetailsServiceImpl implements UserDetailsService {
	@Autowired
	private IUserService userService;

	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		UserDetailsImpl result = new UserDetailsImpl();
		try {
			User user = userService.getUserByLogin(username);
			Collection<GrantedAuthority> authorities = new ArrayList<>();
            authorities.add(new SimpleGrantedAuthority("ROLE_ADMIN"));
            result.setUser(user);
            result.setAuthorities(new ArrayList<>());
		} catch (ServiceException e) {
			throw new UsernameNotFoundException(e.getMessage());
		}
		return result;
	}

}
