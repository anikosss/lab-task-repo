package com.epam.newsmanagement.dao.impl.oracle;

import com.epam.newsmanagement.dao.IUserDAO;
import com.epam.newsmanagement.entity.User;
import com.epam.newsmanagement.exception.DAOException;
import com.github.springtestdbunit.DbUnitTestExecutionListener;
import com.github.springtestdbunit.annotation.DatabaseOperation;
import com.github.springtestdbunit.annotation.DatabaseSetup;
import com.github.springtestdbunit.annotation.DatabaseTearDown;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.springframework.test.context.support.DirtiesContextTestExecutionListener;
import org.springframework.test.context.transaction.TransactionalTestExecutionListener;

import java.util.List;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath:test-context.xml"})
@DatabaseSetup(value = "classpath:test-dataset.xml")
@DatabaseTearDown(value = "classpath:test-dataset.xml", type = DatabaseOperation.DELETE_ALL)
@TestExecutionListeners({ DependencyInjectionTestExecutionListener.class,
        DirtiesContextTestExecutionListener.class,
        TransactionalTestExecutionListener.class,
        DbUnitTestExecutionListener.class })
public class OracleUserDAOImplTest extends Assert {
    @Autowired
    private IUserDAO userDAO;

    @Test
    public void testCreate() throws Exception {
        User user = new User(1L, "name", "login", "password");
        Long id = userDAO.create(user);
        User dbUser = userDAO.read(id);
        assertEquals(id.longValue(), dbUser.getUserId());
    }

    @Test
    public void testRead() throws Exception {
        User user = userDAO.read(2L);
        assertEquals("name2", user.getName());
    }

    @Test
    public void testReadByLogin() throws Exception {
        User user = userDAO.readUserByLogin("login1");
        assertEquals(1L, user.getUserId());
    }

    @Test
    public void testReadAll() throws Exception {
        List<User> users = userDAO.readAll();
        assertEquals(2, users.size());
    }

    @Test
    public void testUpdate() throws Exception {
        User user = userDAO.read(1L);
        user.setName("newName");
        userDAO.update(user);
        user = userDAO.read(1L);
        assertEquals("newName", user.getName());
    }

    @Test(expected = DAOException.class)
    public void testDelete() throws Exception {
        userDAO.delete(2L);
        User user = userDAO.read(2L);
    }

}