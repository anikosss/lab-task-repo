<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<!doctype html>
<html lang="en">
<head>
    <tiles:insertAttribute name="head" />
    <title>Admin page</title>
</head>
<body>
<tiles:insertAttribute name="header" />
<div class="container">
    <div class="row">
        <div class="col-sm-3">
            <tiles:insertAttribute name="left-menu" />
        </div>
        <div class="col-md-9">
            <div class="row">
                <div class="col-md-12">
                    <tiles:insertAttribute name="filter-bar" />
                </div>
            </div>
            <tiles:insertAttribute name="main-content" />
        </div>
    </div>
</div>
<tiles:insertAttribute name="footer" />
</body>
</html>
