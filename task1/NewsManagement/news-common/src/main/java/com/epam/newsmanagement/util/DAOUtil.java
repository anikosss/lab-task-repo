package com.epam.newsmanagement.util;

import org.apache.log4j.Logger;
import org.springframework.jdbc.datasource.DataSourceUtils;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

/**
 * <p>Represents utils for DAO classes</p>
 *
 * @author Dzmitry Anikeichanka
 */
public final class DAOUtil {
    private static final Logger LOGGER = Logger.getLogger(DAOUtil.class);

    private DAOUtil() {}

    /**
     * <p>Releases resources</p>
     *
     * @param dataSource Data source where connection must be returned to
     * @param connection {@link Connection} that must be returned to connections pool
     * @param statement {@link Statement} that must be closed
     * @param resultSet {@link ResultSet} that must be closed
     */
    public static void releaseResources(DataSource dataSource, Connection connection, Statement statement,
                                        ResultSet resultSet) {
        try {
            if (connection != null) {
                DataSourceUtils.releaseConnection(connection, dataSource);
            }
            if (statement != null) {
                statement.close();
            }
            if (resultSet != null) {
                resultSet.close();
            }
        } catch (SQLException e) {
            LOGGER.error(e.getMessage(), e);
        }
    }

    /**
     * <p>Releases resources</p>
     *
     * @param dataSource Data source where connection must be returned to
     * @param connection {@link Connection} that must be returned to connections pool
     * @param statement {@link Statement} that must be closed
     */
    public static void releaseResources(DataSource dataSource, Connection connection, Statement statement) {
        try {
            if (connection != null) {
                DataSourceUtils.releaseConnection(connection, dataSource);
            }
            if (statement != null) {
                statement.close();
            }
        } catch (SQLException e) {
            LOGGER.error(e.getMessage(), e);
        }
    }

}
