package com.epam.newsmanagement.service.impl;

import com.epam.newsmanagement.dao.IUserDAO;
import com.epam.newsmanagement.entity.User;
import com.epam.newsmanagement.service.IUserService;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.ArrayList;
import java.util.List;

import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath:test-context.xml"})
public class UserServiceImplTest extends Assert {
    @InjectMocks
    @Autowired
    private IUserService userService;
    @Mock
    private IUserDAO userDAO;

    @Before
    public void setUp()  {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void testReadUserById() throws Exception {
        when(userDAO.read(1L)).thenReturn(buildUser(1L));
        User user = userService.getUserById(1L);
        verify(userDAO).read(1L);
        assertEquals("name1", user.getName());
    }

    @Test
    public void testReadUserByWrongId() throws Exception {
        when(userDAO.read(12L)).thenReturn(null);
        User user = userService.getUserById(12L);
        verify(userDAO).read(12L);
        assertNull(user);
    }

    @Test
    public void testAddNewUser() throws Exception {
        User user = buildUser(5L);
        when(userDAO.create(user)).thenReturn(15L);
        when(userDAO.read(15L)).thenReturn(user);
        Long id = userService.addUser(user);
        User result = userService.getUserById(id);
        verify(userDAO).create(user);
        verify(userDAO).read(15L);
        assertEquals("name5", result.getName());
    }

    @Test
    public void testReadAllUsers() throws Exception {
        List<User> users = buildUsersList(5);
        when(userDAO.readAll()).thenReturn(users);
        List<User> result = userService.getAllUsers();
        verify(userDAO).readAll();
        assertEquals(5, result.size());
    }

    @Test
    public void testUpdateUser() throws Exception {
        User user = buildUser(3L);
        when(userDAO.read(3L)).thenReturn(new User(3L, "newName", "newLogin", "newPassword"));
        userService.updateUser(user);
        User result = userService.getUserById(3L);
        verify(userDAO).update(user);
        assertEquals("newName", result.getName());
    }

    @Test
    public void testDeleteUserById() throws Exception {
        when(userDAO.read(5L)).thenReturn(null);
        userService.deleteUserById(5L);
        User user = userService.getUserById(5L);
        verify(userDAO).delete(5L);
        assertNull(user);
    }

    @Test
    public void testGetUserByLogin() throws Exception {
        when(userDAO.readUserByLogin(anyString())).thenReturn(buildUser(1L));
        User user = userService.getUserByLogin("login1");
        verify(userDAO).readUserByLogin(anyString());
        assertEquals("login1", user.getLogin());
    }

    private User buildUser(long id) {
        return new User(id, "name" + id, "login" + id, "password" + id);
    }

    private List<User> buildUsersList(int size) {
        List<User> result = new ArrayList<>();
        for (int i = 0; i < size; i++) {
            result.add(buildUser((long) i));
        }
        return result;
    }

}