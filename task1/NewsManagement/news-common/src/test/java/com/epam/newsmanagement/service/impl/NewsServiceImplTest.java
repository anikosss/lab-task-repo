package com.epam.newsmanagement.service.impl;

import com.epam.newsmanagement.dao.INewsDAO;
import com.epam.newsmanagement.entity.News;
import com.epam.newsmanagement.service.INewsService;
import com.epam.newsmanagement.util.SearchCriteria;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.mockito.Matchers.anyInt;
import static org.mockito.Matchers.anyLong;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath:test-context.xml"})
public class NewsServiceImplTest extends Assert {
    @Autowired
    @InjectMocks
    private INewsService newsService;
    @Mock
    private INewsDAO newsDAO;

    @Before
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void testGetNewsById() throws Exception {
        when(newsDAO.read(2L)).thenReturn(buildNews(2L));
        News news = newsService.getNewsById(2L);
        verify(newsDAO).read(2L);
        assertEquals("title2", news.getTitle());
    }

    @Test
    public void testGetNewsByWrongId() throws Exception {
        when(newsDAO.read(15L)).thenReturn(null);
        News news = newsService.getNewsById(15L);
        verify(newsDAO).read(15L);
        assertNull(news);
    }

    @Test
    public void testAddNewNews() throws Exception {
        News news = buildNews(3L);
        when(newsDAO.create(news)).thenReturn(10L);
        when(newsDAO.read(10L)).thenReturn(buildNews(3L));
        Long id = newsService.addNews(news);
        News result = newsService.getNewsById(id);
        verify(newsDAO).create(news);
        verify(newsDAO).read(id);
        assertEquals("title3", result.getTitle());
    }

    @Test
    public void testGetAllNews() throws Exception {
        when(newsDAO.readAll()).thenReturn(buildNewsList(4));
        List<News> news = newsService.getAllNews();
        assertEquals(4, news.size());
    }

    @Test
    public void testGetNewsWithRange() throws Exception {
        when(newsDAO.readWithRange(anyInt(), anyInt())).thenReturn(buildNewsList(4));
        List<News> news = newsService.getNewsWithRange(1,4);
        verify(newsDAO).readWithRange(anyInt(), anyInt());
        assertEquals(4, news.size());
    }

    @Test
    public void testGetNextNewsId() throws Exception {
        when(newsDAO.readNextNewsId(anyLong())).thenReturn(2L);
        long nextNewsId = newsService.getNextNewsId(3L);
        verify(newsDAO).readNextNewsId(anyLong());
        assertEquals(2L, nextNewsId);
    }

    @Test
    public void testGetPreviousNewsId() throws Exception {
        when(newsDAO.readPreviousNewsId(anyLong())).thenReturn(2L);
        long nextNewsId = newsService.getPreviousNewsId(3L);
        verify(newsDAO).readPreviousNewsId(anyLong());
        assertEquals(2L, nextNewsId);
    }

    @Test
    public void testUpdateNews() throws Exception {
        News news = buildNews(1L);
        when(newsDAO.read(1L)).thenReturn(new News(1L, "newTitle", "newText", "newText", null, null));
        newsService.updateNews(news);
        News result = newsService.getNewsById(1L);
        verify(newsDAO).update(news);
        verify(newsDAO).read(1L);
        assertEquals("newTitle", result.getTitle());
    }

    @Test
    public void testDeleteNewsById() throws Exception {
        when(newsDAO.read(4L)).thenReturn(null);
        newsService.deleteNewsById(4L);
        News news = newsService.getNewsById(4L);
        verify(newsDAO).delete(4L);
        assertNull(news);
    }

    @Test
    public void testGetAllNewsCount() throws Exception {
        when(newsDAO.getAllNewsCount()).thenReturn(5);
        int count = newsService.getAllNewsCount();
        assertEquals(5, count);
    }

    @Test
    public void testGetNewsCountBySearchCriteria() throws Exception {
        SearchCriteria searchCriteria = new SearchCriteria(2L, Arrays.asList(3L, 4L, 5L));
        when(newsDAO.getNewsCountBySearchCriteria(eq(searchCriteria))).thenReturn(5);
        int count = newsService.getNewsCountBySearchCriteria(searchCriteria);
        verify(newsDAO).getNewsCountBySearchCriteria(eq(searchCriteria));
        assertEquals(5, count);
    }

    @Test
    public void testSearchBySearchCriteria() throws Exception {
        SearchCriteria searchCriteria = new SearchCriteria(1L, Arrays.asList(1L, 2L));
        when(newsDAO.searchBySearchCriteria(eq(searchCriteria), anyInt(), anyInt())).thenReturn(buildNewsList(2));
        List<News> newsList = newsService.searchNewsBySearchCriteria(searchCriteria, 1, 6);
        verify(newsDAO).searchBySearchCriteria(eq(searchCriteria), anyInt(), anyInt());
        assertEquals(2, newsList.size());
    }

    @Test
    public void testSearchByAuthor() throws Exception {
        when(newsDAO.searchByAuthor(1L, 1, 6)).thenReturn(buildNewsList(2));
        List<News> list = newsService.searchNewsByAuthor(1L, 1, 6);
        verify(newsDAO).searchByAuthor(eq(1L), anyInt(), anyInt());
        assertEquals(2, list.size());
    }

    @Test
    public void testSearchByTags() throws Exception {
        List<Long> tagIdList = new ArrayList<>(Arrays.asList(1L, 2L, 3L));
        when(newsDAO.searchByTagIdList(tagIdList, 1, 6)).thenReturn(buildNewsList(3));
        List<News> news = newsService.searchNewsByTags(tagIdList, 1, 6);
        verify(newsDAO).searchByTagIdList(eq(tagIdList), anyInt(), anyInt());
        assertEquals(3, news.size());
    }

    @Test
    public void testSearchByAuthorAndTags() throws Exception {
        List<Long> tagIdList = new ArrayList<>(Arrays.asList(1L, 2L));
        when(newsDAO.searchByAuthorAndTags(1L, tagIdList, 1, 6)).thenReturn(buildNewsList(2));
        List<News> news = newsService.searchNewsByAuthorAndTags(1L, tagIdList, 1, 6);
        verify(newsDAO).searchByAuthorAndTags(eq(1L), eq(tagIdList), anyInt(), anyInt());
        assertEquals(2, news.size());
    }

    private News buildNews(long id) {
        return new News(id, "title" + id, "text" + id, "text" + id, null, null);
    }

    private List<News> buildNewsList(int size) {
        List<News> result = new ArrayList<>();
        for (int i = 0; i < size; i++) {
            result.add(buildNews((long) size));
        }
        return result;
    }
    
}