package com.epam.newsmanagement.controller;

import com.epam.newsmanagement.command.IActionCommand;
import com.epam.newsmanagement.command.factory.ActionCommandFactory;
import com.epam.newsmanagement.exception.CommandException;
import com.epam.newsmanagement.message.Attributes;
import com.epam.newsmanagement.util.MethodEnum;
import com.epam.newsmanagement.util.ResponseHolder;
import org.apache.log4j.Logger;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * <p>General servlet for application</p>
 */
@WebServlet(name = "Controller", urlPatterns = "/controller", loadOnStartup = 1)
public class Controller extends HttpServlet {
    private static final Logger LOGGER = Logger.getLogger(Controller.class);
    private ActionCommandFactory commandFactory;

    /**
     * <p>Initializes servlet and Spring context</p>
     *
     * @throws ServletException
     */
    @Override
    public void init() throws ServletException {
        super.init();
        ApplicationContext context = new ClassPathXmlApplicationContext("client-context.xml");
        commandFactory = (ActionCommandFactory) context.getBean("commandFactory");
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }
    
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * <p>Processes the request and the response</p>
     *
     * @param request {@link HttpServletRequest} that should be processed
     * @param response {@link HttpServletResponse} that should be processed
     * @throws ServletException
     * @throws IOException
     */
    private void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String action = request.getParameter(Attributes.ACTION);
        try {
            IActionCommand command = commandFactory.defineCommand(action);
            ResponseHolder responseHolder = command.execute(request);
            if (responseHolder.getMethod() == MethodEnum.FORWARD) {
                request.getRequestDispatcher(responseHolder.getPage()).forward(request, response);
            } else {
                response.sendRedirect(responseHolder.getPage());
            }
        } catch (CommandException e) {
            LOGGER.error(e.getMessage(), e);
            if (!response.isCommitted()) {
                response.sendError(403);
            }
        }
    }
}
