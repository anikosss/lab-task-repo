package com.epam.newsmanagement.validator.impl;

import com.epam.newsmanagement.exception.ValidateException;
import com.epam.newsmanagement.message.Attributes;
import com.epam.newsmanagement.message.ErrorMessages;
import com.epam.newsmanagement.message.RegExpressions;
import com.epam.newsmanagement.validator.IRequestValidator;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletRequest;

/**
 * <p>Represents validator for news</p>
 */
@Component
public class NewsValidator implements IRequestValidator {

    private NewsValidator() {}

    @Override
    public boolean validateRequest(HttpServletRequest request) throws ValidateException {
        String newsParam = request.getParameter(Attributes.NEWS);
        if (newsParam == null || newsParam.isEmpty() || !newsParam.matches(RegExpressions.NUMBER_REGEX)) {
            request.setAttribute(Attributes.ERROR, ErrorMessages.WRONG_NEWS);
            throw new ValidateException(ErrorMessages.WRONG_NEWS);
        }
        return true;
    }
}
