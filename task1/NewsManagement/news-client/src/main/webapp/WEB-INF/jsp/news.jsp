<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!doctype html>
<html lang="en">
<jsp:include page="common/head.jsp" />
<body>
<jsp:include page="common/header.jsp" />
<div class="container">
    <jsp:include page="common/search.jsp" />
    <c:if test="${empty newsList}">
    <div class="col-md-offset-4 col-md-4">
            <div class="alert alert-danger alert-header fade in">
                <a href="#" class="close" data-dismiss="alert" area-label="close">&times;</a>
                There are no such news.
            </div>
        </div>
    </c:if>
    <c:forEach items="${newsList}" var="newsDTO">
        <div class="row">
            <div class="col-md-12">
                <div class="media">
                    <h5 class="pull-right">${newsDTO.news.modificationDate}</h5>
                    <div class="media-body">
                        <h3 class="media-heading">${newsDTO.news.title}</h3><h5>(by ${newsDTO.author.name})</h5>
                        ${newsDTO.news.shortText}
                    </div>
                    <h5 class="pull-left"><i>Comments(5)</i></h5>
                    <a href="controller?action=view-news&news=${newsDTO.news.newsId}" class="pull-right">
                        <button class="btn btn-default">View</button>
                    </a>
                    <c:forEach items="${newsDTO.tags}" var="tag">
                        <p class="pull-right"><i>${tag.name}</i></p>
                    </c:forEach>
                </div>
            </div>
        </div>
        <br>
    </c:forEach>
    <jsp:include page="common/pagination.jsp" />
</div>
<br><br>
<jsp:include page="common/footer.jsp" />
<%--<script type="text/javascript" src="resources/js/multiple-select.js"></script>--%>
<script type="text/javascript" src="resources/js/search-bar.js"></script>
<script>
    $('#tags-select:selected').each(function(){
        this.selected = false;
    });
</script>
</body>
</html>
