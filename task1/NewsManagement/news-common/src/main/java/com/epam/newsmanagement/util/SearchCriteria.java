package com.epam.newsmanagement.util;

import java.util.List;

/**
 * <p>Represents class for realize searching</p>
 *
 * @author Dzmitry Anikeichanka
 */
public class SearchCriteria {
    private Long authorId;
    private List<Long> tagIdList;

    public SearchCriteria() {}

    public SearchCriteria(Long authorId) {
        this.authorId = authorId;
    }

    public SearchCriteria(List<Long> tagIdList) {
        this.tagIdList = tagIdList;
    }

    public SearchCriteria(Long authorId, List<Long> tagIdList) {
        this.authorId = authorId;
        this.tagIdList = tagIdList;
    }

    public Long getAuthorId() {
        return authorId;
    }

    public void setAuthorId(Long authorId) {
        this.authorId = authorId;
    }

    public List<Long> getTagIdList() {
        return tagIdList;
    }

    public void setTagIdList(List<Long> tagIdList) {
        this.tagIdList = tagIdList;
    }

    @Override
    public String toString() {
        final StringBuilder result = new StringBuilder("SearchCriteria {");
        result.append("authorId = ").append(authorId);
        result.append(", tagIdList = ").append(tagIdList);
        result.append("}");
        return result.toString();
    }
}
